<?php

use App\Enums\AgentDeviceType;
use App\Enums\AgentStatus;

return [
    AgentStatus::class => [
        AgentStatus::PENDING        => 'Кандидат',
        AgentStatus::EMPLOYED       => 'Сотрудник',
        AgentStatus::REJECTED       => 'Отклоненный',
    ],

    AgentDeviceType::class => [
        AgentDeviceType::IOS        => 'iOS',
        AgentDeviceType::ANDROID    => 'Android',
    ],
];
