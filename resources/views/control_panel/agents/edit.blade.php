@extends('adminlte::page')

@section('title')
    Отредактировать агента - {{ $agent->name }}
@stop

@section('content_header')
    <h1 class="m-0 text-dark">Отредактировать агента - {{ $agent->name }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <form
                method="POST"
                action="{{ $agent->path() }}"
                class="card"
            >
                @csrf
                @method('PATCH')

                <div class="card-body">

                    @include('control_panel.units.form.input', [
                        'name' => 'name',
                        'placeholder' => 'Введите ФИО...',
                        'required' => true,
                        'title' => 'ФИО',
                        'value' => $agent->name,
                    ])

                    @include('control_panel.units.form.input', [
                        'name' => 'cellphone',
                        'placeholder' => 'Введите номер телефона...',
                        'required' => true,
                        'title' => 'Номер телефона (формат +79ХХХХХХХХХ)',
                        'value' => $agent->cellphone,
                    ])

                    @include('control_panel.units.form.select', [
                        'name' => 'city_id',
                        'title' => 'Город',
                        'value' => $agent->city_id,
                        'options' => $cities,
                    ])

                    @include('control_panel.units.form.input', [
                        'name' => 'address',
                        'placeholder' => 'Введите адрес...',
                        'required' => true,
                        'title' => 'Адрес',
                        'value' => $agent->address,
                    ])

                    @include('control_panel.units.form.input', [
                        'name' => 'post_code',
                        'placeholder' => 'Введите почтовый индекс...',
                        'required' => true,
                        'title' => 'Почтовый индекс',
                        'value' => $agent->post_code,
                    ])

                    @include('control_panel.units.form.input', [
                        'name' => 'comment',
                        'placeholder' => 'Введите комментарий...',
                        'title' => 'Комментарий',
                        'value' => $agent->comment,
                    ])

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">
                        Сохранить
                    </button>
                </div>

            </div>
        </form>

    </div>
@stop
