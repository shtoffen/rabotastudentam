@extends('adminlte::page')

@section('title', 'Список кандидатов')

@section('content_header')
    <div class="container-fluid p-0">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                Список {{ \App\Enums\AgentStatus::getDescription($status) }}ов
                </h1>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <table
                        class="table table-bordered table-sm"
                    >
                        <thead>
                            <tr>
                                <th width="35%">Имя</th>
                                <th width="25%">Телефон</th>
                                <th width="20%">Email</th>
                                <th width="20%">Город</th>
                            </tr>
                        </thead>

                        <tbody>

                            @forelse ($agents as $agent)
                                <tr>
                                    <td>
                                        <a href="{{ $agent->path() }}">
                                            {{ $agent->name }}
                                        </a>
                                    </td>
                                    <td>{{ $agent->cellphone }}</td>
                                    <td>{{ $agent->email }}</td>
                                    <td>{{ $agent->city->name }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Ничего нет.</td>
                                </tr>
                            @endforelse

                        </tbody>
                    </table>
                </div>

                <div class="card-footer d-flex justify-content-center">
                    {{ $agents->links() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    @if ($errors->any())
        <script>
            Swal.fire({
                icon: 'error',
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000,
                title: '{{ $errors->first() }}',
                toast: true,
            })
        </script>
    @endif
@stop
