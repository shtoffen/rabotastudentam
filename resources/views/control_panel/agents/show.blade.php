@extends('adminlte::page')

@section('title')
    Карточка агента - {{ $agent->name }}
@stop

@section('css')
    <link href="{{ asset('vendor/lightbox/css/lightbox.min.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="{{ asset('vendor/lightbox/js/lightbox.min.js') }}"></script>
@endsection

@section('content_header')
    <div class="container-fluid p-0">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    Карточка агента - {{ $agent->name }}
                </h1>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-body pb-0">
                    <strong><i class="fas fa-user-tag mr-1"></i> ФИО</strong>
                    <p class="text-muted">{{ $agent->name }}</p>

                    <hr>

                    <strong><i class="fas fa-phone-alt mr-1"></i> Номер телефона</strong>
                    <p class="text-muted">
                        <a href="tel:{{ $agent->cellphone }}">
                            {{ $agent->cellphone }}
                        </a>
                    </p>

                    <hr>

                    <strong><i class="fas fa-envelope mr-1"></i> E-mail</strong>
                    <p class="text-muted">
                        <a href="malito:{{ $agent->email }}">
                            {{ $agent->email }}
                        </a>
                    </p>

                    <hr>

                    <strong><i class="fas fa-map-marker-alt mr-1"></i> Адрес</strong>
                    <p class="text-muted">
                        Город: {{ $agent->city->name }}<br>
                        Адрес: {{ $agent->address }}<br>
                        Почтовый индекс: {{ $agent->post_code }}<br>
                    </p>

                    <hr>

                    <strong><i class="fas fa-comment mr-1"></i> Комментарий</strong>
                    <p class="text-muted">{{ $agent->comment }}</p>

                    <hr>

                    <strong><i class="fas fa-calendar-day mr-1"></i> Дата рождения</strong>
                    <p class="text-muted">
                        {{ \Carbon\Carbon::parse($agent->date_of_birth)->format('d.m.Y') }}
                    </p>

                    <hr>

                    <strong><i class="fas fa-calendar-alt mr-1"></i> Дата регистрации</strong>
                    <p class="text-muted">
                        {{ $agent->created_at->format('d.m.Y H:i') }}
                    </p>

                    <hr>

                    <strong><i class="fas fa-passport mr-1"></i> Фотографии паспорта</strong>
                    <div class="text-center mb-2">
                        @forelse ($agent->images as $image)
                            <a href="{{ $image->url }}" data-lightbox="passport">
                                <img
                                    src="{{ $image->url }}"
                                    class="img-thumbnail rounded"
                                    width="200"
                                >
                            </a>
                        @empty
                            <p class="text-muted">Ничего нет.</p>
                        @endforelse
                    </div>

                </div>

                <div class="card-footer">
                    <a
                        href="{{ $agent->path('edit') }}"
                        class="btn btn-lg btn-primary"
                    >
                        Редактировать
                    </a>

                    <div class="float-right">
                        @if ($agent->status->is(\App\Enums\AgentStatus::PENDING))
                        <form
                            action="{{ $agent->path() }}"
                            class="d-inline"
                            method="POST"
                        >
                            @csrf
                            @method('PATCH')

                            <input
                                name="status"
                                type="hidden"
                                value="{{ \App\Enums\AgentStatus::EMPLOYED }}"
                            >

                            <button
                                class="btn btn-lg btn-success"
                                type="submit"
                            >
                                Принять
                            </button>
                        </form>

                        <form
                            action="{{ $agent->path() }}"
                            class="d-inline"
                            method="POST"
                        >
                            @csrf
                            @method('PATCH')

                            <input
                                name="status"
                                type="hidden"
                                value="{{ \App\Enums\AgentStatus::REJECTED }}"
                            >

                            <button
                                class="btn btn-lg btn-danger"
                                type="submit"
                            >
                                Отклонить
                            </button>
                        </form>
                        @endif
                    </div>

                </div>

                <div class="ribbon-wrapper ribbon-xl">
                    <div
                        class="ribbon text-lg
                            @if ($agent->status->is(\App\Enums\AgentStatus::PENDING))
                                bg-primary
                            @elseif ($agent->status->is(\App\Enums\AgentStatus::EMPLOYED))
                                bg-success
                            @elseif ($agent->status->is(\App\Enums\AgentStatus::REJECTED))
                                bg-danger
                            @endif
                        "
                    >
                        <b>{{ $agent->status->description }}</b>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
