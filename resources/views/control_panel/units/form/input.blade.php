<div class="form-group">
    @isset ($title)
    <label for="{{ $name }}">{{ $title }}</label>
    @endif

    <input
        class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}"

        id="{{ $name }}"
        name="{{ $name }}"
        type="{{ $type ?? 'text' }}"

        @isset ($accept)
        accept="{{ $accept }}"
        @endisset

        @isset ($placeholder)
        placeholder="{{ $placeholder }}"
        @endisset

        @isset ($required)
        required
        @endisset

        @isset ($disabled)
        disabled
        aria-disabled="true"
        @endisset

        value="{{ old($name, $value ?? '') }}"
    >

    @include('control_panel.units.input_error', ['field' => $name])
</div>
