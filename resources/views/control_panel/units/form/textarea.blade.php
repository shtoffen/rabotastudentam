<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>

    <textarea
        class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}"

        id="{{ $name }}"
        name="{{ $name }}"

        @isset ($placeholder)
        placeholder="{{ $placeholder }}"
        @endisset

        @isset ($required)
        required
        @endisset

        @isset ($rows)
        rows="{{ $rows }}"
        @endisset

    >{{ old($name, $value ?? '') }}</textarea>

    @include('control_panel.units.input_error', ['field' => $name])
</div>
