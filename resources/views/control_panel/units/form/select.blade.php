<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>

    <select
        class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}"

        id="{{ $name }}"
        name="{{ $name }}"

        @isset ($required)
        required
        @endisset

        value=""
    >
        <option
            disabled
            {{ old($name, $value ?? '') == '' ? ' selected' : '' }}
        >
            Выберите один из вариантов
        </option>

        @foreach ($options as $option)
            <option
                value="{{ $option->id }}"
                {{ old($name, $value ?? '') == $option->id ? ' selected' : '' }}
            >
                {{ $option->name }}
            </option>
        @endforeach
    </select>

    @include('control_panel.units.input_error', ['field' => $name])
</div>
