@extends('adminlte::page')

@section('title', 'Отредактировать элемент Stories')

@section('content_header')
    <h1 class="m-0 text-dark">Отредактировать элемент в Stories</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <form
                action="{{ $story->path('update') }}"
                class="card"
                enctype="multipart/form-data"
                method="POST"
            >
                @csrf
                @method('PATCH')

                <div class="card-body">

                    @include('control_panel.units.form.input', [
                        'name' => 'title',
                        'placeholder' => 'Введите название...',
                        'required' => true,
                        'title' => 'Название элемента',
                        'value' => $story->title,
                    ])

                    @include('control_panel.units.form.textarea', [
                        'name' => 'description',
                        'placeholder' => 'Введите текст...',
                        'required' => true,
                        'title' => 'Текст элемента',
                        'value' => $story->description,
                    ])

                    @include('control_panel.units.form.input', [
                        'accept' => 'image/*',
                        'name' => 'image',
                        'title' => 'Изображение категории',
                        'type' => 'file',
                    ])

                    <p class="text-muted mb-0">
                        Если оставить это поле пустым, то старое изображение сохранится.
                    </p>

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">
                        Сохранить
                    </button>
                </div>

            </div>
        </form>

    </div>
@stop
