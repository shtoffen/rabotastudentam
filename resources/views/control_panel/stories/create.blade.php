@extends('adminlte::page')

@section('title', 'Добавить новый элемент в Stories')

@section('content_header')
    <h1 class="m-0 text-dark">Добавить новый элемент в Stories</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <form
                action="{{ route('cp.stories.store') }}"
                class="card"
                enctype="multipart/form-data"
                method="POST"
            >
                @csrf

                <div class="card-body">

                    @include('control_panel.units.form.input', [
                        'disabled' => true,
                        'name' => 'category_name',
                        'title' => 'Категория',
                        'value' => optional($category)->title,
                    ])

                    @include('control_panel.units.form.input', [
                        'name' => 'category_id',
                        'required' => true,
                        'type' => 'hidden',
                        'value' => optional($category)->id,
                    ])

                    @include('control_panel.units.form.input', [
                        'name' => 'title',
                        'placeholder' => 'Введите название...',
                        'required' => true,
                        'title' => 'Название элемента',
                    ])

                    @include('control_panel.units.form.textarea', [
                        'name' => 'description',
                        'placeholder' => 'Введите текст...',
                        'required' => true,
                        'rows' => 4,
                        'title' => 'Текст элемента',
                    ])

                    @include('control_panel.units.form.input', [
                        'accept' => 'image/*',
                        'name' => 'image',
                        'required' => true,
                        'title' => 'Изображение элемента',
                        'type' => 'file',
                    ])

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">
                        Добавить
                    </button>
                </div>

            </div>
        </form>

    </div>
@stop
