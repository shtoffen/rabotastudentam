@extends('adminlte::page')

@section('title', 'Stories')

@section('content_header')
    <div class="container-fluid p-0">
        <div class="row mb-2">
            <div class="col-3">
                <h1>Stories</h1>
            </div>

            <div class="col-9">
                <a
                    href="{{ route('cp.stories.categories.create') }}"
                    role="button"
                    class="btn btn-primary float-right"
                >
                    <i class="fas fa-plus"></i>
                    Добавить категорию
                </a>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        @forelse ($categories as $category)
            <div class="col-md-4 mb-4">
                <div class="card h-100">
                    <div class="card-body text-center pb-0">
                        <a href="{{ $category->path() }}">
                            <img
                                src="{{ $category->image->url }}"
                                style="max-height:100px;"
                                class="h-100"
                            >
                        </a>

                        <p class="card-text pt-3 text-bold">
                            {{ $category->title }}
                        </p>
                    </div>
                    <div class="card-footer pr-2">
                        <div class="card-tools float-right">
                            <a
                                href="{{ $category->path() }}"
                                class="btn btn-sm btn-primary float-right"
                            >
                                Подробнее
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        Ничего нет.
                    </div>
                </div>
            </div>
        @endforelse
    </div>
@stop
