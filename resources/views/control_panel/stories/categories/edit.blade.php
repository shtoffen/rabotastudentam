@extends('adminlte::page')

@section('title', 'Отредактировать категорию Stories')

@section('content_header')
    <h1 class="m-0 text-dark">Отредактировать категорию Stories</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <form
                action="{{ $category->path() }}"
                class="card"
                enctype="multipart/form-data"
                method="POST"
            >
                @csrf
                @method('PUT')

                <div class="card-body">

                    @include('control_panel.units.form.input', [
                        'name' => 'title',
                        'placeholder' => 'Введите название...',
                        'required' => true,
                        'title' => 'Название категории',
                        'value' => $category->title,
                    ])

                    @include('control_panel.units.form.input', [
                        'accept' => 'image/*',
                        'name' => 'image',
                        'title' => 'Изображение категории',
                        'type' => 'file',
                    ])

                    <p class="text-muted mb-0">
                        Если оставить это поле пустым, то старое изображение сохранится.
                    </p>

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">
                        Сохранить
                    </button>
                </div>

            </div>
        </form>

    </div>
@stop
