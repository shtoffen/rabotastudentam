@extends('adminlte::page')

@section('title', 'Добавить новую категорию Stories')

@section('content_header')
    <h1 class="m-0 text-dark">Добавить новую категорию Stories</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <form
                action="{{ route('cp.stories.categories.store') }}"
                class="card"
                enctype="multipart/form-data"
                method="POST"
            >
                @csrf

                <div class="card-body">

                    @include('control_panel.units.form.input', [
                        'name' => 'title',
                        'placeholder' => 'Введите название...',
                        'required' => true,
                        'title' => 'Название категории',
                    ])

                    @include('control_panel.units.form.input', [
                        'accept' => 'image/*',
                        'name' => 'image',
                        'required' => true,
                        'title' => 'Изображение категории',
                        'type' => 'file',
                    ])

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">
                        Добавить
                    </button>
                </div>

            </div>
        </form>

    </div>
@stop
