@extends('adminlte::page')

@section('title')
    Категория Stories - {{ $category->title }}
@stop

@section('css')
    <link href="{{ asset('vendor/lightbox/css/lightbox.min.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script src="{{ asset('vendor/lightbox/js/lightbox.min.js') }}"></script>
@endsection

@section('content_header')
    <div class="container-fluid p-0">
        <div class="row mb-2">
            <div class="col">
                <h1>
                    Категория Stories - {{ $category->title }}
                </h1>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pt-1">{{ $category->title }}</h3>
                    <div class="card-tools">
                        <a
                            class="btn btn-sm btn-primary"
                            href="{{ $category->path('edit') }}"
                        >
                            <i class="fas fa-pen"></i> Редактировать
                        </a>

                        <button
                            class="btn btn-sm btn-danger"
                            form="delete"
                            type="submit"
                        >
                            <i class="fas fa-trash"></i> Удалить
                        </button>

                        <form
                            action="{{ $category->path() }}"
                            id="delete"
                            method="POST"
                        >
                            @csrf
                            @method('DELETE')
                        </form>
                    </div>
                </div>

                <div class="card-body">
                    <div class="text-center">
                        <a
                            href="{{ $category->image->url }}"
                            data-lightbox="category"
                        >
                            <img
                                src="{{ $category->image->url }}"
                                class="img-thumbnail rounded"
                                style="height: 200px;"
                            >
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row mb-2">
        <div class="col-6">
            <h2>Stories</h2>
        </div>

        <div class="col-6">
            <a
                href="{{ route('cp.stories.create', [
                    'category_id' => $category->id,
                ]) }}"
                role="button"
                class="btn btn-primary float-right"
            >
                <i class="fas fa-plus"></i>
                Добавить
            </a>
        </div>
    </div>

    <div class="row">
        @forelse ($stories as $story)
            <div class="col-md-4 mb-4">
                <div class="card h-100">
                    <div class="card-body">
                        <div class="text-center">
                            <a
                                href="{{ $story->image->url }}"
                                data-lightbox="story"
                                data-title="{{ $story->title }}"
                            >
                                <img
                                    src="{{ $story->image->url }}"
                                    style="max-height:100px;"
                                    class="h-100 img-thumbnail rounded"
                                >
                            </a>
                        </div>

                        <h3 class="card-title pt-3 text-bold">
                            {{ $story->title }}
                        </h3>
                        <p class="card-text pt-2">
                            {{ $story->description }}
                        </p>
                    </div>
                    <div class="card-footer pr-2">
                        <div class="card-tools float-right">
                            <a
                                href="{{ $story->path('edit') }}"
                                class="btn btn-sm btn-primary"
                            >
                                <i class="fas fa-pen"></i> Редактировать
                            </a>

                            <button
                                type="submit"
                                class="btn btn-sm btn-danger"
                                form="deleteStory-{{ $story->id }}"
                            >
                                <i class="fas fa-trash"></i> Удалить
                            </button>

                            <form
                                action="{{ $story->path('destroy') }}"
                                id="deleteStory-{{ $story->id }}"
                                method="POST"
                            >
                                @csrf
                                @method('DELETE')
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        Ничего нет.
                    </div>
                </div>
            </div>
        @endforelse
    </div>
@stop
