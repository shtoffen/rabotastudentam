@extends('adminlte::page')

@section('title', 'Список этапов обучения')

@section('content_header')
    <div class="container-fluid p-0">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Этапы обучения</h1>
            </div>

            <div class="col-sm-6">
                <a
                    href="{{ route('cp.education.steps.create') }}"
                    role="button"
                    class="btn btn-primary float-right"
                >
                    <i class="fas fa-plus"></i>
                    Добавить
                </a>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <table
                        class="table table-bordered table-sm"
                    >
                        <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="95%">Этап обучения</th>
                            </tr>
                        </thead>

                        <tbody>

                            @forelse ($educationSteps as $educationStep)
                                <tr>
                                    <td>{{ $educationStep->id }}</td>
                                    <td class="pr-2">
                                        <a href="{{ $educationStep->path() }}">
                                            {{ $educationStep->name }}
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Ничего нет.</td>
                                </tr>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
