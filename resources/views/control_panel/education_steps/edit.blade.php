@extends('adminlte::page')

@section('title', 'Отредактировать этап обучения')

@section('content_header')
    <h1 class="m-0 text-dark">Отредактировать этап обучения</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <form
                action="{{ $step->path() }}"
                class="card"
                enctype="multipart/form-data"
                method="POST"
            >
                @csrf
                @method('PATCH')

                <div class="card-body">

                    @include('control_panel.units.form.input', [
                        'name' => 'name',
                        'placeholder' => 'Введите название этапа...',
                        'required' => true,
                        'title' => 'Название этапа',
                        'value' => $step->name,
                    ])

                    @include('control_panel.units.form.textarea', [
                        'name' => 'description',
                        'required' => true,
                        'rows' => 20,
                        'title' => 'Описание',
                        'value' => $step->description,
                    ])

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">
                        Сохранить
                    </button>
                </div>

            </div>
        </form>

    </div>
@stop
