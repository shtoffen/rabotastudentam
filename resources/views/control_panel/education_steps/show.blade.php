@extends('adminlte::page')

@section('title')
    Карточка этапа обучения - {{ $step->name }}
@stop

@section('content_header')
    <div class="container-fluid p-0">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>
                    Карточка этапа обучения - {{ $step->name }}
                </h1>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-body pb-0">

                    <strong>
                        <i class="fas fa-file-signature mr-1"></i>
                        Название
                    </strong>

                    <p class="text-muted">
                        {{ $step->name }}
                    </p>

                    <hr>

                    <strong><i class="fas fa-file-word mr-1"></i>
                        Детальное описание
                    </strong>

                    <p class="text-muted">
                        {{ $step->description }}
                    </p>

                </div>

                <div class="card-footer">

                    <a
                        class="btn btn-sm btn-primary"
                        href="{{ $step->path('edit') }}"
                    >
                        Редактировать
                    </a>

                    <button
                        class="btn btn-sm btn-danger"
                        form="delete"
                        type="submit"
                    >
                        Удалить
                    </button>

                    <form
                        action="{{ $step->path() }}"
                        id="delete"
                        method="POST"
                    >
                        @csrf
                        @method('DELETE')
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop
