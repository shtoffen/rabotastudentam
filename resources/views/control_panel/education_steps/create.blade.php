@extends('adminlte::page')

@section('title', 'Добавить этап обучения')

@section('content_header')
    <h1 class="m-0 text-dark">Добавить этап обучения</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <form
                action="{{ route('cp.education.steps.store') }}"
                class="card"
                enctype="multipart/form-data"
                method="POST"
            >
                @csrf

                <div class="card-body">

                    @include('control_panel.units.form.input', [
                        'name' => 'name',
                        'placeholder' => 'Введите название этапа...',
                        'required' => true,
                        'title' => 'Название этапа',
                    ])

                    @include('control_panel.units.form.textarea', [
                        'name' => 'description',
                        'required' => true,
                        'rows' => 20,
                        'title' => 'Описание',
                    ])

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">
                        Добавить
                    </button>
                </div>

            </div>
        </form>

    </div>
@stop
