@extends('adminlte::page')

@section('title', 'Добавить город')

@section('content_header')
    <h1 class="m-0 text-dark">Добавить город</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <form
                method="POST"
                action="{{ route('cp.cities.store') }}"
                class="card"
            >
                @csrf

                <div class="card-body">

                    @include('control_panel.units.form.input', [
                        'name' => 'name',
                        'placeholder' => 'Введите название города...',
                        'required' => true,
                        'title' => 'Название города',
                    ])

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">
                        Добавить
                    </button>
                </div>

            </div>
        </form>

    </div>
@stop
