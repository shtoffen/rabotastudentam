@extends('adminlte::page')

@section('title', 'Список городов')

@section('content_header')
    <div class="container-fluid p-0">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Города</h1>
            </div>

            <div class="col-sm-6">
                <a
                    href="{{ route('cp.cities.create') }}"
                    role="button"
                    class="btn btn-primary float-right"
                >
                    <i class="fas fa-plus"></i>
                    Добавить
                </a>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <table
                        class="table table-bordered table-sm"
                    >
                        <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="95%">Название</th>
                            </tr>
                        </thead>

                        <tbody>

                            @forelse ($cities as $city)
                                <tr>
                                    <td>{{ $city->id }}</td>
                                    <td class="pr-2">
                                        {{ $city->name }}

                                        <button
                                            form="delete{{ $city->id }}"
                                            role="submit"
                                            type="submit"
                                            class="btn btn-xs btn-outline-danger float-right"
                                            title="Удалить"
                                        >
                                            <i class="fas fa-trash"></i>
                                            Удалить
                                        </button>

                                        <form
                                            method="POST"
                                            action="{{ $city->path('destroy') }}"
                                            id="delete{{ $city->id }}"
                                            hidden="true"
                                        >
                                            @csrf
                                            @method('DELETE')
                                        </form>

                                        <a
                                            href="{{ $city->path('edit') }}"
                                            class="btn btn-xs btn-outline-primary float-right mr-2"
                                            title="Редактировать"
                                            role="button"
                                        >
                                            <i class="fas fa-pen"></i>
                                            Редактировать
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="2">Ничего нет.</td>
                                </tr>
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    @if (session('message'))
        <script>
            Swal.fire({
                icon: 'success',
                position: 'top-end',
                showConfirmButton: false,
                timer: 3500,
                title: '{{ session('message') }}',
                toast: true,
            })
        </script>
    @endisset

    @if ($errors->any())
        <script>
            Swal.fire({
                icon: 'error',
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000,
                title: '{{ $errors->first() }}',
                toast: true,
            })
        </script>
    @endif
@stop
