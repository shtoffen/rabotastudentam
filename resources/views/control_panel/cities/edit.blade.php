@extends('adminlte::page')

@section('title', 'Отредактировать город')

@section('content_header')
    <h1 class="m-0 text-dark">Отредактировать город</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <form
                method="POST"
                action="{{ $city->path('update') }}"
                class="card"
            >
                @csrf
                @method('PATCH')

                <div class="card-body">

                    @include('control_panel.units.form.input', [
                        'name' => 'name',
                        'placeholder' => 'Введите название города...',
                        'required' => true,
                        'title' => 'Название города',
                        'value' => $city->name,
                    ])

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">
                        Сохранить
                    </button>
                </div>

            </div>
        </form>

    </div>
@stop
