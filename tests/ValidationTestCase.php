<?php

namespace Tests;

use Illuminate\Support\Facades\Gate;

abstract class ValidationTestCase extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        Gate::before(fn ($user = null) => true);

        $this->withoutMiddleware();
    }
}
