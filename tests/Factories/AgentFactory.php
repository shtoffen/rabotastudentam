<?php

namespace Tests\Factories;

use App\Agent;
use App\City;
use App\Enums\AgentStatus;

/** @method static static new() */
class AgentFactory extends Factory
{
    protected ?City $city = null;
    protected int $educationStepsCount = 0;
    protected int $imagesCount = 0;
    protected int $status = -1;
    protected bool $withFcmToken = false;

    protected function default(): array
    {
        $array = [
            'address'               => $this->faker->address,
            'cellphone'             => $this->faker->numerify('+79#########'),
            'city_id'               => $this->city
                ? $this->city->id
                : CityFactory::new(),
            'comment'               => $this->faker->sentence,
            'date_of_birth'         => $this->faker->date(),
            'email'                 => $this->faker->safeEmail,
            'name'                  => $this->faker->name,
            'password'              => '$2y$10$xLBGvAxb9yG2Z.aj7Pepz.Kz..vtHASh8kRKvnLWUAaW2KgbHhTvC', // 123
            'post_code'             => $this->faker->postcode,
        ];

        if ($this->status >= 0) {
            $array['status'] = $this->status;
        }

        return $array;
    }

    public function create(array $extra = []): Agent
    {
        $agent = Agent::create($this->attributes($extra));

        if ($this->educationStepsCount > 0) {
            $educationSteps = EducationStepFactory::new()
                ->times($this->educationStepsCount);

            foreach ($educationSteps as $educationStep) {
                $agent->educationSteps()->attach($educationStep->id, [
                    'read' => $this->faker->boolean,
                ]);
            }
        }

        if ($this->imagesCount > 0) {
            ImageFactory::new()
                ->forModel($agent)
                ->times($this->imagesCount);
        }

        if ($this->withFcmToken) {
            FcmTokenFactory::new()->forAgent($agent)->create();
        }

        return $agent;
    }

    public function pending(): self
    {
        $clone = clone $this;

        $clone->status = AgentStatus::PENDING;

        return $clone;
    }

    public function employed(): self
    {
        $clone = clone $this;

        $clone->status = AgentStatus::EMPLOYED;

        return $clone;
    }

    public function rejected(): self
    {
        $clone = clone $this;

        $clone->status = AgentStatus::REJECTED;

        return $clone;
    }

    public function withStatus(): self
    {
        $clone = clone $this;

        $clone->status = AgentStatus::getRandomValue();

        return $clone;
    }

    public function withEducationSteps(int $count = 1): self
    {
        $clone = clone $this;

        $clone->educationStepsCount = $count;

        return $clone;
    }

    public function withCity(City $city): self
    {
        $clone = clone $this;

        $clone->city = $city;

        return $clone;
    }

    public function withImages(int $count = 1): self
    {
        $clone = clone $this;

        $clone->imagesCount = $count;

        return $clone;
    }

    public function withFcmToken(): self
    {
        $clone = clone $this;

        $clone->withFcmToken = true;

        return $clone;
    }
}
