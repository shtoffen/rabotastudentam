<?php

namespace Tests\Factories;

use App\EducationStep;

/** @method static static new() */
class EducationStepFactory extends Factory
{
    protected function default(): array
    {
        return [
            'name'                  => $this->faker->sentence,
            'description'           => $this->faker->sentence,
        ];
    }

    public function create(array $extra = []): EducationStep
    {
        return EducationStep::create($this->attributes($extra));
    }
}
