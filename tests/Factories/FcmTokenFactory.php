<?php

namespace Tests\Factories;

use App\Agent;
use App\FcmToken;

/** @method static static new() */
class FcmTokenFactory extends Factory
{
    protected ?Agent $agent = null;

    protected function default(): array
    {
        return [
            'agent_id'  => $this->agent->id ?? AgentFactory::new(),
            'token'     => $this->faker->uuid,
        ];
    }

    public function create(array $extra = []): FcmToken
    {
        return FcmToken::create($this->attributes($extra));
    }

    public function forAgent(Agent $agent): self
    {
        $clone = clone $this;

        $clone->agent = $agent;

        return $clone;
    }
}
