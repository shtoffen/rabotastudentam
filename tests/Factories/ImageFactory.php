<?php

namespace Tests\Factories;

use App\Enums\ImageContentType;
use App\Image;
use Illuminate\Http\UploadedFile;

/** @method static static new() */
class ImageFactory extends Factory
{
    protected string $disk                  = 'public';
    protected int $imageableId              = 0;
    protected ?string $imagePath            = null;
    protected ?string $imageableType        = null;

    protected function default(): array
    {
        return [
            'content_type'      => ImageContentType::getRandomValue(),
            'disk'              => $this->disk,
            'imageable_id'      => $this->imageableId,
            'imageable_type'    => $this->imageableType,
            'path'              => $this->imagePath
                ?? $this->faker->md5 . '.' . $this->faker->fileExtension,
        ];
    }

    public function create(array $extra = []): Image
    {
        return Image::create($this->attributes($extra));
    }

    public function forModel($imageable): self
    {
        $clone = clone $this;

        $clone->imageableId = $imageable->id;
        $clone->imageableType = get_class($imageable);

        return $clone;
    }

    public function withStoredImage(string $subPath = '/'): self
    {
        $clone = clone $this;

        $clone->imagePath = UploadedFile::fake()
            ->image('test.jpg', 1, 1)
            ->store($subPath, $this->disk);

        return $clone;
    }
}
