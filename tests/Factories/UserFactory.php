<?php

namespace Tests\Factories;

use App\User;

/** @method static static new() */
class UserFactory extends Factory
{
    protected function default(): array
    {
        return [
            'email'                 => $this->faker->unique()->safeEmail,
            'email_verified_at'     => $this->faker->dateTime(),
            'name'                  => $this->faker->name,
            'password'              => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        ];
    }

    public function create(array $extra = []): User
    {
        return User::forceCreate($this->attributes($extra));
    }
}
