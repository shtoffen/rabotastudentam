<?php

namespace Tests\Factories;

use App\Enums\ImageContentType;
use App\StoryCategory;

/** @method static static new() */
class StoryCategoryFactory extends Factory
{
    protected bool $withImage = false;
    protected bool $storedImage = false;

    protected function default(): array
    {
        return [
            'title'             => $this->faker->sentence,
        ];
    }

    public function create(array $extra = []): StoryCategory
    {
        $category = StoryCategory::create($this->attributes($extra));

        if ($this->withImage) {
            $factory = ImageFactory::new()->forModel($category);

            if ($this->storedImage) {
                $factory = $factory->withStoredImage(StoryCategory::IMAGES_PATH);
            }

            $factory->create([
                'content_type' => ImageContentType::STORY_CATEGORY,
            ]);
        }

        return $category;
    }

    public function withImage(bool $realStoredImage = false): self
    {
        $clone = clone $this;

        $clone->withImage = true;
        $clone->storedImage = $realStoredImage;

        return $clone;
    }
}
