<?php

namespace Tests\Factories;

use App\Enums\ImageContentType;
use App\Story;
use App\StoryCategory;

/** @method static static new() */
class StoryFactory extends Factory
{
    protected bool $withImage = false;
    protected bool $storedImage = false;
    protected ?StoryCategory $category = null;

    protected function default(): array
    {
        return [
            'category_id'       => $this->category->id ?? StoryCategoryFactory::new(),
            'description'       => $this->faker->sentence,
            'title'             => $this->faker->sentence,
        ];
    }

    public function create(array $extra = []): Story
    {
        $story = Story::create($this->attributes($extra));

        if ($this->withImage) {
            $factory = ImageFactory::new()->forModel($story);

            if ($this->storedImage) {
                $factory = $factory->withStoredImage(Story::IMAGES_PATH);
            }

            $factory->create([
                'content_type' => ImageContentType::STORY,
            ]);
        }

        return $story;
    }

    public function forCategory(StoryCategory $category): self
    {
        $clone = clone $this;

        $clone->category = $category;

        return $clone;
    }

    public function withImage(bool $realStoredImage = false): self
    {
        $clone = clone $this;

        $clone->withImage = true;
        $clone->storedImage = $realStoredImage;

        return $clone;
    }
}
