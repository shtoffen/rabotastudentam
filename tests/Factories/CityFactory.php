<?php

namespace Tests\Factories;

use App\City;

/** @method static static new() */
class CityFactory extends Factory
{
    protected function default(): array
    {
        return [
            'name' => $this->faker->city,
        ];
    }

    public function create(array $extra = []): City
    {
        return City::create($this->attributes($extra));
    }
}
