<?php

namespace Tests\Factories;

use Faker\Generator as Faker;
use Illuminate\Support\Collection;

abstract class Factory
{
    /** @var \Faker\Generator */
    protected $faker;

    protected $modelClass;

    public function __construct()
    {
        $this->faker = \Faker\Factory::create();

        $reflection = new \ReflectionMethod(static::class, 'create');
        $this->modelClass = $reflection->getReturnType()->getName();
    }

    abstract public function create(array $extra = []);

    public function make(array $extra = [])
    {
        return $this->modelClass::make($this->attributes($extra));
    }

    public function raw(array $extra = []): array
    {
        return $this->attributes($extra);
    }

    public function times(int $times, array $extra = []): Collection
    {
        return collect()
            ->times($times)
            ->map(function () use ($extra) {
                return $this->create($extra);
            });
    }

    abstract protected function default(): array;

    public static function new(): self
    {
        return new static();
    }

    protected function attributes(array $extra = [])
    {
        $raw = array_merge($this->default(), $extra);
        return $this->applyFactories($raw);
    }

    protected function applyFactories(array $attributes): array
    {
        return collect($attributes)
            ->map(function ($attribute) {
                if (!is_a($attribute, self::class)) return $attribute;
                return $attribute->create()->id;
            })
            ->all();
    }

    protected function get($item, $default = null)
    {
        if (is_a($item, NullValue::class)) return null;

        return $item ?? $default;
    }
}
