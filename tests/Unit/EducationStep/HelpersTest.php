<?php

namespace Tests\Unit\EducationStep;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\EducationStepFactory;
use Tests\TestCase;

class HelpersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @group unit
     */
    public function it_has_the_api_path()
    {
        // Setup
        $step = EducationStepFactory::new()->create();

        // Given
        $route = route('api.education.steps.show', ['step' => $step]);

        // When
        $path = $step->apiPath();

        // Then
        $this->assertSame($route, $path);
    }

    /**
     * @test
     * @group unit
     */
    public function it_has_the_path()
    {
         // Given
        $educationStep = EducationStepFactory::new()->create();

        $existingRoutes = ['edit', 'show'];
        $invalidRoutes = ['update', 'destroy', 'test'];

        // Then
        collect($existingRoutes)->each(function($route) use ($educationStep) {
            $this->assertEquals(
                route("cp.education.steps.{$route}", [
                    'step' => $educationStep,
                ]),
                $educationStep->path($route)
            );
        });

        collect($invalidRoutes)->each(function($route) use ($educationStep) {
            $this->expectException(\InvalidArgumentException::class);
            $this->expectExceptionMessage('Invalid route.');

            $educationStep->path($route);
        });
    }
}
