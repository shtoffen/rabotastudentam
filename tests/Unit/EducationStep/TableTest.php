<?php

namespace Tests\Unit\EducationStep;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\EducationStepFactory;
use Tests\TestCase;

class TableTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     * @group unit
     */
    public function it_has_a_name()
    {
        // Given
        $name = $this->faker->name;

        // When
        $educationStep = EducationStepFactory::new()->create([
            'name' => $name,
        ]);

        // Then
        $this->assertEquals($name, $educationStep->name);
    }

    /**
     * @test
     * @group unit
     */
    public function it_has_a_description()
    {
        // Given
        $description = $this->faker->text;

        // When
        $educationStep = EducationStepFactory::new()->create([
            'description' => $description,
        ]);

        // Then
        $this->assertEquals($description, $educationStep->description);
    }

    /**
     * @test
     * @group unit
     */
    public function it_has_a_created_at_timestamp()
    {
        //Given
        $createdAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $createdAt);

        //When
        $educationStep = EducationStepFactory::new()->create([
            'created_at' => $createdAt,
        ])->refresh();

        //Then
        $this->assertEquals($createdAt, $educationStep->created_at);
        $this->assertInstanceOf(Carbon::class, $educationStep->created_at);
    }

    /**
     * @test
     * @group unit
     */
    public function it_has_an_updated_at_timestamp()
    {
        //Given
        $updatedAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $updatedAt);

        //When
        $educationStep = EducationStepFactory::new()->create([
            'updated_at' => $updatedAt,
        ])->refresh();

        //Then
        $this->assertEquals($updatedAt, $educationStep->updated_at);
        $this->assertInstanceOf(Carbon::class, $educationStep->updated_at);
    }
}
