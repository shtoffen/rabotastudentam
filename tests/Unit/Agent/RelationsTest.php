<?php

namespace Tests\Unit\Agent;

use App\City;
use App\EducationStep;
use App\FcmToken;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\AgentFactory;
use Tests\Factories\CityFactory;
use Tests\Factories\FcmTokenFactory;
use Tests\TestCase;

class RelationsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @group unit
     */
    public function it_has_a_city()
    {
        // Given
        $city = CityFactory::new()->create();

        // When
        $agent = AgentFactory::new()->create([
            'city_id' => $city->id,
        ]);

        // Then
        $this->assertInstanceOf(City::class, $agent->city);
        $this->assertTrue($city->is($agent->city));
    }

    /** @test */
    public function it_belongs_to_many_education_steps()
    {
        // Given
        $agent = AgentFactory::new()->withEducationSteps(2)->create();

        // When
        $steps = $agent->educationSteps;

        // Then
        $this->assertInstanceOf(Collection::class, $steps);
        $this->assertInstanceOf(EducationStep::class, $steps->first());
        $this->assertInstanceOf(BelongsToMany::class, $agent->educationSteps());
    }

    /** @test */
    public function it_has_one_fcm_token()
    {
        // Given
        $agent = AgentFactory::new()->create();

        // When
        $fcmToken = FcmTokenFactory::new()->forAgent($agent)->create();

        // Then
        $this->assertInstanceOf(FcmToken::class, $agent->fcmToken);
        $this->assertTrue($fcmToken->is($agent->fcmToken));
    }
}
