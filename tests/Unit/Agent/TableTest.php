<?php

namespace Tests\Unit\Agent;

use App\Enums\AgentDeviceType;
use App\Enums\AgentStatus;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\Factories\AgentFactory;
use Tests\Factories\CityFactory;
use Tests\TestCase;

class TableTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_has_a_name()
    {
        // Given
        $name = $this->faker->name;

        // When
        $agent = AgentFactory::new()->create([
            'name' => $name,
        ]);

        // Then
        $this->assertEquals($name, $agent->name);
    }

    /** @test */
    public function it_has_a_cellphone()
    {
        // Given
        $cellphone = $this->faker->numerify('+79#########');

        // When
        $agent = AgentFactory::new()->create([
            'cellphone' => $cellphone,
        ]);

        // Then
        $this->assertEquals($cellphone, $agent->cellphone);
    }

    /** @test */
    public function it_has_an_email()
    {
        // Given
        $email = $this->faker->safeEmail;

        // When
        $agent = AgentFactory::new()->create([
            'email' => $email,
        ]);

        // Then
        $this->assertEquals($email, $agent->email);
    }

    /** @test */
    public function an_email_must_be_unique()
    {
        // Setup
        $existingAgent = AgentFactory::new()->create();

        // Given
        $email = $existingAgent->email;

        // Except
        $this->expectException(QueryException::class);

        // When
        AgentFactory::new()->create([
            'email' => $email,
        ]);
    }

    /** @test */
    public function it_has_a_city_id()
    {
        // Setup
        $city = CityFactory::new()->create();

        // Given
        $cityId = $city->id;

        // When
        $agent = AgentFactory::new()->create([
            'city_id' => $cityId,
        ]);

        // Then
        $this->assertEquals($cityId, $agent->city_id);
    }

    /** @test */
    public function a_city_id_must_exists()
    {
        // Given
        $cityId = 0;

        // Except
        $this->expectException(QueryException::class);

        // When
        AgentFactory::new()->create([
            'city_id' => $cityId,
        ]);
    }

    /** @test */
    public function it_has_an_address()
    {
        // Given
        $address = $this->faker->address;

        // When
        $agent = AgentFactory::new()->create([
            'address' => $address,
        ]);

        // Then
        $this->assertEquals($address, $agent->address);
    }

    /** @test */
    public function it_has_a_post_code()
    {
        // Given
        $postCode = $this->faker->postcode;

        // When
        $agent = AgentFactory::new()->create([
            'post_code' => $postCode,
        ]);

        // Then
        $this->assertEquals($postCode, $agent->post_code);
    }

    /** @test */
    public function it_has_a_device_type()
    {
        // Given
        $deviceType = AgentDeviceType::getRandomValue();

        // When
        $agent = AgentFactory::new()->create([
            'device_type' => $deviceType,
        ]);

        // Then
        $this->assertTrue($agent->device_type->is($deviceType));
    }

    /** @test */
    public function it_has_a_device_uuid()
    {
        // Given
        $deviceUuid = $this->faker->uuid;

        // When
        $agent = AgentFactory::new()->create([
            'device_uuid' => $deviceUuid,
        ]);

        // Then
        $this->assertEquals($deviceUuid, $agent->device_uuid);
    }

    /** @test */
    public function it_has_a_password()
    {
        // Given
        $password = '123';
        $hashedPassword = Hash::make('123');

        // When
        $agent = AgentFactory::new()->create([
            'password' => $hashedPassword,
        ]);

        // Then
        $this->assertTrue(Hash::check($password, $hashedPassword));
        $this->assertEquals($hashedPassword, $agent->password);
    }

    /** @test */
    public function it_has_a_comment()
    {
        // Given
        $comment = $this->faker->sentence;

        // When
        $agent = AgentFactory::new()->create([
            'comment' => $comment,
        ]);

        // Then
        $this->assertEquals($comment, $agent->comment);
    }

    /** @test */
    public function it_has_a_status()
    {
        // Given
        $status = AgentStatus::getRandomValue();

        // When
        $agent = AgentFactory::new()->create([
            'status' => $status,
        ]);

        // Then
        $this->assertTrue($agent->status->is($status));
    }

    /** @test */
    public function a_status_has_default_value()
    {
        // When
        $agent = AgentFactory::new()->create();

        // Then
        $this->assertEquals(AgentStatus::PENDING, $agent->status);
    }

    /** @test */
    public function it_has_a_created_at_timestamp()
    {
        // Given
        $createdAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $createdAt);

        // When
        $agent = AgentFactory::new()->create([
            'created_at' => $createdAt,
        ])->refresh();

        // Then
        $this->assertEquals($createdAt, $agent->created_at);
        $this->assertInstanceOf(Carbon::class, $agent->created_at);
    }

    /** @test */
    public function it_has_an_updated_at_timestamp()
    {
        // Given
        $updatedAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $updatedAt);

        // When
        $agent = AgentFactory::new()->create([
            'updated_at' => $updatedAt,
        ])->refresh();

        // Then
        $this->assertEquals($updatedAt, $agent->updated_at);
        $this->assertInstanceOf(Carbon::class, $agent->updated_at);
    }

    /**
     * @test
     * @dataProvider nullableFields
     * @testdox The $fieldName field can be nullable
     */
    public function a_field_can_be_nullable(string $fieldName)
    {
        // When
        $agent = AgentFactory::new()->create([
            $fieldName => null,
        ]);

        // Then
        $this->assertNull($agent->$fieldName);
    }

    public function nullableFields()
    {
        return [
            ['address'],
            ['city_id'],
            ['comment'],
            ['created_at'],
            ['device_type'],
            ['device_uuid'],
            ['post_code'],
            ['updated_at'],
        ];
    }

    /**
     * @test
     * @dataProvider notNullableFields
     * @testdox The $fieldName field cannot be nullable
     */
    public function a_field_cannot_be_nullable(string $fieldName)
    {
        $this->expectException(QueryException::class);

        // When
        AgentFactory::new()->create([
            $fieldName => null,
        ]);
    }

    public function notNullableFields()
    {
        return [
            ['cellphone'],
            ['email'],
            ['name'],
            ['password'],
            ['status'],
        ];
    }
}
