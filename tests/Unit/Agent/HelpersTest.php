<?php

namespace Tests\Unit\Agent;

use Illuminate\Foundation\Testing\RefreshDatabase;
use InvalidArgumentException;
use Tests\Factories\AgentFactory;
use Tests\TestCase;

class HelpersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @group unit
     */
    public function it_has_a_path()
    {
        // Given
        $agent = AgentFactory::new()->create();

        $existingRoutes = ['edit', 'show'];
        $invalidRoutes = ['index', 'update', 'destroy', 'test'];

        // Then
        collect($existingRoutes)->each(function($route) use ($agent) {
            $this->assertEquals(
                route("cp.agents.{$route}", ['agent' => $agent]),
                $agent->path($route)
            );
        });

        collect($invalidRoutes)->each(function($route) use ($agent) {
            $this->expectException(InvalidArgumentException::class);
            $this->expectExceptionMessage(
                'Only edit and show routes are allowed.'
            );

            $agent->path($route);
        });
    }
}
