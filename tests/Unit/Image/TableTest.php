<?php

namespace Tests\Unit\Image;

use App\EducationStep;
use App\Enums\ImageContentType;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\EducationStepFactory;
use Tests\Factories\ImageFactory;
use Tests\TestCase;

class TableTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_has_an_imageable_type()
    {
        // Given
        $imageable = EducationStepFactory::new()->create();
        $imageableType = EducationStep::class;

        // When
        $image = ImageFactory::new()->create([
            'imageable_type' => $imageableType,
            'imageable_id' => $imageable->id,
        ]);

        // Then
        $this->assertSame($imageableType, $image->imageable_type);
    }

    /** @test */
    public function it_has_an_imageable_id()
    {
        // Given
        $imageableId = 0;

        // When
        $image = ImageFactory::new()->create([
            'imageable_id' => 0,
        ]);

        // Then
        $this->assertSame($imageableId, $image->imageable_id);
    }

    /** @test */
    public function it_has_a_disk()
    {
        // Given
        $disk = $this->faker->sentence;

        // When
        $image = ImageFactory::new()->create([
            'disk' => $disk,
        ]);

        // Then
        $this->assertSame($disk, $image->disk);
    }

    /** @test */
    public function it_has_a_path()
    {
        // Given
        $path = $this->faker->imageUrl();

        // When
        $image = ImageFactory::new()->create([
            'path' => $path,
        ]);

        // Then
        $this->assertSame($path, $image->path);
    }

    /** @test */
    public function it_has_a_content_type()
    {
        // Given
        $contentType = ImageContentType::getRandomValue();

        // When
        $image = ImageFactory::new()->create([
            'content_type' => $contentType
        ]);

        // Then
        $this->assertSame($contentType, $image->getRawOriginal('content_type'));
    }

    /** @test */
    public function it_has_a_created_at_timestamp()
    {
        // Given
        $createdAt = now()->subMinute()->startOfSecond();
        $this->assertInstanceOf(Carbon::class, $createdAt);

        // When
        $image = ImageFactory::new()->create([
            'created_at' => $createdAt,
        ]);

        // Then
        $this->assertEquals($createdAt, $image->created_at);
        $this->assertInstanceOf(Carbon::class, $image->created_at);
    }

    /** @test */
    public function it_has_an_updated_at_timestamp()
    {
        // Given
        $updatedAt = now()->subMinute()->startOfSecond();
        $this->assertInstanceOf(Carbon::class, $updatedAt);

        // When
        $image = ImageFactory::new()->create([
            'updated_at' => $updatedAt,
        ]);

        // Then
        $this->assertEquals($updatedAt, $image->updated_at);
        $this->assertInstanceOf(Carbon::class, $image->updated_at);
    }

    /**
     * @test
     * @dataProvider nullableFields
     * @testdox The $fieldName field can be nullable
     */
    public function a_field_can_be_nullable(string $fieldName)
    {
        // When
        $image = ImageFactory::new()->create([
            $fieldName => null,
        ]);

        // Then
        $this->assertNull($image->$fieldName);
    }

    public function nullableFields()
    {
        return [
            ['created_at'],
            ['updated_at'],
        ];
    }
}
