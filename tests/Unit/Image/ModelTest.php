<?php

namespace Tests\Unit\Image;

use App\Image;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Tests\Factories\ImageFactory;
use Tests\TestCase;

class ModelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_the_array_public_constant_named_MIME_TYPES()
    {
        $this->assertTrue(is_array(Image::MIME_TYPES));
    }

    /** @test */
    public function a_MIME_TYPES_array_has_image_mimetypes()
    {
        $this->assertEquals([
            'image/bmp',
            'image/gif',
            'image/jpeg',
            'image/png',
            'image/svg+xml',
            'image/webp',
        ], Image::MIME_TYPES);
    }

    /** @test */
    public function an_image_file_is_deleted_following_an_image_object()
    {
        // Setup
        $disk = 'public';
        Storage::fake($disk);

        // Given
        $fileName = 'test.jpg';
        Storage::disk($disk)->put($fileName, 'test');

        $image = ImageFactory::new()->create([
            'disk' => $disk,
            'path' => $fileName,
        ]);

        $this->assertTrue(Storage::disk($image->disk)->exists($image->path));

        // When
        $image->delete();

        // Then
        $this->assertFalse(Storage::disk($image->disk)->exists($image->path));
    }
}
