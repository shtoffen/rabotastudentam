<?php

namespace Tests\Unit\Image;

use App\EducationStep;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\EducationStepFactory;
use Tests\Factories\ImageFactory;
use Tests\TestCase;

class RelationsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_an_imageable()
    {
        // Given
        $imageable = EducationStepFactory::new()->create();
        $imageableType = EducationStep::class;

        // When
        $image = ImageFactory::new()->create([
            'imageable_type' => $imageableType,
            'imageable_id' => $imageable->id,
        ]);

        // Then
        $this->assertInstanceOf($imageableType, $image->imageable);
        $this->assertTrue($imageable->is($image->imageable));
    }
}
