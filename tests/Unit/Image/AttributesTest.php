<?php

namespace Tests\Unit\Image;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Tests\Factories\ImageFactory;
use Tests\TestCase;

class AttributesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_has_an_url()
    {
        // Given
        $disk = 'public';
        $path = $this->faker->word;

        // When
        $image = ImageFactory::new()->create([
            'disk' => $disk,
            'path' => $path,
        ]);

        // Then
        $this->assertEquals($image->url, Storage::disk($disk)->url($path));
    }
}
