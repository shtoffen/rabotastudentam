<?php

namespace Tests\Unit\StoryCategory;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\StoryCategoryFactory;
use Tests\TestCase;

class TableTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_has_a_title()
    {
        // Given
        $title = $this->faker->sentence;

        // When
        $category = StoryCategoryFactory::new()->create([
            'title' => $title,
        ]);

        // Then
        $this->assertSame($title, $category->title);
    }

    /** @test */
    public function it_has_a_created_at_timestamp()
    {
        // Given
        $createdAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $createdAt);

        // When
        $category = StoryCategoryFactory::new()->create([
            'created_at' => $createdAt,
        ]);

        // Then
        $this->assertEquals($createdAt, $category->created_at);
        $this->assertInstanceOf(Carbon::class, $category->created_at);
    }

    /** @test */
    public function it_has_an_updated_at_timestamp()
    {
        // Given
        $updatedAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $updatedAt);

        // When
        $story = StoryCategoryFactory::new()->create([
            'updated_at' => $updatedAt,
        ]);

        // Then
        $this->assertEquals($updatedAt, $story->updated_at);
        $this->assertInstanceOf(Carbon::class, $story->updated_at);
    }

    /**
     * @test
     * @dataProvider nullableFields
     * @testdox The $fieldName field can be nullable
     */
    public function a_field_can_be_nullable(string $fieldName)
    {
        // When
        $category = StoryCategoryFactory::new()->create([
            $fieldName => null,
        ]);

        // Then
        $this->assertNull($category->$fieldName);
    }

    public function nullableFields()
    {
        return [
            ['created_at'],
            ['updated_at'],
        ];
    }

    /**
     * @test
     * @dataProvider notNullableFields
     * @testdox The $fieldName field cannot be nullable
     */
    public function a_field_cannot_be_nullable(string $fieldName)
    {
        $this->expectException(QueryException::class);

        // When
        StoryCategoryFactory::new()->create([
            $fieldName => null,
        ]);
    }

    public function notNullableFields()
    {
        return [
            ['title'],
        ];
    }
}
