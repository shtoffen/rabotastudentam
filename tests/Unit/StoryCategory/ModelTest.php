<?php

namespace Tests\Unit\StoryCategory;

use App\Image;
use App\Story;
use App\StoryCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Tests\Factories\StoryCategoryFactory;
use Tests\Factories\StoryFactory;
use Tests\TestCase;

class ModelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_images_disk_constant()
    {
        $this->assertSame('public', StoryCategory::IMAGES_DISK);
    }

    /** @test */
    public function it_has_images_path_constant()
    {
        $this->assertSame('stories/categories', StoryCategory::IMAGES_PATH);
    }

    /** @test */
    public function it_deletes_related_story_and_his_image()
    {
        // Setup
        Storage::fake($storyDisk = Story::IMAGES_DISK);
        Storage::fake($categoryDisk = StoryCategory::IMAGES_DISK);

        $category = StoryCategoryFactory::new()->withImage(true)->create();
        $storyA = StoryFactory::new()
            ->forCategory($category)
            ->withImage(true)
            ->create();

        $storyB = StoryFactory::new()
            ->withImage(true)
            ->create();

        $this->assertNotSame($storyA->category_id, $storyB->category_id);

        // Given
        Storage::disk($categoryDisk)
            ->assertExists($categoryImage = $category->image->path);

        Storage::disk($storyDisk)
            ->assertExists($storyAImage = $storyA->image->path);

        Storage::disk($storyDisk)
            ->assertExists($storyBImage = $storyB->image->path);

        $categoryAttributes = $category->getAttributes();
        $storyAAttributes = $storyA->getAttributes();
        $storyBAttributes = $storyB->getAttributes();

        $this->assertDatabaseHas('story_categories', $categoryAttributes);
        $this->assertDatabaseHas('stories', $storyAAttributes);
        $this->assertDatabaseHas('stories', $storyBAttributes);
        $this->assertSame(3, Image::count());

        // When
        $category->delete();

        // Then
        $this->assertDatabaseMissing('story_categories', $categoryAttributes);
        $this->assertDatabaseMissing('stories', $storyAAttributes);
        $this->assertDatabaseHas('stories', $storyBAttributes);
        $this->assertSame(1, Image::count());

        Storage::disk($categoryDisk)->assertMissing($categoryImage);
        Storage::disk($storyDisk)->assertMissing($storyAImage);
        Storage::disk($storyDisk)->assertExists($storyBImage);
    }
}
