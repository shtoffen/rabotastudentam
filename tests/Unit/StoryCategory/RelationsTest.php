<?php

namespace Tests\Unit\StoryCategory;

use App\Enums\ImageContentType;
use App\Image;
use App\Story;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\StoryCategoryFactory;
use Tests\Factories\StoryFactory;
use Tests\TestCase;

class RelationsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function image_morphs_one_to_image()
    {
        // Given
        $category = StoryCategoryFactory::new()->withImage()->create();

        // When
        $image = $category->image;

        // Then
        $this->assertInstanceOf(Image::class, $image);
        $this->assertTrue($image->content_type->is(ImageContentType::STORY_CATEGORY));
    }

    /** @test */
    public function it_has_many_stories()
    {
        // Given
        $category = StoryCategoryFactory::new()->create();
        StoryFactory::new()->forCategory($category)->times(2);

        // When
        $stories = $category->fresh()->stories;

        // Then
        $this->assertInstanceOf(Collection::class, $stories);
        $this->assertInstanceOf(Story::class, $stories->first());
        $this->assertSame(2, $stories->count());
    }
}
