<?php

namespace Tests\Unit\StoryCategory;

use App\Enums\ImageContentType;
use App\Image;
use App\StoryCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\Factories\StoryCategoryFactory;
use Tests\TestCase;

class HelpersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @dataProvider pathsDataProvider
     */
    public function it_has_the_path(string $route, bool $allowed)
    {
        // Given
        $category = StoryCategoryFactory::new()->create();

        if (!$allowed) {
            $this->expectException(\InvalidArgumentException::class);
            $this->expectExceptionMessage('Invalid route.');
        }

        // When
        $path = $category->path($route);

        // Then
        $this->assertSame(
            route("cp.stories.categories.{$route}", ['category' => $category]),
            $path
        );
    }

    public function pathsDataProvider(): array
    {
        return [
            ['index',       false],
            ['create',      false],
            ['store',       false],
            ['show',        true],
            ['edit',        true],
            ['update',      false],
            ['destroy',     false],
        ];
    }

    /** @test */
    public function the_store_image_does_nothing_if_file_not_specified()
    {
        // Setup
        Storage::fake($disk = StoryCategory::IMAGES_DISK);
        $category = StoryCategoryFactory::new()->withImage(true)->create();

        // Given
        $this->assertSame(1, Image::count());
        $this->assertTrue($category->image->content_type->is(ImageContentType::STORY_CATEGORY));
        Storage::disk($disk)->assertExists($oldFile = $category->image->path);

        // When
        $category->storeImage();

        // Then
        $category->refresh();
        $this->assertSame(1, Image::count());
        $this->assertTrue($category->image->content_type->is(ImageContentType::STORY_CATEGORY));
        Storage::disk($disk)->assertExists($oldFile);
    }

    /** @test */
    public function the_store_image_create_new_image()
    {
        // Setup
        Storage::fake($disk = StoryCategory::IMAGES_DISK);
        $category = StoryCategoryFactory::new()->create();

        // Given
        $this->assertSame(0, Image::count());

        // When
        $category->storeImage(UploadedFile::fake()->image('test.jpg'));

        // Then
        $category->refresh();
        $this->assertSame(1, Image::count());
        $this->assertTrue($category->image->content_type->is(ImageContentType::STORY_CATEGORY));
        Storage::disk($disk)->assertExists($category->image->path);
    }

    /** @test */
    public function the_store_image_replace_old_image()
    {
        // Setup
        Storage::fake($disk = StoryCategory::IMAGES_DISK);
        $category = StoryCategoryFactory::new()->withImage(true)->create();

        // Given
        $this->assertSame(1, Image::count());
        $this->assertTrue($category->image->content_type->is(ImageContentType::STORY_CATEGORY));
        Storage::disk($disk)->assertExists($oldFile = $category->image->path);

        // When
        $category->storeImage(UploadedFile::fake()->image('test.jpg'));

        // Then
        $category->refresh();
        $this->assertSame(1, Image::count());
        $this->assertTrue($category->image->content_type->is(ImageContentType::STORY_CATEGORY));
        Storage::disk($disk)->assertMissing($oldFile);
        Storage::disk($disk)->assertExists($category->image->path);
    }
}
