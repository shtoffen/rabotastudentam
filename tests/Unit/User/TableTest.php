<?php

namespace Tests\Unit\User;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\UserFactory;
use Tests\TestCase;

class TableTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_has_a_name()
    {
        // Given
        $name = $this->faker->name;

        // When
        $user = UserFactory::new()->create([
            'name' => $name,
        ]);

        // Then
        $this->assertEquals($name, $user->name);
    }

    /** @test */
    public function it_has_an_email()
    {
        // Given
        $email = $this->faker->email;

        // When
        $user = UserFactory::new()->create([
            'email' => $email,
        ]);

        // Then
        $this->assertEquals($email, $user->email);
    }

    /** @test */
    public function it_has_an_email_verified_at()
    {
        // Given
        $emailVerifiedAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $emailVerifiedAt);

        // When
        $user = UserFactory::new()->create([
            'email_verified_at' => $emailVerifiedAt,
        ]);

        // Then
        $this->assertEquals($emailVerifiedAt, $user->email_verified_at);
        $this->assertInstanceOf(Carbon::class, $user->email_verified_at);
    }

    /** @test */
    public function it_has_a_password()
    {
        // Given
        $password = $this->faker->password;

        // When
        $user = UserFactory::new()->create([
            'password' => $password,
        ]);

        // Then
        $this->assertEquals($password, $user->password);
    }

    /** @test */
    public function it_has_a_remember_token()
    {
        // Given
        $rememberToken = $this->faker->sentence;

        // When
        $user = UserFactory::new()->create([
            'remember_token' => $rememberToken,
        ]);

        // Then
        $this->assertEquals($rememberToken, $user->remember_token);
    }

    /** @test */
    public function it_has_a_created_at_timestamp()
    {
        // Given
        $createdAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $createdAt);

        // When
        $user = UserFactory::new()->create([
            'created_at' => $createdAt,
        ]);

        // Then
        $this->assertEquals($createdAt, $user->created_at);
        $this->assertInstanceOf(Carbon::class, $user->created_at);
    }

    /** @test */
    public function it_has_an_updated_at_timestamp()
    {
        // Given
        $updatedAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $updatedAt);

        // When
        $user = UserFactory::new()->create([
            'updated_at' => $updatedAt,
        ]);

        // Then
        $this->assertEquals($updatedAt, $user->updated_at);
        $this->assertInstanceOf(Carbon::class, $user->updated_at);
    }

    /**
     * @test
     * @dataProvider nullableFields
     * @testdox The $fieldName field can be nullable
     */
    public function a_field_can_be_nullable(string $fieldName)
    {
        // When
        $user = UserFactory::new()->create([
            $fieldName => null,
        ]);

        // Then
        $this->assertNull($user->$fieldName);
    }

    public function nullableFields()
    {
        return [
            ['email_verified_at'],
            ['created_at'],
            ['remember_token'],
            ['updated_at'],
        ];
    }

    /**
     * @test
     * @dataProvider notNullableFields
     * @testdox The $fieldName field cannot be nullable
     */
    public function a_field_cannot_be_nullable(string $fieldName)
    {
        $this->expectException(QueryException::class);

        // When
        UserFactory::new()->create([
            $fieldName => null,
        ]);
    }

    public function notNullableFields()
    {
        return [
            ['email'],
            ['name'],
            ['password'],
        ];
    }
}
