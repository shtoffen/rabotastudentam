<?php

namespace Tests\Unit\FcmToken;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\AgentFactory;
use Tests\Factories\FcmTokenFactory;
use Tests\TestCase;

class TableTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_has_an_agent_id()
    {
        // Given
        $agent = AgentFactory::new()->create();

        // When
        $fcmToken = FcmTokenFactory::new()->create([
            'agent_id' => $agent->id,
        ]);

        // Then
        $this->assertSame($agent->id, $fcmToken->agent_id);
    }

    /** @test */
    public function it_has_a_token()
    {
        // Given
        $token = $this->faker->uuid;

        // When
        $fcmToken = FcmTokenFactory::new()->create([
            'token' => $token,
        ]);

        // Then
        $this->assertSame($token, $fcmToken->token);
    }

    /** @test */
    public function it_has_a_created_at_timestamp()
    {
        // Given
        $createdAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $createdAt);

        // When
        $fcmToken = FcmTokenFactory::new()->create([
            'created_at' => $createdAt,
        ]);

        // Then
        $this->assertEquals($createdAt, $fcmToken->created_at);
        $this->assertInstanceOf(Carbon::class, $fcmToken->created_at);
    }

    /** @test */
    public function it_has_an_updated_at_timestamp()
    {
        // Given
        $updatedAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $updatedAt);

        // When
        $fcmToken = FcmTokenFactory::new()->create([
            'updated_at' => $updatedAt,
        ]);

        // Then
        $this->assertEquals($updatedAt, $fcmToken->updated_at);
        $this->assertInstanceOf(Carbon::class, $fcmToken->updated_at);
    }

    /**
     * @test
     * @dataProvider foreignFields
     * @testdox The $fieldName has foreign key
     */
    public function it_has_foreign_agent_id(string $fieldName)
    {
        // Expect
        $this->expectException(QueryException::class);
        $this->expectExceptionMessageMatches("/FOREIGN KEY constraint failed?\w+/");

        // When
        FcmTokenFactory::new()->create([
            $fieldName => 0,
        ]);
    }

    public function foreignFields()
    {
        return [
            ['agent_id'],
        ];
    }

    /**
     * @test
     * @dataProvider nullableFields
     * @testdox The $fieldName field can be nullable
     */
    public function a_field_can_be_nullable(string $fieldName)
    {
        // When
        $fcmToken = FcmTokenFactory::new()->create([
            $fieldName => null,
        ]);

        // Then
        $this->assertNull($fcmToken->$fieldName);
    }

    public function nullableFields()
    {
        return [
            ['created_at'],
            ['updated_at'],
        ];
    }

    /**
     * @test
     * @dataProvider notNullableFields
     * @testdox The $fieldName field cannot be nullable
     */
    public function a_field_cannot_be_nullable(string $fieldName)
    {
        // Given
        $this->expectException(QueryException::class);
        $this->expectExceptionMessageMatches('/NOT NULL constraint failed?\w+/');

        // When
        FcmTokenFactory::new()->create([
            $fieldName => null,
        ]);
    }

    public function notNullableFields()
    {
        return [
            ['agent_id'],
            ['token'],
        ];
    }
}
