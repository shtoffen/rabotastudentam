<?php

namespace Tests\Unit\FcmToken;

use App\Agent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\AgentFactory;
use Tests\Factories\FcmTokenFactory;
use Tests\TestCase;

class RelationsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function the_agent_belongs_to_agent_model()
    {
        // Given
        $agent = AgentFactory::new()->create();

        // When
        $fcmToken = FcmTokenFactory::new()->create([
            'agent_id' => $agent->id,
        ]);

        // Then
        $this->assertInstanceOf(Agent::class, $fcmToken->agent);
        $this->assertTrue($agent->is($fcmToken->agent));
    }
}
