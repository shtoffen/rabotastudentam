<?php

namespace Tests\Unit\Enums;

use App\Enums\AgentStatus;
use Tests\TestCase;

class AgentStatusTest extends TestCase
{
    /**
     * @test
     * @group unit
     */
    public function there_is_a_fixed_set_of_agent_statuses()
    {
        // Given
        $set = [
            'PENDING',
            'EMPLOYED',
            'REJECTED',
        ];

        // When
        $keys = AgentStatus::getKeys();

        // Then
        $this->assertEquals($set, $keys);
    }

    /**
     * @test
     * @group unit
     */
    public function all_values_are_between_0_and_255()
    {
        // Given
        $values = AgentStatus::getValues();

        // Then
        collect($values)->each(function ($item) {
            $this->assertTrue(is_integer($item));
            $this->assertTrue($item >= 0);
            $this->assertTrue($item <= 255);
        });
    }

    /**
     * @test
     * @group unit
     */
    public function it_has_russian_localization_by_default()
    {
        // Given
        $translations = [
            AgentStatus::PENDING    => 'Кандидат',
            AgentStatus::EMPLOYED   => 'Сотрудник',
            AgentStatus::REJECTED   => 'Отклоненный',
        ];

        // Then
        collect($translations)->each(function ($translation, $status) {
            $this->assertEquals(
                $translation,
                AgentStatus::getDescription($status)
            );
        });
    }
}
