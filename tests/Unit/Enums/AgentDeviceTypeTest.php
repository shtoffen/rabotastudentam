<?php

namespace Tests\Unit\Enums;

use App\Enums\AgentDeviceType;
use Tests\TestCase;

class AgentDeviceTypeTest extends TestCase
{
    /**
     * @test
     * @group unit
     */
    public function there_is_a_fixed_set_of_agent_device_types()
    {
        // Given
        $set = [
            'IOS',
            'ANDROID',
        ];

        // When
        $keys = AgentDeviceType::getKeys();

        // Then
        $this->assertEquals($set, $keys);
    }

    /**
     * @test
     * @group unit
     */
    public function all_values_are_between_0_and_255()
    {
        // Given
        $values = AgentDeviceType::getValues();

        // Then
        collect($values)->each(function ($item) {
            $this->assertTrue(is_integer($item));
            $this->assertTrue($item >= 0);
            $this->assertTrue($item <= 255);
        });
    }

    /**
     * @test
     * @group unit
     */
    public function it_has_localization_by_default()
    {
        // Given
        $translations = [
            AgentDeviceType::IOS        => 'iOS',
            AgentDeviceType::ANDROID    => 'Android',
        ];

        // Then
        collect($translations)->each(function ($translation, $status) {
            $this->assertEquals(
                $translation,
                AgentDeviceType::getDescription($status)
            );
        });
    }
}
