<?php

namespace Tests\Unit\Story;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\StoryCategoryFactory;
use Tests\Factories\StoryFactory;
use Tests\TestCase;

class TableTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_has_a_category_id()
    {
        // Given
        $category = StoryCategoryFactory::new()->create();

        // When
        $story = StoryFactory::new()->create([
            'category_id' => $category->id,
        ]);

        // Then
        $this->assertSame($category->id, $story->category_id);
    }

    /** @test */
    public function it_has_a_title()
    {
        // Given
        $title = $this->faker->sentence;

        // When
        $story = StoryFactory::new()->create([
            'title' => $title,
        ]);

        // Then
        $this->assertSame($title, $story->title);
    }

    /** @test */
    public function it_has_a_description()
    {
        // Given
        $description = $this->faker->text;

        // When
        $story = StoryFactory::new()->create([
            'description' => $description,
        ]);

        // Then
        $this->assertSame($description, $story->description);
    }

    /** @test */
    public function it_has_a_created_at_timestamp()
    {
        // Given
        $createdAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $createdAt);

        // When
        $story = StoryFactory::new()->create([
            'created_at' => $createdAt,
        ]);

        // Then
        $this->assertEquals($createdAt, $story->created_at);
        $this->assertInstanceOf(Carbon::class, $story->created_at);
    }

    /** @test */
    public function it_has_an_updated_at_timestamp()
    {
        // Given
        $updatedAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $updatedAt);

        // When
        $story = StoryFactory::new()->create([
            'updated_at' => $updatedAt,
        ]);

        // Then
        $this->assertEquals($updatedAt, $story->updated_at);
        $this->assertInstanceOf(Carbon::class, $story->updated_at);
    }

    /**
     * @test
     * @dataProvider nullableFields
     * @testdox The $fieldName field can be nullable
     */
    public function a_field_can_be_nullable(string $fieldName)
    {
        // When
        $story = StoryFactory::new()->create([
            $fieldName => null,
        ]);

        // Then
        $this->assertNull($story->$fieldName);
    }

    public function nullableFields()
    {
        return [
            ['created_at'],
            ['updated_at'],
        ];
    }

    /**
     * @test
     * @dataProvider notNullableFields
     * @testdox The $fieldName field cannot be nullable
     */
    public function a_field_cannot_be_nullable(string $fieldName)
    {
        $this->expectException(QueryException::class);

        // When
        StoryFactory::new()->create([
            $fieldName => null,
        ]);
    }

    public function notNullableFields()
    {
        return [
            ['category_id'],
            ['description'],
            ['title'],
        ];
    }
}
