<?php

namespace Tests\Unit\Story;

use App\Story;
use PHPUnit\Framework\TestCase;

class ModelTest extends TestCase
{
    /** @test */
    public function it_has_images_disk_constant()
    {
        $this->assertSame('public', Story::IMAGES_DISK);
    }

    /** @test */
    public function it_has_images_path_constant()
    {
        $this->assertSame('stories/items', Story::IMAGES_PATH);
    }
}
