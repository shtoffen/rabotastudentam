<?php

namespace Tests\Unit\Story;

use App\Enums\ImageContentType;
use App\Image;
use App\StoryCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\StoryFactory;
use Tests\TestCase;

class RelationsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function image_morphs_one_to_image()
    {
        // Given
        $story = StoryFactory::new()->withImage()->create();

        // When
        $image = $story->image;

        // Then
        $this->assertInstanceOf(Image::class, $image);
        $this->assertTrue($image->content_type->is(ImageContentType::STORY));
    }

    /** @test */
    public function category_id_belongs_to_category()
    {
        // Given
        $story = StoryFactory::new()->create();

        // When
        $category = $story->category;

        // Then
        $this->assertInstanceOf(StoryCategory::class, $category);
    }
}
