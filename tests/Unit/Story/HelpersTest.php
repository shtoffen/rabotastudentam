<?php

namespace Tests\Unit\Story;

use App\Enums\ImageContentType;
use App\Image;
use App\StoryCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\Factories\StoryFactory;
use Tests\TestCase;

class HelpersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @dataProvider pathsDataProvider
     */
    public function it_has_the_path(string $route, bool $allowed)
    {
        // Given
        $story = StoryFactory::new()->create();

        if (!$allowed) {
            $this->expectException(\InvalidArgumentException::class);
            $this->expectExceptionMessage('Invalid route.');
        }

        // When
        $path = $story->path($route);

        // Then
        $this->assertSame(
            route("cp.stories.{$route}", ['story' => $story]),
            $path
        );
    }

    public function pathsDataProvider(): array
    {
        return [
            ['index',       false],
            ['create',      false],
            ['store',       false],
            ['show',        false],
            ['edit',        true],
            ['update',      true],
            ['destroy',     true],
        ];
    }

    /** @test */
    public function the_store_image_does_nothing_if_file_not_specified()
    {
        // Setup
        Storage::fake($disk = StoryCategory::IMAGES_DISK);
        $story = StoryFactory::new()->withImage(true)->create();

        // Given
        $this->assertSame(1, Image::count());
        $this->assertTrue($story->image->content_type->is(ImageContentType::STORY));
        Storage::disk($disk)->assertExists($oldFile = $story->image->path);

        // When
        $story->storeImage();

        // Then
        $story->refresh();
        $this->assertSame(1, Image::count());
        $this->assertTrue($story->image->content_type->is(ImageContentType::STORY));
        Storage::disk($disk)->assertExists($oldFile);
    }

    /** @test */
    public function the_store_image_create_new_image()
    {
        // Setup
        Storage::fake($disk = StoryCategory::IMAGES_DISK);
        $story = StoryFactory::new()->create();

        // Given
        $this->assertSame(0, Image::count());

        // When
        $story->storeImage(UploadedFile::fake()->image('test.jpg'));

        // Then
        $story->refresh();
        $this->assertSame(1, Image::count());
        $this->assertTrue($story->image->content_type->is(ImageContentType::STORY));
        Storage::disk($disk)->assertExists($story->image->path);
    }

    /** @test */
    public function the_store_image_replace_old_image()
    {
        // Setup
        Storage::fake($disk = StoryCategory::IMAGES_DISK);
        $story = StoryFactory::new()->withImage(true)->create();

        // Given
        $this->assertSame(1, Image::count());
        $this->assertTrue($story->image->content_type->is(ImageContentType::STORY));
        Storage::disk($disk)->assertExists($oldFile = $story->image->path);

        // When
        $story->storeImage(UploadedFile::fake()->image('test.jpg'));

        // Then
        $story->refresh();
        $this->assertSame(1, Image::count());
        $this->assertTrue($story->image->content_type->is(ImageContentType::STORY));
        Storage::disk($disk)->assertMissing($oldFile);
        Storage::disk($disk)->assertExists($story->image->path);
    }
}
