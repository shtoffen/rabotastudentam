<?php

namespace Tests\Unit\City;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\CityFactory;
use Tests\TestCase;

class HelpersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_a_path()
    {
        // Given
        $city = CityFactory::new()->create();

        $existingRoutes = ['edit', 'update', 'destroy'];
        $invalidRoutes = ['index', 'show', 'test'];

        // Then
        collect($existingRoutes)->each(function($route) use ($city) {
            $this->assertEquals(
                route("cp.cities.{$route}", ['city' => $city]),
                $city->path($route)
            );
        });

        collect($invalidRoutes)->each(function($route) use ($city) {
            $this->expectException(\InvalidArgumentException::class);
            $this->expectExceptionMessage(
                'Only edit, update and delete routes are allowed.'
            );

            $city->path($route);
        });
    }

    /** @test */
    public function it_has_an_api_path()
    {
        // Given
        $city = CityFactory::new()->create();

        // When
        $apiPath = $city->apiPath();

        // Then
        $this->assertSame(
            route('api.cities.show', ['city' => $city]),
            $apiPath
        );
    }
}
