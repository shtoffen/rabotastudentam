<?php

namespace Tests\Unit\City;

use App\Agent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Tests\Factories\AgentFactory;
use Tests\Factories\CityFactory;
use Tests\TestCase;

class RelationsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_many_agents()
    {
        // Given
        $city = CityFactory::new()->create();

        // When
        AgentFactory::new()->withCity($city)->times(2);

        // Then
        $this->assertInstanceOf(Collection::class, $city->agents);
        $this->assertInstanceOf(Agent::class, $city->agents->first());
    }
}
