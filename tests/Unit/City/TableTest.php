<?php

namespace Tests\Unit\City;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\CityFactory;
use Tests\TestCase;

class TableTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_has_a_name()
    {
        // Given
        $name = $this->faker->city;

        // When
        $city = CityFactory::new()->create([
            'name' => $name,
        ]);

        // Then
        $this->assertSame($name, $city->name);
    }

    /** @test */
    public function it_has_a_created_at_timestamp()
    {
        // Given
        $createdAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $createdAt);

        // When
        $city = CityFactory::new()->create([
            'created_at' => $createdAt,
        ]);

        // Then
        $this->assertEquals($createdAt, $city->created_at);
        $this->assertInstanceOf(Carbon::class, $city->created_at);
    }

    /** @test */
    public function it_has_an_updated_at_timestamp()
    {
        // Given
        $updatedAt = now()->subMinute()->roundSecond();
        $this->assertInstanceOf(Carbon::class, $updatedAt);

        // When
        $city = CityFactory::new()->create([
            'updated_at' => $updatedAt,
        ]);

        // Then
        $this->assertEquals($updatedAt, $city->updated_at);
        $this->assertInstanceOf(Carbon::class, $city->updated_at);
    }

    /**
     * @test
     * @dataProvider nullableFields
     * @testdox The $fieldName field can be nullable
     */
    public function a_field_can_be_nullable(string $fieldName)
    {
        // When
        $city = CityFactory::new()->create([
            $fieldName => null,
        ]);

        // Then
        $this->assertNull($city->$fieldName);
    }

    public function nullableFields()
    {
        return [
            ['created_at'],
            ['updated_at'],
        ];
    }

    /**
     * @test
     * @dataProvider notNullableFields
     * @testdox The $fieldName field cannot be nullable
     */
    public function a_field_cannot_be_nullable(string $fieldName)
    {
        $this->expectException(QueryException::class);

        // When
        CityFactory::new()->create([
            $fieldName => null,
        ]);
    }

    public function notNullableFields()
    {
        return [
            ['name'],
        ];
    }
}
