<?php

namespace Tests\Unit\Notifications;

use App\Enums\AgentStatus;
use App\Notifications\AgentRejectedNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use NotificationChannels\Fcm\FcmChannel;
use Tests\Factories\AgentFactory;
use Tests\TestCase;

class AgentRejectedNotificationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_specified_notifications_channels()
    {
        // Given
        $notifiable = AgentFactory::new()->make();

        // When
        $notification = new AgentRejectedNotification();
        $channels = $notification->via($notifiable);

        // Then
        $this->assertIsArray($channels);
        $this->assertCount(1, $channels);
        $this->assertSame(FcmChannel::class, $channels[0]);
    }

    /** @test */
    public function it_has_specified_fcm_notification()
    {
        // Given
        $notifiable = AgentFactory::new()->make();

        // When
        $notification = new AgentRejectedNotification();
        $fcmMessage = $notification->toFcm($notifiable);
        $fcmNotification = $fcmMessage->getNotification();

        // Then
        $title = "Ваша заявка отклонена.";
        $this->assertSame($title, $fcmNotification->getTitle());

        $body = "Здравствуйте, к сожалению, ваша заявка на работу отклонена.";
        $this->assertSame($body, $fcmNotification->getBody());
    }

    /** @test */
    public function it_has_specified_fcm_data()
    {
        // Given
        $notifiable = AgentFactory::new()->make();

        // When
        $notification = new AgentRejectedNotification();
        $fcmMessage = $notification->toFcm($notifiable);
        $fcmData = $fcmMessage->getData();

        // Then
        $this->assertSame([
            'category' => 'status_changed',
            'status_id' => (string) AgentStatus::REJECTED()->value,
            'status_name' => AgentStatus::REJECTED()->description,
        ], $fcmData);
    }

    /** @test */
    public function it_has_specified_job_tags()
    {
        // When
        $notification = new AgentRejectedNotification();
        $tags = $notification->tags();

        // Then
        $this->assertIsArray($tags);
        $this->assertCount(1, $tags);
        $this->assertSame('push_notification', $tags[0]);
    }
}
