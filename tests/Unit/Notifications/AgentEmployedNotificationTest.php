<?php

namespace Tests\Unit\Notifications;

use App\Enums\AgentStatus;
use App\Notifications\AgentEmployedNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use NotificationChannels\Fcm\FcmChannel;
use Tests\Factories\AgentFactory;
use Tests\TestCase;

class AgentEmployedNotificationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_specified_notifications_channels()
    {
        // Given
        $notifiable = AgentFactory::new()->make();

        // When
        $notification = new AgentEmployedNotification();
        $channels = $notification->via($notifiable);

        // Then
        $this->assertIsArray($channels);
        $this->assertCount(1, $channels);
        $this->assertSame(FcmChannel::class, $channels[0]);
    }

    /** @test */
    public function it_has_specified_fcm_notification()
    {
        // Given
        $notifiable = AgentFactory::new()->make();

        // When
        $notification = new AgentEmployedNotification();
        $fcmMessage = $notification->toFcm($notifiable);
        $fcmNotification = $fcmMessage->getNotification();

        // Then
        $title = "Ваша заявка принята.";
        $this->assertSame($title, $fcmNotification->getTitle());

        $body = "Здравствуйте! Ваша заявка на работу согласована, просим начать обучение в мобильном приложении.";
        $this->assertSame($body, $fcmNotification->getBody());
    }

    /** @test */
    public function it_has_specified_fcm_data()
    {
        // Given
        $notifiable = AgentFactory::new()->make();

        // When
        $notification = new AgentEmployedNotification();
        $fcmMessage = $notification->toFcm($notifiable);
        $fcmData = $fcmMessage->getData();

        // Then
        $this->assertSame([
            'category' => 'status_changed',
            'status_id' => (string) AgentStatus::EMPLOYED()->value,
            'status_name' => AgentStatus::EMPLOYED()->description,
        ], $fcmData);
    }

    /** @test */
    public function it_has_specified_job_tags()
    {
        // When
        $notification = new AgentEmployedNotification();
        $tags = $notification->tags();

        // Then
        $this->assertIsArray($tags);
        $this->assertCount(1, $tags);
        $this->assertSame('push_notification', $tags[0]);
    }
}
