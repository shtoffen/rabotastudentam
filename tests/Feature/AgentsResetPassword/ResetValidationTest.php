<?php

namespace Tests\Feature\AgentsResetPassword;

use App\Agent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\AgentFactory;
use Tests\TestCase;

class ResetValidationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected Agent $agent;

    protected function setUp(): void
    {
        parent::setUp();

        $this->agent = AgentFactory::new()->create();
    }

    /** @test */
    public function it_requires_an_email()
    {
        // Given
        $attributes = [
            'email' => null,
        ];

        // When
        $response = $this->post(
            route('agents.password.update', ['token' => '123']),
            $attributes
        );

        // Then
        $response->assertSessionHasErrors([
            'email' => 'The email field is required.'
        ]);
    }

    /** @test */
    public function it_requires_an_email_to_be_email()
    {
        // Given
        $attributes = [
            'email' => $this->faker->sentence,
        ];

        // When
        $response = $this->post(
            route('agents.password.update', ['token' => '123']),
            $attributes
        );

        // Then
        $response->assertSessionHasErrors([
            'email' => 'The email must be a valid email address.'
        ]);
    }

    /** @test */
    public function it_requires_an_existing_email()
    {
        // Given
        $email = 'test@example.com';
        $password = $this->faker->password(8);
        $token = $this->faker->regexify('[a-z]{40}');

        $attributes = [
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $password,
            'token' => $token,
        ];

        $this->assertDatabaseMissing('agents', compact('email'));
        $this->assertDatabaseMissing('agents_password_resets', compact('email'));

        // When
        $response = $this->post(
            route('agents.password.update', ['token' => '123']),
            $attributes
        );

        // Then
        $response->assertSessionHasErrors([
            'email' => 'We can\'t find a user with that e-mail address.'
        ]);
    }

    /** @test */
    public function it_requires_a_password()
    {
        // Given
        $attributes = [
            'password' => null,
        ];

        // When
        $response = $this->post(
            route('agents.password.update', ['token' => '123']),
            $attributes
        );

        // Then
        $response->assertSessionHasErrors([
            'password' => 'The password field is required.'
        ]);
    }

    /** @test */
    public function it_requires_a_password_to_be_confirmed()
    {
        // Given
        $attributes = [
            'password' => '123',
            'password_confirmation' => '456',
        ];

        // When
        $response = $this->post(
            route('agents.password.update', ['token' => '123']),
            $attributes
        );

        // Then
        $response->assertSessionHasErrors([
            'password' => 'The password confirmation does not match.'
        ]);
    }

    /** @test */
    public function it_requires_a_password_to_be_lte_8()
    {
        // Given
        $attributes = [
            'password' => '123',
        ];

        // When
        $response = $this->post(
            route('agents.password.update', ['token' => '123']),
            $attributes
        );

        // Then
        $response->assertSessionHasErrors([
            'password' => 'The password must be at least 8 characters.'
        ]);
    }

    /** @test */
    public function it_requires_a_token()
    {
        // Given
        $attributes = [
            'token' => null,
        ];

        // When
        $response = $this->post(
            route('agents.password.update', ['token' => '123']),
            $attributes
        );

        // Then
        $response->assertSessionHasErrors([
            'token' => 'The token field is required.'
        ]);
    }

    /** @test */
    public function it_requires_an_existing_token()
    {
        // Given
        $password = $this->faker->password(8);
        $token = $this->faker->regexify('[a-z]{40}');

        $attributes = [
            'email' => $this->agent->email,
            'password' => $password,
            'password_confirmation' => $password,
            'token' => $token,
        ];

        // When
        $response = $this->post(
            route('agents.password.update', ['token' => '123']),
            $attributes
        );

        // Then
        $response->assertSessionHasErrors([
            'email' => 'This password reset token is invalid.'
        ]);
    }
}
