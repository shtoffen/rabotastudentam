<?php

namespace Tests\Feature\AgentsResetPassword;

use App\Agent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\Factories\AgentFactory;
use Tests\TestCase;

class ResetTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected Agent $agent;

    protected function setUp(): void
    {
        parent::setUp();

        $this->agent = AgentFactory::new()->create();
    }

    /** @test */
    public function it_has_the_reset_form()
    {
        // Given
        $token = $this->faker->regexify('[a-z]{40}');

        // When
        $response = $this->get(route('agents.password.reset', [
            'email' => $this->agent->email,
            'token' => $token,
        ]));

        // Then
        $response->assertOk()
            ->assertViewIs('agents_password_reset.form')
            ->assertViewHas([
                'email' => $this->agent->email,
                'token' => $token,
            ]);
    }

    /** @test */
    public function it_resets_a_password()
    {
        // Given
        $password = $this->faker->password(8);
        $token = $this->getPasswordResetToken($this->agent->email);

        $attributes = [
            'email' => $this->agent->email,
            'password' => $password,
            'password_confirmation' => $password,
            'token' => $token,
        ];

        $this->assertFalse(
            Hash::check($password, $this->agent->password)
        );

        // When
        $response = $this->post(
            route('agents.password.update', ['token' => $token]),
            $attributes
        );

        // Then
        $response->assertSessionDoesntHaveErrors()
            ->assertRedirect(route('agents.password.reset.success'));

        $this->assertTrue(
            Hash::check($password, $this->agent->fresh()->password)
        );
    }

    /** @test */
    public function it_has_password_reseted_page()
    {
        // When
        $response = $this->get(route('agents.password.reset.success'));

        // Then
        $response->assertOk()
            ->assertViewIs('agents_password_reset.success')
            ->assertViewHas('message', 'Пароль успешно изменен.');
    }

    /**
     * Mock password reset request.
     *
     * @param string $email
     * @return void
     */
    protected function getPasswordResetToken(string $email)
    {
        $token = Str::random(40);

        DB::table('agents_password_resets')->insert([
            'email' => $email,
            'token' => bcrypt($token),
            'created_at' => now(),
        ]);

        return $token;
    }
}
