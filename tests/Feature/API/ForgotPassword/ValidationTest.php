<?php

namespace Tests\Feature\API\ForgotPassword;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ValidationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected string $url;

    protected function setUp(): void
    {
        parent::setUp();

        $this->url = route('api.agents.reset_password');
    }

    /** @test */
    public function it_requires_an_email()
    {
        // Given
        $attributes = [
            'email' => null,
        ];

        // When
        $response = $this->post($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'email' => 'The email field is required.',
        ]);
    }

    /** @test */
    public function it_requires_an_email_to_be_email()
    {
        // Given
        $attributes = [
            'email' => $this->faker->sentence,
        ];

        // When
        $response = $this->post($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'email' => 'The email must be a valid email address.',
        ]);
    }

    /** @test */
    public function it_requires_an_existing_email()
    {
        // Given
        $attributes = [
            'email' => $this->faker->safeEmail,
        ];

        // When
        $response = $this->post($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'email' => 'The selected email is invalid.',
        ]);
    }
}
