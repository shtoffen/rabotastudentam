<?php

namespace Tests\Feature\API\ForgotPassword;

use App\Agent;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Tests\Factories\AgentFactory;
use Tests\TestCase;

class SendResetLinkTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected Agent $agent;
    protected array $attibutes;
    protected string $url;

    protected function setUp(): void
    {
        parent::setUp();

        Mail::fake();
        Notification::fake();

        $this->agent = AgentFactory::new()->create();

        $this->attributes = [
            'email' => $this->agent->email
        ];

        $this->url = route('api.agents.reset_password');
    }

    /** @test */
    public function it_creates_a_record_in_the_database()
    {
        // Given
        $this->assertDatabaseMissing('agents_password_resets', $this->attributes);

        // When
        $response = $this->post($this->url, $this->attributes);

        // Then
        $response->assertOk();
        $this->assertDatabaseHas('agents_password_resets', $this->attributes);
    }

    /** @test */
    public function it_sends_reset_password_notification_to_agent()
    {
        // Given
        Notification::assertNothingSent();

        // When
        $response = $this->post($this->url, $this->attributes);

        // Then
        $response->assertOk()->assertJson([
            'message' => 'We have e-mailed your password reset link!',
        ]);

        Notification::assertSentTo(
            $this->agent,
            ResetPasswordNotification::class,
            function ($notification, $channels, $notifiable){
                return $notifiable->email === $this->agent->email;
            }
        );
    }

    /** @test */
    public function it_notifies_via_mail_channel()
    {
        // Given
        Notification::assertNothingSent();

        // When
        $response = $this->post($this->url, $this->attributes);

        // Then
        $response->assertOk();
        Notification::assertSentTo(
            $this->agent,
            ResetPasswordNotification::class,
            function ($notification, $channels, $notifiable){
                return in_array('mail', $channels);
            }
        );
    }

    /** @test */
    public function it_cannot_notify_twice()
    {
        // When
        $this->post($this->url, $this->attributes);
        $response = $this->post($this->url, $this->attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson(['message' => 'Please wait before retrying.']);
    }
}
