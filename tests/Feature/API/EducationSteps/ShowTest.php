<?php

namespace Tests\Feature\API\Education\Steps;

use App\EducationStep;
use App\Enums\AgentStatus;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DataProvidersTestCase;
use Tests\Factories\AgentFactory;
use Tests\Factories\EducationStepFactory;

class ShowTest extends DataProvidersTestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_not_allowed_to_guest()
    {
        // Setup
        $step = EducationStepFactory::new()->create();

        // Given
        $this->assertGuest('api');

        // When
        $response = $this->getJson($step->apiPath());

        // Then
        $response->assertUnauthorized();
    }

    /**
     * @test
     * @dataProvider agentStatusesDataProvider
     */
    public function it_allowed_only_to_specified_agent_statuses(
        int $status,
        bool $allowed
    ) {
        // Setup
        $step = EducationStepFactory::new()->create();
        $agent = AgentFactory::new()->create([
            'status' => $status,
        ]);

        // Given
        $this->signInApi($agent);

        // When
        $response = $this->getJson($step->apiPath());

        // Then
        $allowed
            ? $this->assertFalse($response->isForbidden())
            : $this->assertTrue($response->isForbidden());
    }

    /** @test */
    public function it_has_the_card_of_step()
    {
        // Setup
        $agent = AgentFactory::new()
            ->employed()
            ->withEducationSteps(1)
            ->create();

        // Given
        $step = $agent->educationSteps()->first();

        // When
        $this->signInApi($agent);
        $response = $this->getJson($step->apiPath());

        // Then
        $response->assertOk()
            ->assertJsonPath('data', $this->getJsonStep($step));
    }

    protected function getJsonStep(EducationStep $educationStep)
    {
        return [
            'id'            => $educationStep->id,
            'description'   => $educationStep->description,
            'name'          => $educationStep->name,
            'read'          => $educationStep->pivot->read,
        ];
    }

    public function agentStatusesDataProvider(): array
    {
        return $this->getSetOfPermissions(AgentStatus::class, [
            AgentStatus::EMPLOYED(),
        ]);
    }
}
