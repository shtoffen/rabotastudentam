<?php

namespace Tests\Feature\API\Education\Steps;

use App\EducationStep;
use App\Enums\AgentStatus;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DataProvidersTestCase;
use Tests\Factories\AgentFactory;
use Tests\Factories\EducationStepFactory;

class ReadTest extends DataProvidersTestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_not_allowed_to_guest()
    {
        // Setup
        $step = EducationStepFactory::new()->create();

        // Given
        $this->assertGuest('api');

        // When
        $response = $this->patchJson($step->apiPath());

        // Then
        $response->assertUnauthorized();
    }

    /**
     * @test
     * @dataProvider agentStatusesDataProvider
     */
    public function it_allowed_only_to_specified_agent_statuses(
        int $status,
        bool $allowed
    ) {
        // Setup
        $step = EducationStepFactory::new()->create();
        $agent = AgentFactory::new()->create([
            'status' => $status,
        ]);

        // Given
        $this->signInApi($agent);

        // When
        $response = $this->patchJson($step->apiPath());

        // Then
        $allowed
            ? $this->assertFalse($response->isForbidden())
            : $this->assertTrue($response->isForbidden());
    }

    /** @test */
    public function it_can_be_marked_as_read()
    {
        // Setup
        $agent = AgentFactory::new()
            ->employed()
            ->withEducationSteps(1)
            ->create();

        // Given
        $step = $agent->educationSteps()->first();

        // When
        $this->signInApi($agent);
        $response = $this->patchJson($step->apiPath());

        // Then
        $step = $agent->educationSteps()->first();

        $response->assertOk()
            ->assertJsonPath('data', $this->getJsonStep($step));
    }

    protected function getJsonStep(EducationStep $educationStep)
    {
        return [
            'id'            => $educationStep->id,
            'description'   => $educationStep->description,
            'name'          => $educationStep->name,
            'read'          => '1',
        ];
    }

    public function agentStatusesDataProvider(): array
    {
        return $this->getSetOfPermissions(AgentStatus::class, [
            AgentStatus::EMPLOYED(),
        ]);
    }
}
