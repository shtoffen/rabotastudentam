<?php

namespace Tests\Feature\API\Education\Steps;

use App\EducationStep;
use App\Enums\AgentStatus;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DataProvidersTestCase;
use Tests\Factories\AgentFactory;
use Tests\Factories\EducationStepFactory;

class IndexTest extends DataProvidersTestCase
{
    use RefreshDatabase;

    protected string $url;

    protected function setUp(): void
    {
        parent::setUp();

        $this->url = route('api.education.steps.index');
    }

    /** @test */
    public function it_not_allowed_to_guest()
    {
        // Given
        $this->assertGuest('api');

        // When
        $response = $this->getJson($this->url);

        // Then
        $response->assertUnauthorized();
    }

    /**
     * @test
     * @dataProvider agentStatusesDataProvider
     */
    public function it_allowed_only_to_specified_agent_statuses(
        int $status,
        bool $allowed
    ) {
        // Setup
        $agent = AgentFactory::new()->create([
            'status' => $status,
        ]);

        // Given
        $this->signInApi($agent);

        // When
        $response = $this->getJson($this->url);

        // Then
        $allowed
            ? $this->assertFalse($response->isForbidden())
            : $this->assertTrue($response->isForbidden());
    }

    /** @test */
    public function it_has_the_list_of_steps()
    {
        // Setup
        $agent = AgentFactory::new()
            ->employed()
            ->withEducationSteps(2)
            ->create();

        // Given
        $educationSteps = $agent->educationSteps;

        // When
        $this->signInApi($agent);
        $response = $this->getJson($this->url);

        // Then
        $response->assertOk()
            ->assertJsonPath('data', [
                $this->getJsonPreview($educationSteps[0]),
                $this->getJsonPreview($educationSteps[1]),
            ]);
    }

    /** @test */
    public function it_syncs_new_education_step()
    {
        // Setup
        $agent = AgentFactory::new()
            ->employed()
            ->withEducationSteps(2)
            ->create();

        // Given
        EducationStepFactory::new()->create();
        $this->assertSame(2, $agent->educationSteps()->count());

        // When
        $this->signInApi($agent);
        $response = $this->getJson($this->url);

        // Then
        $this->assertSame(3, $agent->educationSteps()->count());
        $response->assertOk()->assertJsonCount(3, 'data');
    }

    protected function getJsonPreview(EducationStep $educationStep)
    {
        return [
            'id' => $educationStep->id,
            'name' => $educationStep->name,
            'read' => $educationStep->pivot->read,
        ];
    }

    public function agentStatusesDataProvider(): array
    {
        return $this->getSetOfPermissions(AgentStatus::class, [
            AgentStatus::EMPLOYED(),
        ]);
    }
}
