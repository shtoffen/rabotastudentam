<?php

namespace Tests\Feature\API\Profile;

use App\Agent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\AgentFactory;
use Tests\TestCase;

class ShowTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_not_allowed_to_guests()
    {
        // Given
        $this->assertGuest('api');

        // When
        $response = $this->getJson(route('api.profile.show'));

        // Then
        $response->assertUnauthorized();
    }

    /** @test */
    public function it_has_the_card()
    {
        // Setup
        $agent = AgentFactory::new()->withStatus()->withImages(2)->create();

        // Given
        $this->signInApi($agent);

        // When
        $response = $this->getJson(route('api.profile.show'));

        // Then
        $response->assertOk()
            ->assertJsonPath('data', $this->jsonCard($agent));
    }

    protected function jsonCard(Agent $agent)
    {
        return [
            'id'            => $agent->id,
            'address'       => $agent->address,
            'cellphone'     => $agent->cellphone,
            'city'          => [
                'id'    => $agent->city->id,
                'name'  => $agent->city->name,
            ],
            'date_of_birth' => $agent->date_of_birth,
            'email'         => $agent->email,
            'name'          => $agent->name,
            'passport_images' => [
                [
                    'image' => $agent->images[0]->url,
                    'type' => $agent->images[0]->content_type->value,
                ],
                [
                    'image' => $agent->images[1]->url,
                    'type' => $agent->images[1]->content_type->value,
                ],
            ],
            'post_code'     => $agent->post_code,
            'status'        => [
                'id'    => $agent->status->value,
                'name'  => $agent->status->description,
            ],
        ];
    }
}
