<?php

namespace Tests\Feature\API\Profile;

use App\Agent;
use App\City;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\AgentFactory;
use Tests\Factories\CityFactory;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_not_allowed_to_guests()
    {
        // Given
        $this->assertGuest('api');

        // When
        $response = $this->getJson(route('api.profile.update'));

        // Then
        $response->assertUnauthorized();
    }

    /** @test */
    public function it_not_allowed_to_pending_agents()
    {
        // Setup
        $agent = AgentFactory::new()->pending()->create();

        // Given
        $this->signInApi($agent);

        // When
        $response = $this->patchJson(route('api.profile.update'));

        // Then
        $response->assertForbidden();
    }

    /** @test */
    public function it_not_allowed_to_rejected_agents()
    {
        // Setup
        $agent = AgentFactory::new()->rejected()->create();

        // Given
        $this->signInApi($agent);

        // When
        $response = $this->patchJson(route('api.profile.update'));

        // Then
        $response->assertForbidden();
    }

    /** @test */
    public function it_allowed_to_employed_agents()
    {
        // Setup
        $agent = AgentFactory::new()->employed()->create();

        // Given
        $this->signInApi($agent);

        // When
        $response = $this->patchJson(route('api.profile.update'));

        // Then
        $this->assertFalse($response->isForbidden());
    }

    /** @test */
    public function it_can_be_updated()
    {
        // Setup
        $agent = AgentFactory::new()->employed()->create();
        $city = CityFactory::new()->create();

        // Given
        $attributes = [
            'address'       => $this->faker->address,
            'cellphone'     => $this->faker->numerify('+79#########'),
            'city_id'       => $city->id,
            'date_of_birth' => $this->faker->date(),
            'name'          => $this->faker->name,
            'post_code'     => $this->faker->postcode,
        ];

        // When
        $this->signInApi($agent);
        $response = $this->patchJson(route('api.profile.update'), $attributes);

        // Then
        $response->assertOk()
            ->assertJsonPath('data', $this->jsonCard($agent, $attributes));
    }

    protected function jsonCard(Agent $agent, array $updatedAttributes)
    {
        $city = City::find($updatedAttributes['city_id']);

        return [
            'id'            => $agent->id,
            'address'       => $updatedAttributes['address'],
            'cellphone'     => $updatedAttributes['cellphone'],
            'city'          => [
                'id'    => $city->id,
                'name'  => $city->name,
            ],
            'date_of_birth' => $updatedAttributes['date_of_birth'],
            'email'         => $agent->email,
            'name'          => $updatedAttributes['name'],
            'passport_images' => [],
            'post_code'     => $updatedAttributes['post_code'],
            'status'        => [
                'id'    => $agent->status->value,
                'name'  => $agent->status->description,
            ],
        ];
    }
}
