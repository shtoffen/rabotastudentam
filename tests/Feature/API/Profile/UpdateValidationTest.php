<?php

namespace Tests\Feature\API\Profile;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\ValidationTestCase;

class UpdateValidationTest extends ValidationTestCase
{
    use RefreshDatabase, WithFaker;

    protected string $url;

    protected function setUp(): void
    {
        parent::setUp();

        $this->url = route('api.profile.update');
    }

    /** @test */
    public function it_requires_an_address_to_be_string()
    {
        // Given
        $attributes = [
            'address' => [1, 2],
        ];

        // When
        $response = $this->patchJson($this->url, $attributes);

        // Then
        $response->assertJsonValidationErrors([
            'address' => 'The address must be a string.',
        ]);
    }

    /** @test */
    public function it_requires_an_address_to_be_lte_255()
    {
        // Given
        $attributes = [
            'address' => $this->faker->regexify('a{256}'),
        ];

        // When
        $response = $this->patchJson($this->url, $attributes);

        // Then
        $response->assertJsonValidationErrors([
            'address' => 'The address may not be greater than 255 characters.',
        ]);
    }

    /** @test */
    public function it_requires_a_cellphone_to_be_string()
    {
        // Given
        $attributes = [
            'cellphone' => [1, 2],
        ];

        // When
        $response = $this->patchJson($this->url, $attributes);

        // Then
        $response->assertJsonValidationErrors([
            'cellphone' => 'The cellphone must be a string.',
        ]);
    }

    /**
     * @test
     * @dataProvider getCellphonesDataProvider
     */
    public function it_requires_a_cellphone_to_be_in_russian_format($value)
    {
        // Given
        $attributes = [
            'cellphone' => $value,
        ];

        // When
        $response = $this->patchJson($this->url, $attributes);

        // Then
        $response->assertJsonValidationErrors([
            'cellphone' => 'The cellphone must be a russian cellphone.',
        ]);
    }

    public function getCellphonesDataProvider()
    {
        return [
            ['79001234567'],
            ['89001234567'],
            ['9001234567'],
            ['1234567900'],
            ['test'],
        ];
    }

    /** @test */
    public function it_requires_a_city_id_to_be_numeric()
    {
        // Given
        $attributes = [
            'city_id' => 'test',
        ];

        // When
        $response = $this->patchJson($this->url, $attributes);

        // Then
        $response->assertJsonValidationErrors([
            'city_id' => 'The city id must be a number.',
        ]);
    }

    /** @test */
    public function it_requires_an_existing_city_id()
    {
        // Given
        $attributes = [
            'city_id' => 0,
        ];

        // When
        $response = $this->patchJson($this->url, $attributes);

        // Then
        $response->assertJsonValidationErrors([
            'city_id' => 'The selected city is invalid.',
        ]);
    }

    /**
     * @test
     * @dataProvider dateOfBirthDataProvider
     */
    public function it_requires_a_date_of_birth_to_be_valid_date(string $date)
    {
        // Given
        $attributes = [
            'date_of_birth' => $date,
        ];

        // When
        $response = $this->patchJson($this->url, $attributes);

        // Then
        $response->assertJsonValidationErrors([
            'date_of_birth' => 'The date of birth does not match the format Y-m-d.',
        ]);
    }

    public function dateOfBirthDataProvider(): array
    {
        return [
            ['2000-13-01'],
            ['01-01-2000'],
            ['13-01-2000'],
            ['01-13-2000'],
            ['01/13/2000'],
            ['13/01/2000'],
            ['2000/13/01'],
            ['2000/01/13'],
            ['946684800'],
        ];
    }

    /** @test */
    public function it_requires_a_name_to_be_string()
    {
        // Given
        $attributes = [
            'name' => [1, 2],
        ];

        // When
        $response = $this->patchJson($this->url, $attributes);

        // Then
        $response->assertJsonValidationErrors([
            'name' => 'The name must be a string.',
        ]);
    }

    /** @test */
    public function it_requires_a_name_to_be_lte_255()
    {
        // Given
        $attributes = [
            'name' => $this->faker->regexify('a{256}'),
        ];

        // When
        $response = $this->patchJson($this->url, $attributes);

        // Then
        $response->assertJsonValidationErrors([
            'name' => 'The name may not be greater than 255 characters.',
        ]);
    }

    /**
     * @test
     * @dataProvider getPostCodesDataProvider
     */
    public function it_requires_a_post_code_to_be_6_digits($value)
    {
        // Given
        $attributes = [
            'post_code' => $value,
        ];

        // When
        $response = $this->patchJson($this->url, $attributes);

        // Then
        $response->assertJsonValidationErrors([
            'post_code' => 'The post code must be 6 digits.',
        ]);
    }

    public function getPostCodesDataProvider()
    {
        return [
            [1],
            [12],
            [123],
            [1234],
            [12345],
            [1234567],
            ['test'],
        ];
    }
}
