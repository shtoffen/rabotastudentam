<?php

namespace Tests\Feature\API\Profile\FcmToken;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\AgentFactory;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_is_not_allowed_to_guests()
    {
        // Given
        $this->assertGuest('api');

        // When
        $response = $this->putJson(route('api.profile.fcm_token'));

        // Then
        $response->assertUnauthorized();
    }

    /** @test */
    public function it_can_create_new_if_token_not_exists()
    {
        // Setup
        $agent = AgentFactory::new()->create();

        // Given
        $attributes = ['token' => $this->faker->md5];
        $this->assertNull($agent->fcmToken);

        // When
        $this->signInApi($agent);
        $response = $this->putJson(route('api.profile.fcm_token'), $attributes);

        // Then
        $response->assertCreated()->assertJsonPath('data', $attributes);

        $agent->refresh();
        $this->assertNotNull($agent->fcmToken);
        $this->assertSame($attributes['token'], $agent->fcmToken->token);
    }

    /** @test */
    public function it_can_update_an_existing_token()
    {
        // Setup
        $agent = AgentFactory::new()->withFcmToken()->create();

        // Given
        $attributes = ['token' => $this->faker->md5];
        $this->assertNotNull($agent->fcmToken()->first());
        $oldToken = $agent->fcmToken->token;

        // When
        $this->signInApi($agent);
        $response = $this->putJson(route('api.profile.fcm_token'), $attributes);

        // Then
        $response->assertOk()->assertJsonPath('data', $attributes);

        $agent->refresh();
        $this->assertNotNull($agent->fcmToken);
        $this->assertNotSame($oldToken, $agent->fcmToken->token);
        $this->assertSame($attributes['token'], $agent->fcmToken->token);
    }
}
