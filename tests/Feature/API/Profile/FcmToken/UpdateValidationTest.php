<?php

namespace Tests\Feature\API\Profile\FcmToken;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\Factories\AgentFactory;
use Tests\ValidationTestCase;

class UpdateValidationTest extends ValidationTestCase
{
    use RefreshDatabase, WithFaker;

    protected string $url;

    protected function setUp(): void
    {
        parent::setUp();

        $this->url = route('api.profile.fcm_token');
    }

    /** @test */
    public function it_requires_a_token()
    {
        // Given
        $attributes = [
            'token' => null,
        ];

        // When
        $response = $this->putJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'token' => 'The token field is required.',
            ]);
    }

    /** @test */
    public function it_requires_a_token_to_be_string()
    {
        // Given
        $attributes = [
            'token' => [1, 2],
        ];

        // When
        $response = $this->putJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'token' => 'The token must be a string.',
            ]);
    }

    /** @test */
    public function it_requires_a_token_to_be_lte_255()
    {
        // Given
        $attributes = [
            'token' => $this->faker->regexify('a{256}'),
        ];

        // When
        $response = $this->putJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'token' => 'The token may not be greater than 255 characters.',
            ]);
    }
}
