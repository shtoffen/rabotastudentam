<?php

namespace Tests\Feature\API\Agents;

use App\Agent;
use App\Enums\AgentDeviceType;
use App\Enums\ImageContentType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Tests\Factories\CityFactory;
use Tests\TestCase;

class CreateTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_can_be_created()
    {
        // Setup
        Storage::fake($disk = Agent::PASSPORT_IMAGES_DISK);

        // Given
        $password = $this->faker->password;
        $passportImages =  [
            [
                'image' => UploadedFile::fake()->image('image1.jpg'),
                'type'  => ImageContentType::PASSPORT_PHOTO_SECTION,
            ],
            [
                'image' => UploadedFile::fake()->image('image1.jpg'),
                'type'  => ImageContentType::PASSPORT_REGISTRATION_SECTION,
            ],
        ];

        $attributes = [
            'address'           => $this->faker->address,
            'cellphone'         => $this->faker->numerify('+79#########'),
            'city_id'           => CityFactory::new()->create()->id,
            'email'             => $this->faker->safeEmail,
            'date_of_birth'     => $this->faker->date(),
            'name'              => $this->faker->name,
            'post_code'         => $this->faker->postcode,
        ];

        $this->assertEquals(0, Agent::count());

        // When
        $response = $this->postJson(route('api.agents.store'), $attributes + [
            'password'          => $password,
            'passport_images'   => $passportImages,
        ]);

        // Then
        $response->assertCreated()->assertExactJson([]);
        $this->assertEquals(1, Agent::count());

        $this->assertDatabaseHas('agents', $attributes);
        $this->assertTrue(Hash::check($password, Agent::first()->password));

        $agent = Agent::first();
        $this->assertSame(2, $agent->images->count());

        $this->assertTrue($agent->images[0]->content_type->is(
            ImageContentType::PASSPORT_PHOTO_SECTION
        ));
        $this->assertTrue($agent->images[1]->content_type->is(
            ImageContentType::PASSPORT_REGISTRATION_SECTION
        ));

        Storage::disk($disk)->assertExists($agent->images[0]->path);
        Storage::disk($disk)->assertExists($agent->images[1]->path);
    }
}
