<?php

namespace Tests\Feature\API\Agents;

use App\Agent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Tests\Factories\AgentFactory;
use Tests\TestCase;

class CreateValidationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected string $url;

    protected function setUp(): void
    {
        parent::setUp();

        $this->url = route('api.agents.store');
        $this->assertEquals(0, Agent::count());
    }

    protected function tearDown(): void
    {
        $this->assertEquals(0, Agent::count());

        parent::tearDown();
    }

    /** @test */
    public function it_requires_an_address()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'address' => null,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'address' => 'The address field is required.',
            ]);
    }

    /** @test */
    public function it_requires_an_address_to_be_string()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'address' => [1, 2],
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'address' => 'The address must be a string.',
            ]);
    }

    /** @test */
    public function it_requires_an_address_to_be_lte_255()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'address' => $this->faker->regexify('[a-z]{256}'),
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'address' => 'The address may not be greater than 255 characters.',
            ]);
    }

    /** @test */
    public function it_requires_a_cellphone()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'cellphone' => null,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'cellphone' => 'The cellphone field is required.',
            ]);
    }

    /** @test */
    public function it_requires_a_cellphone_to_be_string()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'cellphone' => [1, 2],
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'cellphone' => 'The cellphone must be a string.',
            ]);
    }

    /** @test */
    public function it_requires_a_cellphone_to_be_russian_cellphone()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'cellphone' => $this->faker->sentence,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'cellphone' => 'The cellphone must be a russian cellphone.',
            ]);
    }

    /** @test */
    public function it_requires_a_city_id()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'city_id' => null,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'city_id' => 'The city id field is required.',
            ]);
    }

    /** @test */
    public function it_requires_an_existing_city_id()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'city_id' => 0,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'city_id' => 'The selected city id is invalid.',
            ]);
    }

    /** @test */
    public function it_requires_an_email()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'email' => null,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'email' => 'The email field is required.',
            ]);
    }

    /** @test */
    public function it_requires_an_email_to_be_string()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'email' => [1, 2],
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'email' => 'The email must be a string.',
            ]);
    }

    /** @test */
    public function it_requires_an_email_to_be_lte_255()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'email' => $this->faker->regexify('[a-z]{256}'),
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'email' => 'The email may not be greater than 255 characters.',
            ]);
    }

    /** @test */
    public function it_requires_an_email_to_be_email()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'email' => $this->faker->word,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'email' => 'The email must be a valid email address.',
            ]);
    }

    /** @test */
    public function it_requires_an_unique_email()
    {
        // Setup
        $existingAgent = AgentFactory::new()->create();

        // Given
        $attributes = AgentFactory::new()->raw([
            'email' => $existingAgent->email,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'email' => 'The email has already been taken.',
            ]);

        $existingAgent->delete();
    }

    /** @test */
    public function it_requires_a_date_of_birth()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'date_of_birth' => null,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'date_of_birth' => 'The date of birth field is required.',
            ]);
    }

    /**
     * @test
     * @dataProvider dateOfBirthDataProvider
     */
    public function it_requires_a_date_of_birth_to_be_valid_date(string $date)
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'date_of_birth' => $date,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'date_of_birth' => 'The date of birth does not match the format Y-m-d.',
            ]);
    }

    public function dateOfBirthDataProvider(): array
    {
        return [
            ['2000-13-01'],
            ['01-01-2000'],
            ['13-01-2000'],
            ['01-13-2000'],
            ['01/13/2000'],
            ['13/01/2000'],
            ['2000/13/01'],
            ['2000/01/13'],
            ['946684800'],
        ];
    }

    /** @test */
    public function it_requires_a_name()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'name' => null,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'name' => 'The name field is required.',
            ]);
    }

    /** @test */
    public function it_requires_a_name_to_be_string()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'name' => [1, 2],
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'name' => 'The name must be a string.',
            ]);
    }

    /** @test */
    public function it_requires_a_name_to_be_lte_255()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'name' => $this->faker->regexify('[a-z]{256}'),
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'name' => 'The name may not be greater than 255 characters.',
            ]);
    }

    /** @test */
    public function it_requires_a_passport_images()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'passport_images' => null,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'passport_images' => 'The passport images field is required.',
            ]);
    }

    /** @test */
    public function it_requires_a_passport_images_to_be_array()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'passport_images' => '123',
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'passport_images' => 'The passport images must be an array.',
            ]);
    }

    /**
     * @test
     * @dataProvider passportImagesElementsDataProvider
     */
    public function it_requires_a_passport_images_array_to_contain_2_elements(
        array $data
    ) {
        // Given
        $attributes = AgentFactory::new()->raw([
            'passport_images' => $data,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'passport_images' => 'The passport images must contain 2 items.',
            ]);
    }

    public function passportImagesElementsDataProvider(): array
    {
        return [
            [[1]],
            [[1, 2, 3]],
        ];
    }

    /** @test */
    public function it_requires_an_image_in_passport_images_element()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'passport_images' => [
                ['test' => 123]
            ],
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'passport_images.0.image' => 'The passport_images.0.image field is required.',
            ]);
    }

    /** @test */
    public function it_requires_an_image_in_passport_images_element_to_be_image()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'passport_images' => [
                ['image' => 123]
            ],
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'passport_images.0.image' => 'The passport_images.0.image must be an image.',
            ]);
    }

    /** @test */
    public function it_requires_an_image_in_passport_images_element_to_be_lte_10000_kilobytes()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'passport_images' => [
                ['image' => UploadedFile::fake()->image('test.jpg')->size(10001)]
            ],
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'passport_images.0.image'
                 => 'The passport_images.0.image may not be greater than 10000 kilobytes.',
            ]);
    }

    /** @test */
    public function it_requires_a_type_in_passport_images_element()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'passport_images' => [
                ['test' => 123]
            ],
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'passport_images.0.type' => 'The passport_images.0.type field is required.',
            ]);
    }

    /** @test */
    public function it_requires_a_type_in_passport_images_element_to_be_integer()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'passport_images' => [
                ['type' => 'abc']
            ],
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'passport_images.0.type' => 'The passport_images.0.type must be an integer.',
            ]);
    }

    /** @test */
    public function it_requires_a_type_to_be_1_in_passport_images_first_element()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'passport_images' => [
                ['type' => 2],
                ['type' => 1],
            ],
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'passport_images.0.type' => 'The passport_images.0.type must be equal to 1.',
            ]);
    }

    /** @test */
    public function it_requires_a_type_to_be_2_in_passport_images_second_element()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'passport_images' => [
                ['type' => 2],
                ['type' => 1],
            ],
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'passport_images.1.type' => 'The passport_images.1.type must be equal to 2.',
            ]);
    }

    /** @test */
    public function it_requires_a_password()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'password' => null,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'password' => 'The password field is required.',
            ]);
    }

    /** @test */
    public function it_requires_a_password_to_be_string()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'password' => [1, 2],
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'password' => 'The password must be a string.',
            ]);
    }

    /** @test */
    public function it_requires_a_password_to_be_gte_6()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'password' => $this->faker->regexify('[a-z]{5}'),
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'password' => 'The password must be at least 6 characters.',
            ]);
    }

    /** @test */
    public function it_requires_a_password_to_be_lte_255()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'password' => $this->faker->regexify('[a-z]{256}'),
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'password' => 'The password may not be greater than 255 characters.',
            ]);
    }

    /** @test */
    public function it_requires_a_post_code()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'post_code' => null,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'post_code' => 'The post code field is required.',
            ]);
    }

    /**
     * @test
     * @dataProvider postCodeProvider
     */
    public function it_requires_a_post_code_to_be_6_digits($mockedData)
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'post_code' => $mockedData,
        ]);

        // When
        $response = $this->postJson($this->url, $attributes);

        // Then
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonValidationErrors([
                'post_code' => 'The post code must be 6 digits.',
            ]);
    }

    /**
     * Post codes data provider.
     *
     * @return array
     */
    public function postCodeProvider(): array
    {
        return [
            ['test'],
            ['11111'],
            ['1111111'],
        ];
    }
}
