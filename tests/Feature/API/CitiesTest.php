<?php

namespace Tests\Feature\API;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\CityFactory;
use Tests\TestCase;

class CitiesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function anyone_can_get_a_list_of_cities()
    {
        // Given
        $cities = CityFactory::new()->times(2);

        // When
        $response = $this->getJson(route('api.cities.index'));

        // Then
        $response->assertOk()->assertExactJson(['data' => [
            $this->jsonCity($cities[0]),
            $this->jsonCity($cities[1]),
        ]]);
    }

    /** @test */
    public function anyone_can_get_the_specific_city()
    {
        // Given
        $city = CityFactory::new()->create();

        // When
        $response = $this->getJson($city->apiPath());

        // Then
        $response->assertOk()->assertExactJson(['data' =>
            $this->jsonCity($city),
        ]);
    }

    protected function jsonCity($city)
    {
        return [
            'id' => $city->id,
            'name' => $city->name,
        ];
    }
}
