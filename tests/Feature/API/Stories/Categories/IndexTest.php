<?php

namespace Tests\Feature\API\Stories\Categories;

use App\Enums\AgentStatus;
use App\StoryCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Tests\DataProvidersTestCase;
use Tests\Factories\AgentFactory;
use Tests\Factories\StoryCategoryFactory;

class IndexTest extends DataProvidersTestCase
{
    use RefreshDatabase;

    protected string $url;

    protected function setUp(): void
    {
        parent::setUp();

        $this->url = route('api.stories.categories.index');
    }

    /** @test */
    public function it_is_not_allowed_to_guest()
    {
        // Given
        $this->assertGuest('api');

        // When
        $response = $this->getJson($this->url);

        // Then
        $response->assertUnauthorized();
    }

    /**
     * @test
     * @dataProvider agentStatusesDataProvider
     */
    public function it_allowed_only_to_specified_agent_statuses(
        int $status,
        bool $allowed
    ) {
        // Setup
        $agent = AgentFactory::new()->create([
            'status' => $status,
        ]);

        // Given
        $this->signInApi($agent);

        // When
        $response = $this->getJson($this->url);

        // Then
        $allowed
            ? $this->assertFalse($response->isForbidden())
            : $this->assertTrue($response->isForbidden());
    }

    /** @test */
    public function it_has_the_list_of_stories()
    {
        // Setup
        Storage::disk(StoryCategory::IMAGES_DISK);

        // Given
        $categories = StoryCategoryFactory::new()->withImage()->times(2);

        // When
        $this->signInApi(AgentFactory::new()->employed()->create());
        $response = $this->getJson($this->url);

        // Then
        $response->assertOk()
            ->assertJsonPath('data', [
                $this->getJsonPreview($categories[0]),
                $this->getJsonPreview($categories[1]),
            ]);
    }

    protected function getJsonPreview(StoryCategory $category): array
    {
        return [
            'id'        => $category->id,
            'title'     => $category->title,
            'image'     => $category->image->url,
        ];
    }

    public function agentStatusesDataProvider(): array
    {
        return $this->getSetOfPermissions(AgentStatus::class, [
            AgentStatus::EMPLOYED(),
        ]);
    }
}
