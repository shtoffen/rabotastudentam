<?php

namespace Tests\Feature\API\Stories;

use App\Enums\AgentStatus;
use App\Story;
use App\StoryCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Tests\DataProvidersTestCase;
use Tests\Factories\AgentFactory;
use Tests\Factories\StoryCategoryFactory;
use Tests\Factories\StoryFactory;

class IndexTest extends DataProvidersTestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_is_not_allowed_to_guest()
    {
        // Given
        $this->assertGuest('api');

        // When
        $response = $this->getJson(route('api.stories.index'));

        // Then
        $response->assertUnauthorized();
    }

    /**
     * @test
     * @dataProvider agentStatusesDataProvider
     */
    public function it_allowed_only_to_specified_agent_statuses(
        int $status,
        bool $allowed
    ) {
        // Setup
        $agent = AgentFactory::new()->create([
            'status' => $status,
        ]);

        // Given
        $this->signInApi($agent);

        // When
        $response = $this->getJson(route('api.stories.index'));

        // Then
        $allowed
            ? $this->assertFalse($response->isForbidden())
            : $this->assertTrue($response->isForbidden());
    }

    /** @test */
    public function it_shows_nothing_by_default()
    {
        // Given
        $stories = StoryFactory::new()->times(2);

        // When
        $this->signInApi(AgentFactory::new()->employed()->create());
        $resposne = $this->getJson(route('api.stories.index'));

        // Then
        $resposne->assertOk()
            ->assertJsonCount(0, 'data')
            ->assertJsonPath('data', []);
    }

    /** @test */
    public function it_can_be_filtered_by_category_id()
    {
        // Setup
        Storage::disk(StoryCategory::IMAGES_DISK);
        $category = StoryCategoryFactory::new()->create();

        // Given
        $stories = StoryFactory::new()
            ->withImage()
            ->forCategory($category)
            ->times(2);

        $someStory = StoryFactory::new()->withImage()->create();
        $this->assertNotSame($stories[0]->category_id, $someStory->category_id);

        // When
        $this->signInApi(AgentFactory::new()->employed()->create());
        $response = $this->getJson(
            route('api.stories.index', ['category_id' => $category->id])
        );

        // Then
        $response->assertOk()
            ->assertJsonCount(2, 'data')
            ->assertJsonPath('data', [
                $this->jsonStory($stories[0]),
                $this->jsonStory($stories[1]),
            ]);
    }

    protected function jsonStory(Story $story): array
    {
        return [
            'id'                => $story->id,
            'category_id'       => $story->category_id,
            'description'       => $story->description,
            'image'             => $story->image->url,
            'title'             => $story->title,
        ];
    }

    public function agentStatusesDataProvider(): array
    {
        return $this->getSetOfPermissions(AgentStatus::class, [
            AgentStatus::EMPLOYED(),
        ]);
    }
}
