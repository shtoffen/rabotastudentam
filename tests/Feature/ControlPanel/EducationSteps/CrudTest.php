<?php

namespace Tests\Feature\ControlPanel\EducationSteps;

use App\EducationStep;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\EducationStepFactory;
use Tests\TestCase;

class CrudTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->signIn();
    }

    /** @test */
    public function it_has_the_index_page()
    {
        // Given
        $educationSteps = EducationStepFactory::new()->times(2);

        // When
        $response = $this->get(route('cp.education.steps.index'));

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.education_steps.index')
            ->assertSeeTextInOrder(array_merge(
                $this->getAttributesListOfEducationCard($educationSteps[0]),
                $this->getAttributesListOfEducationCard($educationSteps[1])
            ));
    }

    public function getAttributesListOfEducationCard($educationStep)
    {
        return [
            $educationStep->id,
            $educationStep->name,
        ];
    }

    /** @test */
    public function it_has_the_creating_form()
    {
        // When
        $response = $this->get(route('cp.education.steps.create'));

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.education_steps.create');
    }

    /** @test */
    public function it_can_be_stored()
    {
        // Given
        $this->assertSame(0, EducationStep::count());

        $attributes = [
            'name'          => $this->faker->sentence,
            'description'   => $this->faker->text,
        ];

        // When
        $response = $this->post(route('cp.education.steps.store'), $attributes);

        // Then
        $this->assertEquals(1, EducationStep::count());
        $educationStep = EducationStep::first();

        $response->assertSessionDoesntHaveErrors()
            ->assertRedirect($educationStep->path());

        $this->assertDatabaseHas('education_steps', $attributes);
    }

    /** @test */
    public function it_has_the_card_page()
    {
        // Given
        $educationStep = EducationStepFactory::new()->create();

        // When
        $response = $this->get($educationStep->path());

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.education_steps.show')
            ->assertViewHas('step', $educationStep);
    }

    /** @test */
    public function it_has_the_updating_form()
    {
        // Given
        $educationStep = EducationStepFactory::new()->create();

        // When
        $response = $this->get($educationStep->path('edit'));

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.education_steps.edit')
            ->assertViewHas('step', $educationStep);
    }

    /** @test */
    public function it_can_be_updated()
    {
        // Setup
        $educationStep = EducationStepFactory::new()->create();

        // Given
        $attributes = [
            'name'          => $this->faker->sentence,
            'description'   => $this->faker->sentence,
        ];

        // When
        $response = $this->patch($educationStep->path(), $attributes);

        // Then
        $response->assertSessionDoesntHaveErrors()
            ->assertRedirect($educationStep->path());

        $this->assertDatabaseHas('education_steps', $attributes + [
            'id' => $educationStep->id,
        ]);
    }

    /** @test */
    public function it_can_be_deleted()
    {
        // Given
        $educationStep = EducationStepFactory::new()->create();

        $attributes = $educationStep->getAttributes();
        $this->assertDatabaseHas('education_steps', $attributes);

        // When
        $response = $this->delete($educationStep->path());

        // Then
        $response->assertSessionDoesntHaveErrors()
            ->assertRedirect(route('cp.education.steps.index'));

        $this->assertDatabaseMissing('education_steps', $attributes);
    }
}
