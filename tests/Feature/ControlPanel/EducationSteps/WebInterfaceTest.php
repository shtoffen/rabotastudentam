<?php

namespace Tests\Feature\ControlPanel\EducationSteps;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class WebInterfaceTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->signIn();
    }

    /**
     * @test
     * @group feature
     */
    public function it_has_the_educations_steps_sidebar_menu_button()
    {
        //When
        $response = $this->get(route('cp.home'));

        //Then
        $response->assertOk()
            ->assertSee('Обучение')
            ->assertSee(route('cp.education.steps.index'));
    }
}
