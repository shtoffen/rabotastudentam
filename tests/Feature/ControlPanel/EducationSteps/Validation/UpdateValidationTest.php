<?php

namespace Tests\Feature\ControlPanel\EducationSteps\Validation;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\ValidationTestCase;

class UpdateValidationTest extends ValidationTestCase
{
    use RefreshDatabase, WithFaker;

    protected string $url;

    protected function setUp(): void
    {
        parent::setUp();

        $this->url = route('cp.education.steps.update', [
            'step' => 1,
        ]);
    }

    /**
     * @test
     * @group feature
     * @group validation
     */
    public function it_requires_a_name()
    {
        // Given
        $attributes = [
            'name' => null,
        ];

        // When
        $response = $this->patch($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'name' => 'The name field is required.',
        ]);
    }

    /**
     * @test
     * @group feature
     * @group validation
     */
    public function it_requires_a_name_to_be_string()
    {
        // Given
        $attributes = [
            'name' => [1, 2],
        ];

        // When
        $response = $this->patch($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'name' => 'The name must be a string.',
        ]);
    }

    /**
     * @test
     * @group feature
     * @group validation
     */
    public function it_requires_a_name_to_be_lte_255()
    {
        // Given
        $attributes = [
            'name' => $this->faker->regexify('[a-z]{256}'),
        ];

        // When
        $response = $this->patch($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'name' => 'The name may not be greater than 255 characters.',
        ]);
    }

    /**
     * @test
     * @group feature
     * @group validation
     */
    public function it_requires_a_description()
    {
        // Given
        $attributes = [
            'description' => null,
        ];

        // When
        $response = $this->patch($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'description' => 'The description field is required.',
        ]);
    }

    /**
     * @test
     * @group feature
     * @group validation
     */
    public function it_requires_a_description_to_be_string()
    {
        // Given
        $attributes = [
            'description' => [1, 2],
        ];

        // When
        $response = $this->patch($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'description' => 'The description must be a string.',
        ]);
    }

    /**
     * @test
     * @group feature
     * @group validation
     */
    public function it_requires_a_description_to_be_lte_20000()
    {
        // Given
        $attributes = [
            'description' => $this->faker->regexify('a{20001}'),
        ];

        // When
        $response = $this->patch($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'description' => 'The description may not be greater than 20000 characters.',
        ]);
    }
}
