<?php

namespace Tests\Feature\ControlPanel\Agents;

use App\Enums\AgentStatus;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DataProvidersTestCase;
use Tests\Factories\AgentFactory;

class AuthorizationTest extends DataProvidersTestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_cannot_read_or_update_agents()
    {
        // Given
        $agent = AgentFactory::new()->create();

        // When
        $responses[0] = $this->get(route('cp.agents.index'));
        $responses[1] = $this->get($agent->path('edit'));
        $responses[2] = $this->get($agent->path());
        $responses[3] = $this->patch($agent->path());

        // Then
        collect($responses)->each(function($response) {
            $response->assertRedirect(route('cp.login.form'));
        });
    }

    /**
     * @@test
     * @dataProvider statusesDataProvider
     */
    public function it_can_update_status_only_with_specified_status(
        int $status,
        bool $passes
    ) {
        // Setup
        $agent = AgentFactory::new()->create([
            'status' => $status,
        ]);

        // Given
        $newStatus = AgentStatus::getRandomValue();

        // When
        $this->signIn();
        $response = $this->patch($agent->path(), ['status' => $newStatus]);

        // Then
        $passes
            ? $this->assertSame($newStatus, $agent->fresh()->status->value)
            : $response->assertForbidden();
    }

    public function statusesDataProvider(): array
    {
        return $this->getSetOfPermissions(AgentStatus::class, [
            AgentStatus::PENDING(),
        ]);
    }
}
