<?php

namespace Tests\Feature\ControlPanel\Agents;

use App\Enums\AgentStatus;
use App\Notifications\AgentEmployedNotification;
use App\Notifications\AgentRejectedNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Tests\Factories\AgentFactory;
use Tests\Factories\CityFactory;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->signIn();
    }

    /** @test */
    public function it_has_the_edit_form()
    {
        // Given
        $agent = AgentFactory::new()->create();

        // When
        $response = $this->get($agent->path('edit'));

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.agents.edit')
            ->assertViewHas('agent', $agent);
    }

    /** @test */
    public function it_can_be_updated()
    {
        // Setup
        Notification::fake();
        $agent = AgentFactory::new()->create();

        // Given
        $attributes = [
            'name' => $this->faker->name,
            'cellphone' => $this->faker->numerify('+79#########'),
            'city_id' => CityFactory::new()->create()->id,
            'address' => $this->faker->address,
            'post_code' => $this->faker->postcode,
            'comment' => $this->faker->sentence,
        ];

        // When
        $response = $this->patch($agent->path(), $attributes);

        // Then
        $response->assertRedirect($agent->path())
            ->assertSessionHas('message', 'Агент успешно изменен.');

        $this->assertDatabaseHas('agents', $attributes + ['id' => $agent->id]);
    }

    /** @test */
    public function a_status_can_be_updated()
    {
        // Setup
        Notification::fake();
        $agent = AgentFactory::new()->create([
            'status' => AgentStatus::PENDING,
        ]);

        // Given
        $attributes = [
            'status' => AgentStatus::getRandomValue(),
        ];

        // When
        $response = $this->patch($agent->path(), $attributes);

        // Then
        $response->assertSessionDoesntHaveErrors();

        $agent->refresh();
        $this->assertSame($attributes['status'], $agent->status->value);
    }

    /** @test */
    public function it_notifies_agent_if_new_status_is_employed()
    {
        // Setup
        Notification::fake();

        // Given
        $agent = AgentFactory::new()->pending()->create();
        $newStatus = AgentStatus::EMPLOYED();

        // When
        $response = $this->patch($agent->path(), [
            'status' => $newStatus->value,
        ]);

        // Then
        $response->assertSessionDoesntHaveErrors();

        Notification::assertSentTo($agent, AgentEmployedNotification::class);
        Notification::assertNotSentTo($agent, AgentRejectedNotification::class);
    }

    /** @test */
    public function it_notifies_agent_if_new_status_is_rejected()
    {
        // Setup
        Notification::fake();

        // Given
        $agent = AgentFactory::new()->pending()->create();
        $newStatus = AgentStatus::REJECTED();

        // When
        $response = $this->patch($agent->path(), [
            'status' => $newStatus->value,
        ]);

        // Then
        $response->assertSessionDoesntHaveErrors();

        Notification::assertSentTo($agent, AgentRejectedNotification::class);
        Notification::assertNotSentTo($agent, AgentEmployedNotification::class);
    }

    /** @test */
    public function it_does_not_notifies_agent_if_status_is_still_pending()
    {
        // Setup
        Notification::fake();

        // Given
        $agent = AgentFactory::new()->pending()->create();
        $newStatus = AgentStatus::PENDING();

        // When
        $response = $this->patch($agent->path(), [
            'status' => $newStatus->value,
        ]);

        // Then
        $response->assertSessionDoesntHaveErrors();
        Notification::assertNothingSent();
    }
}
