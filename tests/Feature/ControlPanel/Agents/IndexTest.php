<?php

namespace Tests\Feature\ControlPanel\Agents;

use App\Agent;
use App\Enums\AgentStatus;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Pagination\Paginator;
use Tests\Factories\AgentFactory;
use Tests\TestCase;

class IndexTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->signIn();
    }

    /** @test */
    public function it_has_an_index_page()
    {
        // Setup
        Paginator::currentPathResolver(function () {});
        AgentFactory::new()->times(2);

        // Given
        $agents = Agent::with('city')->paginate(20);

        // When
        $response = $this->get(route('cp.agents.index'));

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.agents.index')
            ->assertViewHas('agents', $agents);
    }

    /** @test */
    public function it_can_be_filtered_by_pending_agent_status()
    {
        // Given
        $pendingAgent = AgentFactory::new()->pending()->create();
        $employedAgent = AgentFactory::new()->employed()->create();

        // When
        $response = $this->get(route('cp.agents.index', [
            'status' => AgentStatus::PENDING
        ]));

        // Then
        $response->assertOk()->assertSeeText('Список Кандидатов');

        $agents = $response->viewData('agents');

        $this->assertTrue($agents->contains($pendingAgent));
        $this->assertFalse($agents->contains($employedAgent));
    }

    /** @test */
    public function it_can_be_filtered_by_employed_agent_status()
    {
        // Given
        $pendingAgent = AgentFactory::new()->pending()->create();
        $employedAgent = AgentFactory::new()->employed()->create();

        // When
        $response = $this->get(route('cp.agents.index', [
            'status' => AgentStatus::EMPLOYED
        ]));

        // Then
        $response->assertOk()->assertSeeText('Список Сотрудников');

        $agents = $response->viewData('agents');

        $this->assertTrue($agents->contains($employedAgent));
        $this->assertFalse($agents->contains($pendingAgent));
    }

    /** @test */
    public function it_has_the_candidates_sidebar_menu_button()
    {
        // When
        $response = $this->get(route('cp.home'));

        // Then
        $response->assertOk()
            ->assertSee('Кандидаты')
            ->assertSee(route('cp.agents.index'));
    }

    /** @test */
    public function it_has_the_employees_sidebar_menu_button()
    {
        // When
        $response = $this->get(route('cp.home'));

        // Then
        $response->assertOk()
            ->assertSee('Сотрудники')
            ->assertSee(route('cp.agents.index'));
    }
}
