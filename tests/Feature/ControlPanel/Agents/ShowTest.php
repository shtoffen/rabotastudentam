<?php

namespace Tests\Feature\ControlPanel\Agents;

use App\Enums\AgentStatus;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DataProvidersTestCase;
use Tests\Factories\AgentFactory;

class ShowTest extends DataProvidersTestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->signIn();
    }

    /** @test */
    public function it_has_the_card_page()
    {
        // Given
        $agent = AgentFactory::new()->create();

        // When
        $response = $this->get($agent->path());

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.agents.show')
            ->assertViewHas('agent', $agent);
    }

    /**
     * @test
     * @dataProvider statusesDataProvider
     */
    public function it_has_change_status_buttons_only_for_agent_with_specified_status(
        int $status,
        bool $passes
    ) {
        // Given
        $agent = AgentFactory::new()->create([
            'status' => $status,
        ]);

        // When
        $response = $this->get($agent->path());

        // Then
        $response->assertOk();
        $passes
            ? $response->assertSee('Принять')->assertSee('Отклонить')
            : $response->assertDontSee('Принять')->assertDontSee('Отклонить');
    }

    public function statusesDataProvider(): array
    {
        return $this->getSetOfPermissions(AgentStatus::class, [
            AgentStatus::PENDING(),
        ]);
    }
}
