<?php

namespace Tests\Feature\ControlPanel\Agents\Validation;

use App\Enums\AgentStatus;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\DataProvidersTestCase;

class IndexValidationTest extends DataProvidersTestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->signIn();
    }

    /** @test */
    public function it_requires_a_status_to_be_integer()
    {
        // Given
        $status = 'abc';

        // When
        $response = $this->get(route('cp.agents.index', ['status' => $status]));

        // Then
        $response->assertSessionHasErrors([
            'status' => 'The status must be an integer.'
        ]);
    }

    /**
     * @test
     * @dataProvider statusesDataProvider
     */
    public function it_requires_a_status_to_be_among_specified_variants(
        int $mockedData,
        bool $passes
    ) {
        // When
        $response = $this->get(route('cp.agents.index', [
            'status' => $mockedData
        ]));

        // Then
        $passes
            ? $response->assertOk()
            : $response->assertSessionHasErrors([
                'status' => 'The selected status is invalid.'
            ]);
    }

    public function statusesDataProvider(): array
    {
        return $this->getSetOfPermissions(AgentStatus::class, [
            AgentStatus::PENDING(),
            AgentStatus::EMPLOYED(),
        ]);
    }
}
