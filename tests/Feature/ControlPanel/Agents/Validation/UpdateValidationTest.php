<?php

namespace Tests\Feature\ControlPanel\Agents\Validation;

use App\Agent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\AgentFactory;
use Tests\TestCase;

class UpdateValidationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected Agent $agent;

    protected function setUp(): void
    {
        parent::setUp();

        $this->signIn();
        $this->agent = AgentFactory::new()->create();
    }

    /** @test */
    public function it_requires_an_address_to_be_string()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'address' => [1, 2],
        ]);

        // When
        $response = $this->patch($this->agent->path(), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'address' => 'The address must be a string.',
        ]);
    }

    /** @test */
    public function it_requires_an_address_to_be_lte_255()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'address' => $this->faker->regexify('[a-z]{256}'),
        ]);

        // When
        $response = $this->patch($this->agent->path(), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'address' => 'The address may not be greater than 255 characters.',
        ]);
    }

    /** @test */
    public function it_requires_a_cellphone_to_be_string()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'cellphone' => [1, 2],
        ]);

        // When
        $response = $this->patch($this->agent->path(), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'cellphone' => 'The cellphone must be a string.',
        ]);
    }

    /** @test */
    public function it_requires_a_cellphone_to_be_russian_cellphone()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'cellphone' => $this->faker->sentence,
        ]);

        // When
        $response = $this->patch($this->agent->path(), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'cellphone' => 'The cellphone must be a russian cellphone.',
        ]);
    }

    /** @test */
    public function it_requires_a_city_id_to_be_integer()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'city_id' => 'abc',
        ]);

        // When
        $response = $this->patch($this->agent->path(), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'city_id' => 'The city id must be an integer.',
        ]);
    }

    /** @test */
    public function it_requires_an_existing_city_id()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'city_id' => 0,
        ]);

        // When
        $response = $this->patch($this->agent->path(), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'city_id' => 'The selected city id is invalid.',
        ]);
    }

    /** @test */
    public function it_requires_a_comment_to_be_string()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'comment' => [1, 2],
        ]);

        // When
        $response = $this->patch($this->agent->path(), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'comment' => 'The comment must be a string.',
        ]);
    }

    /** @test */
    public function it_requires_a_comment_to_be_lte_255()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'comment' => $this->faker->regexify('[a-z]{256}'),
        ]);

        // When
        $response = $this->patch($this->agent->path(), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'comment' => 'The comment may not be greater than 255 characters.',
        ]);
    }

    /** @test */
    public function it_requires_a_name_to_be_string()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'name' => [1, 2],
        ]);

        // When
        $response = $this->patch($this->agent->path(), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'name' => 'The name must be a string.',
        ]);
    }

    /** @test */
    public function it_requires_a_name_to_be_lte_255()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'name' => $this->faker->regexify('[a-z]{256}'),
        ]);

        // When
        $response = $this->patch($this->agent->path(), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'name' => 'The name may not be greater than 255 characters.',
        ]);
    }

    /**
     * @test
     * @dataProvider postCodesProvider
     */
    public function it_requires_a_post_code_to_be_6_digits($mockedData)
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'post_code' => $mockedData,
        ]);

        // When
        $response = $this->patch($this->agent->path(), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'post_code' => 'The post code must be 6 digits.',
        ]);
    }

    public function postCodesProvider(): array
    {
        return [
            ['test'],
            ['11111'],
            ['1111111'],
        ];
    }

    /** @test */
    public function it_requires_a_status_to_be_integer()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'status' => 'abc',
        ]);

        // When
        $response = $this->patch($this->agent->path(), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'status' => 'The status must be an integer.',
        ]);
    }

    /** @test */
    public function it_requires_an_existing_status()
    {
        // Given
        $attributes = AgentFactory::new()->raw([
            'status' => 999,
        ]);

        // When
        $response = $this->patch($this->agent->path(), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'status' => 'The value you have entered is invalid.',
        ]);
    }
}
