<?php

namespace Tests\Feature\ControlPanel;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HomeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_guest_cannot_view_home_page()
    {
        // Given
        $this->assertGuest('web');

        // When
        $response = $this->get(route('cp.home'));

        // Then
        $response->assertRedirect(route('cp.login.form'));
    }

    /** @test */
    public function it_has_a_home_page()
    {
        // Given
        $this->signIn();

        // When
        $response = $this->get(route('cp.home'));

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.home')
            ->assertSeeInOrder([
                'Dashboard',
                'You are logged in!',
            ]);
    }
}
