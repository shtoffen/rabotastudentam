<?php

namespace Tests\Feature\ControlPanel\Cities;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\AgentFactory;
use Tests\Factories\CityFactory;
use Tests\TestCase;

class DeleteTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_cannot_be_deleted_with_agents()
    {
        // Setup
        $city = CityFactory::new()->create();

        // Given
        AgentFactory::new()->withCity($city)->times(2);

        // When
        $this->signIn();
        $response = $this->from(route('cp.cities.index'))
                        ->delete($city->path('destroy'));

        // Then
        $response->assertRedirect(route('cp.cities.index'))
            ->assertSessionHasErrors([
                'city' => "Город {$city->name} не может быть удален, т.к. имеет клиентов."
            ]);
    }
}
