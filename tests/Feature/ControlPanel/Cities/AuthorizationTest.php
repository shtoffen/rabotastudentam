<?php

namespace Tests\Feature\ControlPanel\Cities;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\TestResponse;
use Tests\Factories\CityFactory;
use Tests\TestCase;

class AuthorizationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function a_guest_cannot_crud_cities()
    {
        // Given
        $city = CityFactory::new()->create();

        // When
        $responses[0] = $this->get(route('cp.cities.index'));
        $responses[1] = $this->get(route('cp.cities.create'));
        $responses[2] = $this->post(route('cp.cities.store'));
        $responses[3] = $this->get($city->path('edit'));
        $responses[4] = $this->patch($city->path('update'));
        $responses[5] = $this->delete($city->path('destroy'));

        // Then
        collect($responses)->each(function(TestResponse $response) {
            $response->assertRedirect(route('cp.login.form'));
        });
    }
}
