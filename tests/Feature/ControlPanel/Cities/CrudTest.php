<?php

namespace Tests\Feature\ControlPanel\Cities;

use App\City;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\Factories\CityFactory;
use Tests\TestCase;

class CrudTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->signIn();
    }

    /** @test */
    public function it_has_the_index_page()
    {
        // Setup
        CityFactory::new()->times(2);

        // Given
        $cities = City::all();

        // When
        $response = $this->get(route('cp.cities.index'));

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.cities.index')
            ->assertViewHas('cities', $cities);
    }

    /** @test */
    public function it_has_the_creating_from()
    {
        // When
        $response = $this->get(route('cp.cities.create'));

        // Then
        $response->assertOk()->assertViewIs('control_panel.cities.create');
    }

    /** @test */
    public function it_can_be_created()
    {
        // Given
        $attributes = [
            'name' => $this->faker->city,
        ];
        $this->assertSame(0, City::count());

        // When
        $response = $this->post(route('cp.cities.store'), $attributes);

        // Then
        $this->assertSame(1, City::count());
        $this->assertDatabaseHas('cities', $attributes);

        $response->assertRedirect(route('cp.cities.index'));
        $response->assertSessionHas([
            'message' => "Город {$attributes['name']} успешно добавлен.",
        ]);
    }

    /** @test */
    public function it_does_not_have_the_card_page()
    {
        // Given
        $city = CityFactory::new()->create();

        // When
        $response = $this->get("cp/cities/{$city->id}");

        // Then
        $response->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
    }

    /** @test */
    public function it_has_the_edit_page()
    {
        // Given
        $city = CityFactory::new()->create();

        // When
        $response = $this->get($city->path('edit'));

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.cities.edit')
            ->assertViewHas('city', $city);
    }

    /** @test */
    public function it_can_be_updated()
    {
        // Setup
        $city = CityFactory::new()->create();

        // Given
        $attributes = [
            'name' => $this->faker->city,
        ];

        // When
        $response = $this->patch($city->path('update'), $attributes);

        // Then
        $this->assertDatabaseHas('cities', $attributes + ['id' => $city->id]);

        $response->assertRedirect(route('cp.cities.index'));
        $response->assertSessionHas([
            'message' => "Город {$attributes['name']} успешно изменен.",
        ]);
    }

    /** @test */
    public function it_can_be_deleted()
    {
        // Setup
        $city = CityFactory::new()->create();

        //Given
        $attributes = $city->getAttributes();
        $this->assertDatabaseHas('cities', $attributes);

        // When
        $response = $this->delete($city->path('destroy'));

        // Then
        $this->assertDatabaseMissing('cities', $attributes);

        $response->assertRedirect(route('cp.cities.index'));
        $response->assertSessionHas([
            'message' => "Город {$attributes['name']} успешно удален.",
        ]);
    }
}
