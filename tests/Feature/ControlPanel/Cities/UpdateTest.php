<?php

namespace Tests\Feature\ControlPanel\Cities;

use App\City;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\CityFactory;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected City $city;

    protected function setUp(): void
    {
        parent::setUp();

        $this->signIn();
        $this->city = CityFactory::new()->create();
    }

    /**
     * @test
     * @group validation
     */
    public function it_allows_a_current_name()
    {
        // Given
        $attributes = CityFactory::new()->raw([
            'name' => $this->city->name,
        ]);

        // When
        $response = $this->patch($this->city->path('update'), $attributes);

        // Then
        $response->assertRedirect(route('cp.cities.index'))
            ->assertSessionDoesntHaveErrors()
            ->assertSessionHas(
                'message', "Город {$this->city->name} успешно изменен."
            );
    }
}
