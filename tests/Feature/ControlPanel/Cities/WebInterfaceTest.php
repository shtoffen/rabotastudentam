<?php

namespace Tests\Feature\ControlPanel\Cities;

use Tests\ValidationTestCase;

class WebInterfaceTest extends ValidationTestCase
{
    /** @test */
    public function it_has_the_cities_sidebar_menu_button()
    {
        // When
        $response = $this->get(route('cp.home'));

        // Then
        $response->assertOk()
            ->assertSee('Города')
            ->assertSee(route('cp.cities.index'));
    }
}
