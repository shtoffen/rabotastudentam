<?php

namespace Tests\Feature\ControlPanel\Cities\Validation;

use App\City;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\CityFactory;
use Tests\TestCase;

class UpdateValidationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected City $city;

    protected function setUp(): void
    {
        parent::setUp();

        $this->signIn();
        $this->city = CityFactory::new()->create();
    }

    /** @test */
    public function it_requires_a_name()
    {
        // Given
        $attributes = CityFactory::new()->raw([
            'name' => null,
        ]);

        // When
        $response = $this->patch($this->city->path('update'), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'name' => 'The name field is required.',
        ]);
    }

    /** @test */
    public function it_requires_a_name_to_be_string()
    {
        // Given
        $attributes = CityFactory::new()->raw([
            'name' => [1, 2],
        ]);

        // When
        $response = $this->patch($this->city->path('update'), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'name' => 'The name must be a string.',
        ]);
    }

    /** @test */
    public function it_requires_a_name_to_be_lte_255()
    {
        // Given
        $attributes = CityFactory::new()->raw([
            'name' => $this->faker->regexify('[a-z]{256}'),
        ]);

        // When
        $response = $this->patch($this->city->path('update'), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'name' => 'The name may not be greater than 255 characters.',
        ]);
    }

    /** @test */
    public function it_requires_an_unique_name()
    {
        // Setup
        $existingCity = CityFactory::new()->create([
            'name' => 'test',
        ]);

        // Given
        $attributes = CityFactory::new()->raw([
            'name' => $existingCity->name,
        ]);

        // When
        $response = $this->patch($this->city->path('update'), $attributes);

        // Then
        $response->assertSessionHasErrors([
            'name' => 'The name has already been taken.',
        ]);
    }
}
