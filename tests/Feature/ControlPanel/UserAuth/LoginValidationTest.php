<?php

namespace Tests\Feature\ControlPanel\UserAuth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginValidationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_requires_an_email()
    {
        // Given
        $attributes = [
            'email' => null,
        ];

        // When
        $response = $this->from('/login')->post(route('cp.login'), $attributes);

        // Then
        $response->assertRedirect('/login')
            ->assertSessionHasErrors([
                'email' => 'The email field is required.',
            ]);
    }

    /** @test */
    public function it_requires_an_email_to_be_string()
    {
        // Given
        $attributes = [
            'email' => [1, 2],
        ];

        // When
        $response = $this->from('/login')->post(route('cp.login'), $attributes);

        // Then
        $response->assertRedirect('/login')
            ->assertSessionHasErrors([
                'email' => 'The email must be a string.',
            ]);
    }

    /** @test */
    public function it_requires_a_password()
    {
        // Given
        $attributes = [
            'password' => null,
        ];

        // When
        $response = $this->from('/login')->post(route('cp.login'), $attributes);

        // Then
        $response->assertRedirect('/login')
            ->assertSessionHasErrors([
                'password' => 'The password field is required.',
            ]);
    }

    /** @test */
    public function it_requires_a_password_to_be_password()
    {
        // Given
        $attributes = [
            'password' => [1, 2],
        ];

        // When
        $response = $this->from('/login')->post(route('cp.login'), $attributes);

        // Then
        $response->assertRedirect('/login')
            ->assertSessionHasErrors([
                'password' => 'The password must be a string.',
            ]);
    }

    /** @test */
    public function it_requires_a_correct_credentials()
    {
        // Given
        $attributes = [
            'email' => 'test',
            'password' => 'test',
        ];

        // When
        $response = $this->from('/login')->post(route('cp.login'), $attributes);

        // Then
        $response->assertRedirect('/login')
            ->assertSessionHasErrors([
                'email' => 'These credentials do not match our records.',
            ]);
    }
}
