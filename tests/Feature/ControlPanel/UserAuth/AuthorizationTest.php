<?php

namespace Tests\Feature\ControlPanel\UserAuth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\UserFactory;
use Tests\TestCase;

class AuthorizationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_user_cannot_view_the_login_form_when_authentificated()
    {
        // Setup
        $user = UserFactory::new()->create();

        // Given
        $this->actingAs($user);
        $this->assertAuthenticatedAs($user, 'web');

        // When
        $response = $this->get(route('cp.login.form'));

        // Then
        $response->assertRedirect(route('cp.home'));
    }

    /** @test */
    public function an_user_cannot_logout_when_not_authentificated()
    {
        // Given
        $this->assertGuest('web');

        // When
        $response = $this->post(route('cp.logout'));

        // Then
        $response->assertRedirect(route('cp.login.form'));
    }
}
