<?php

namespace Tests\Feature\ControlPanel\UserAuth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\UserFactory;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_user_can_view_the_logout_button()
    {
        // Setup
        $user = UserFactory::new()->create();

        // Given
        $this->actingAs($user);
        $this->assertAuthenticatedAs($user, 'web');

        // When
        $response = $this->get(route('cp.home'));

        // Then
        $response->assertSuccessful()
            ->assertSee('Выход')
            ->assertSee(route('cp.logout'));
    }

    /** @test */
    public function an_user_can_be_logged_out()
    {
        // Setup
        $user = UserFactory::new()->create();

        // Given
        $this->actingAs($user);
        $this->assertAuthenticatedAs($user, 'web');

        // When
        $response = $this->post(route('cp.logout'));

        // Then
        $response->assertRedirect(route('cp.login.form'));
        $this->assertGuest('web');
    }
}
