<?php

namespace Tests\Feature\ControlPanel\UserAuth;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Factories\UserFactory;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function an_user_can_view_the_login_form()
    {
        // Given
        $this->assertGuest('web');

        // When
        $response = $this->get(route('cp.login.form'));

        // Then
        $response->assertOk()->assertViewIs('control_panel.auth.login');
    }

    /** @test */
    public function an_user_can_be_logged_in()
    {
        // Setup
        $password = $this->faker->password;
        $user = UserFactory::new()->create(['password' => bcrypt($password)]);

        // Given
        $attributes = [
            'email' => $user->email,
            'password' => $password
        ];
        $this->assertGuest('web');

        // When
        $response = $this->post(route('cp.login'), $attributes);

        // Then
        $response->assertRedirect('/cp');
        $this->assertAuthenticatedAs($user, 'web');
        $this->assertNull($user->fresh()->remember_token);
    }

    /** @test */
    public function an_user_can_be_logged_in_with_remembered_flag()
    {
        // Setup
        $password = $this->faker->password;
        $user = UserFactory::new()->create(['password' => bcrypt($password)]);

        // Given
        $attributes = [
            'email' => $user->email,
            'password' => $password,
            'remember' => 'on',
        ];
        $this->assertGuest('web');
        $this->assertNull($user->remember_token);

        // When
        $response = $this->post(route('cp.login'), $attributes);

        // Then
        $response->assertRedirect('/cp');
        $this->assertAuthenticatedAs($user, 'web');
        $this->assertNotNull($user->fresh()->remember_token);
    }

    /** @test */
    public function an_user_cannot_attempt_to_login_more_than_five_times_per_one_minute()
    {
        // Setup
        $user = UserFactory::new()->create(['password' => bcrypt('123')]);

        // Given
        $invalidCredentials = [
            'email' => $user->email,
            'password' => 'test',
        ];

        // When
        $route = route('cp.login');
        for ($i = 1; $i <= 5; $i++) $this->post($route, $invalidCredentials);
        $response = $this->post($route, $invalidCredentials);

        // Then
        $response->assertSessionHasErrors([
            'email' => 'Too many login attempts. Please try again in 60 seconds.',
        ]);
    }
}
