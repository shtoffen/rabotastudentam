<?php

namespace Tests\Feature\ControlPanel\Stories;

use App\Story;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\Factories\StoryFactory;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_not_allowed_to_guests()
    {
        // Setup
        $story = StoryFactory::new()->create();

        // Given
        $this->assertGuest();

        // When
        $responseA = $this->get($story->path('edit'));
        $responseB = $this->patch($story->path('update'));

        // Then
        $responseA->assertRedirect(route('cp.login'));
        $responseB->assertRedirect(route('cp.login'));
    }

    /** @test */
    public function it_has_the_editing_form()
    {
        // Given
        $story = StoryFactory::new()->create();

        // When
        $this->signIn();
        $response = $this->get($story->path('edit'));

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.stories.edit')
            ->assertViewHas('story', $story);
    }

    /** @test */
    public function it_can_be_updated_without_new_image()
    {
        // Setup
        Storage::fake($disk = Story::IMAGES_DISK);
        $story = StoryFactory::new()->withImage(true)->create();

        // Given
        $attributes = [
            'title' => $this->faker->sentence,
            'description' => $this->faker->sentence,
        ];
        Storage::disk($disk)->assertExists($oldFile = $story->image->path);

        // When
        $this->signIn();
        $response = $this->patch($story->path('update'), $attributes);

        // Then
        $response->assertSessionDoesntHaveErrors()
            ->assertRedirect($story->category->path());

        $story->refresh();
        $this->assertDatabaseHas('stories', $attributes + [
            'id' => $story->id,
        ]);
        Storage::disk($disk)->assertExists($oldFile);
    }

    /** @test */
    public function it_can_be_updated_with_new_image()
    {
        // Setup
        Storage::fake($disk = Story::IMAGES_DISK);
        $story = StoryFactory::new()->withImage(true)->create();

        // Given
        $attributes = [
            'title' => $this->faker->sentence,
            'description' => $this->faker->sentence,
        ];
        Storage::disk($disk)->assertExists($oldFile = $story->image->path);

        // When
        $this->signIn();
        $response = $this->patch($story->path(), $attributes + [
            'image' => UploadedFile::fake()->image('test.jpg'),
        ]);

        // Then
        $response->assertSessionDoesntHaveErrors()
            ->assertRedirect($story->category->path());

        $story->refresh();
        $this->assertDatabaseHas('stories', $attributes + [
            'id' => $story->id,
        ]);
        Storage::disk($disk)->assertMissing($oldFile);
        Storage::disk($disk)->assertExists($story->image->path);
    }
}
