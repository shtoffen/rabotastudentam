<?php

namespace Tests\Feature\ControlPanel\Stories\Validation;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\ValidationTestCase;

class UpdateValidationTest extends ValidationTestCase
{
    use WithFaker;

    protected string $url;

    protected function setUp(): void
    {
        parent::setUp();

        $this->url = route('cp.stories.update', ['story' => '1']);
    }

    /** @test */
    public function it_requires_a_title()
    {
        // Given
        $attributes = [
            'title' => null,
        ];

        // When
        $response = $this->patch($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'title' => 'The title field is required.',
        ]);
    }

    /** @test */
    public function it_requires_a_title_to_be_string()
    {
        // Given
        $attributes = [
            'title' => [1, 2],
        ];

        // When
        $response = $this->patch($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'title' => 'The title must be a string.',
        ]);
    }

    /** @test */
    public function it_requires_a_title_to_be_lte_255()
    {
        // Given
        $attributes = [
            'title' => $this->faker->regexify('a{256}'),
        ];

        // When
        $response = $this->patch($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'title' => 'The title may not be greater than 255 characters.',
        ]);
    }

    /** @test */
    public function it_requires_a_description()
    {
        // Given
        $attributes = [
            'description' => null,
        ];

        // When
        $response = $this->patch($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'description' => 'The description field is required.',
        ]);
    }

    /** @test */
    public function it_requires_a_description_to_be_string()
    {
        // Given
        $attributes = [
            'description' => [1, 2],
        ];

        // When
        $response = $this->patch($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'description' => 'The description must be a string.',
        ]);
    }

    /** @test */
    public function it_requires_a_description_to_be_lte_2500()
    {
        // Given
        $attributes = [
            'description' => $this->faker->regexify('a{2501}'),
        ];

        // When
        $response = $this->patch($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'description' => 'The description may not be greater than 2500 characters.',
        ]);
    }

    /** @test */
    public function it_requires_an_image_to_be_image()
    {
        // Given
        $attributes = [
            'image' => 'test',
        ];

        // When
        $response = $this->patch($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'image' => 'The image must be an image.',
        ]);
    }

    /** @test */
    public function it_requires_an_image_to_be_lte_10000()
    {
        // Given
        $attributes = [
            'image' => UploadedFile::fake()->image('test.jpg')->size(10001),
        ];

        // When
        $response = $this->patch($this->url, $attributes);

        // Then
        $response->assertSessionHasErrors([
            'image' => 'The image may not be greater than 10000 kilobytes.',
        ]);
    }
}
