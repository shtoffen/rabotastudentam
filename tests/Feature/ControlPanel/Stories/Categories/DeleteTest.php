<?php

namespace Tests\Feature\ControlPanel\Stories\Categories;

use App\Enums\ImageContentType;
use App\Image;
use App\StoryCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Tests\Factories\ImageFactory;
use Tests\Factories\StoryCategoryFactory;
use Tests\TestCase;

class DeleteTest extends TestCase
{
    use RefreshDatabase;

    protected StoryCategory $category;

    protected function setUp(): void
    {
        parent::setUp();

        $this->category = StoryCategoryFactory::new()->create();
    }

    /** @test */
    public function it_not_allowed_to_guests()
    {
        // Given
        $this->assertGuest();

        // When
        $response = $this->delete($this->category->path());

        // Then
        $response->assertRedirect(route('cp.login'));
    }

    /** @test */
    public function it_can_be_deleted()
    {
        // When
        $this->signIn();
        $response = $this->delete($this->category->path());

        // Then
        $response->assertSessionDoesntHaveErrors()
            ->assertRedirect(route('cp.stories.categories.index'));

        $this->assertSame(0, StoryCategory::count());
    }

    /** @test */
    public function it_deletes_image()
    {
        // Setup
        Storage::fake($disk = StoryCategory::IMAGES_DISK);
        ImageFactory::new()
            ->forModel($this->category)
            ->withStoredImage(StoryCategory::IMAGES_PATH)
            ->create(['content_type' => ImageContentType::STORY_CATEGORY]);

        // Given
        $attributes = $this->category->getAttributes();
        $this->assertDatabaseHas('story_categories', $attributes);
        $this->assertSame(1, Image::count());
        Storage::disk($disk)->assertExists($path = $this->category->image->path);

        // When
        $this->signIn();
        $response = $this->delete($this->category->path());

        // Then
        $response->assertSessionDoesntHaveErrors()
            ->assertRedirect(route('cp.stories.categories.index'));

        $this->assertDatabaseMissing('story_categories', $attributes);
        $this->assertSame(0, Image::count());
        Storage::disk($disk)->assertMissing($path);
    }
}
