<?php

namespace Tests\Feature\ControlPanel\Stories\Categories;

use App\StoryCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\StoryCategoryFactory;
use Tests\Factories\StoryFactory;
use Tests\TestCase;

class ShowTest extends TestCase
{
    use RefreshDatabase;

    protected StoryCategory $category;

    protected function setUp(): void
    {
        parent::setUp();

        $this->category = StoryCategoryFactory::new()->withImage()->create();
    }

    /** @test */
    public function it_not_allowed_to_guests()
    {
        // Given
        $this->assertGuest();

        // When
        $response = $this->get($this->category->path());

        // Then
        $response->assertRedirect(route('cp.login'));
    }

    /** @test */
    public function it_has_the_card()
    {
        // Setup
        StoryFactory::new()
            ->forCategory($this->category)
            ->withImage()
            ->times(2);

        // Given
        $stories = $this->category->stories()->with('image')->get();

        // When
        $this->signIn();
        $response = $this->get($this->category->path());

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.stories.categories.show')
            ->assertViewHas('category', $this->category)
            ->assertViewHas('stories', $stories);
    }
}
