<?php

namespace Tests\Feature\ControlPanel\Stories\Categories;

use App\StoryCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class CreateTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_not_allowed_to_guests()
    {
        // Given
        $this->assertGuest();

        // When
        $responseA = $this->get(route('cp.stories.categories.create'));
        $responseB = $this->post(route('cp.stories.categories.store'));

        // Then
        $responseA->assertRedirect(route('cp.login'));
        $responseB->assertRedirect(route('cp.login'));
    }

    /** @test */
    public function it_has_the_creating_form()
    {
        // When
        $this->signIn();
        $response = $this->get(route('cp.stories.categories.create'));

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.stories.categories.create');
    }

    /** @test */
    public function it_can_be_created()
    {
        // Setup
        Storage::fake($disk = StoryCategory::IMAGES_DISK);

        // Given
        $attributes = [
            'title'         => $this->faker->sentence,
            'image'         => UploadedFile::fake()->image('image.jpg'),
        ];
        $this->assertSame(0, StoryCategory::count());

        // When
        $this->signIn();
        $response = $this->post(route('cp.stories.categories.store'), $attributes);

        // Then
        $response->assertSessionDoesntHaveErrors();

        $this->assertSame(1, StoryCategory::count());
        $category = StoryCategory::first();

        $response->assertRedirect($category->path());
        Storage::disk($disk)->assertExists($category->image->path);
    }
}
