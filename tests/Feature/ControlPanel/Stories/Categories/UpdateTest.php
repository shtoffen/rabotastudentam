<?php

namespace Tests\Feature\ControlPanel\Stories\Categories;

use App\StoryCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\Factories\StoryCategoryFactory;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_not_allowed_to_guests()
    {
        // Setup
        $category = StoryCategoryFactory::new()->create();

        // Given
        $this->assertGuest();

        // When
        $responseA = $this->get($category->path('edit'));
        $responseB = $this->patch($category->path());

        // Then
        $responseA->assertRedirect(route('cp.login'));
        $responseB->assertRedirect(route('cp.login'));
    }

    /** @test */
    public function it_has_the_editing_form()
    {
        // Given
        $category = StoryCategoryFactory::new()->create();

        // When
        $this->signIn();
        $response = $this->get($category->path('edit'));

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.stories.categories.edit')
            ->assertViewHas('category', $category);
    }

    /** @test */
    public function it_can_be_updated_without_new_image()
    {
        // Setup
        Storage::fake($disk = StoryCategory::IMAGES_DISK);
        $category = StoryCategoryFactory::new()->withImage(true)->create();

        // Given
        $attributes = [
            'title' => $this->faker->sentence,
        ];
        Storage::disk($disk)->assertExists($oldFile = $category->image->path);

        // When
        $this->signIn();
        $response = $this->patch($category->path(), $attributes);

        // Then
        $response->assertSessionDoesntHaveErrors()
            ->assertRedirect($category->path());

        $category->refresh();
        $this->assertDatabaseHas('story_categories', $attributes + [
            'id' => $category->id,
        ]);
        Storage::disk($disk)->assertExists($oldFile);
    }

    /** @test */
    public function it_can_be_updated_with_new_image()
    {
        // Setup
        Storage::fake($disk = StoryCategory::IMAGES_DISK);
        $category = StoryCategoryFactory::new()->withImage(true)->create();

        // Given
        $attributes = [
            'title' => $this->faker->sentence,
        ];
        Storage::disk($disk)->assertExists($oldFile = $category->image->path);

        // When
        $this->signIn();
        $response = $this->patch($category->path(), $attributes + [
            'image' => UploadedFile::fake()->image('test.jpg'),
        ]);

        // Then
        $response->assertSessionDoesntHaveErrors()
            ->assertRedirect($category->path());

        $category->refresh();
        $this->assertDatabaseHas('story_categories', $attributes + [
            'id' => $category->id,
        ]);
        Storage::disk($disk)->assertMissing($oldFile);
        Storage::disk($disk)->assertExists($category->image->path);
    }
}
