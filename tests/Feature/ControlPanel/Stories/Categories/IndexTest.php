<?php

namespace Tests\Feature\ControlPanel\Stories\Categories;

use App\StoryCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Factories\StoryCategoryFactory;
use Tests\TestCase;

class IndexTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_not_allowed_to_guests()
    {
        // Given
        $this->assertGuest();

        // When
        $response = $this->get(route('cp.stories.categories.index'));

        // Then
        $response->assertRedirect(route('cp.login'));
    }

    /** @test */
    public function it_has_the_index_page()
    {
        // Setup
        StoryCategoryFactory::new()->withImage()->times(2);

        // Given
        $categories = StoryCategory::with('image')->get();

        // When
        $this->signIn();
        $response = $this->get(route('cp.stories.categories.index'));

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.stories.categories.index')
            ->assertViewHas('categories', $categories);
    }

    /** @test */
    public function it_has_the_stories_sidebar_menu_button()
    {
        // Given
        $this->signIn();

        // When
        $response = $this->get(route('cp.home'));

        // Then
        $response->assertOk()
            ->assertSee('Stories')
            ->assertSee(route('cp.stories.categories.index'));
    }
}
