<?php

namespace Tests\Feature\ControlPanel\Stories;

use App\Enums\ImageContentType;
use App\Image;
use App\Story;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Tests\Factories\ImageFactory;
use Tests\Factories\StoryFactory;
use Tests\TestCase;

class DeleteTest extends TestCase
{
    use RefreshDatabase;

    protected Story $story;

    protected function setUp(): void
    {
        parent::setUp();

        $this->story = StoryFactory::new()->create();
    }

    /** @test */
    public function it_not_allowed_to_guests()
    {
        // Given
        $this->assertGuest();

        // When
        $response = $this->delete($this->story->path('destroy'));

        // Then
        $response->assertRedirect(route('cp.login'));
    }

    /** @test */
    public function it_can_be_deleted()
    {
        // Given
        $category = $this->story->category;
        $attributes = $this->story->getAttributes();
        $this->assertDatabaseHas('stories', $attributes);

        // When
        $this->signIn();
        $response = $this->delete($this->story->path());

        // Then
        $response->assertSessionDoesntHaveErrors()
            ->assertRedirect($category->path());

        $this->assertSame(0, Story::count());
        $this->assertDatabaseMissing('stories', $attributes);
    }

    /** @test */
    public function it_deletes_image()
    {
        // Setup
        Storage::fake($disk = Story::IMAGES_DISK);
        ImageFactory::new()
            ->forModel($this->story)
            ->withStoredImage(Story::IMAGES_PATH)
            ->create(['content_type' => ImageContentType::STORY]);

        // Given
        $this->assertSame(1, Image::count());
        Storage::disk($disk)->assertExists($path = $this->story->image->path);

        // When
        $this->signIn();
        $response = $this->delete($this->story->path('destroy'));

        // Then
        $response->assertSessionDoesntHaveErrors()->assertRedirect();

        $this->assertSame(0, Image::count());
        Storage::disk($disk)->assertMissing($path);
    }
}
