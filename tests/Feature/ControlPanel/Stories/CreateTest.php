<?php

namespace Tests\Feature\ControlPanel\Stories;

use App\Story;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\Factories\StoryCategoryFactory;
use Tests\TestCase;

class CreateTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function it_not_allowed_to_guests()
    {
        // Given
        $this->assertGuest();

        // When
        $responseA = $this->get(route('cp.stories.create'));
        $responseB = $this->post(route('cp.stories.store'));

        // Then
        $responseA->assertRedirect(route('cp.login'));
        $responseB->assertRedirect(route('cp.login'));
    }

    /** @test */
    public function it_has_the_creating_form()
    {
        // When
        $this->signIn();
        $response = $this->get(route('cp.stories.create', ['category_id' => 0]));

        // Then
        $response->assertOk()
            ->assertViewIs('control_panel.stories.create');
    }

    /** @test */
    public function it_can_be_created()
    {
        // Setup
        Storage::fake($disk = Story::IMAGES_DISK);

        // Given
        $attributes = [
            'category_id'   => StoryCategoryFactory::new()->create()->id,
            'description'   => $this->faker->sentence,
            'title'         => $this->faker->sentence,
        ];
        $this->assertSame(0, Story::count());

        // When
        $this->signIn();
        $response = $this->post(route('cp.stories.store'), $attributes + [
            'image'         => UploadedFile::fake()->image('image.jpg'),
        ]);

        // Then
        $response->assertSessionDoesntHaveErrors();

        $this->assertSame(1, Story::count());
        $this->assertDatabaseHas('stories', $attributes);
        $story = Story::first();

        $response->assertRedirect($story->category->path());
        Storage::disk($disk)->assertExists($story->image->path);
    }
}
