<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Passport\Passport;
use Tests\Factories\UserFactory;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function signIn(User $user = null)
    {
        return $this->actingAs($user ?? UserFactory::new()->create());
    }

    protected function signInApi($user = null)
    {
        Passport::actingAs($user ?? UserFactory::new()->create());

        return $this;
    }
}
