<?php

namespace Tests;

use BenSampo\Enum\Enum;

abstract class DataProvidersTestCase extends TestCase
{
    use CreatesApplication;

    /**
     * Initialize application in constructor for working data providers.
     * Because data providers is processing before application launch.
     * https://github.com/sebastianbergmann/phpunit/issues/836
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->createApplication();
    }

    /**
     * Get set of allowed/restricted cases.
     *
     * @param Enum  $class  Base Enum class.
     * @param Enum[]  $allowedKeys  Allowed Enum instances.
     * @return array
     */
    protected function getSetOfPermissions(
        string $class,
        array $allowedKeys
    ): array {
        $allowedCases = [];

        foreach ($allowedKeys as $allowedKey) {
            $allowedCases[$allowedKey->key] = [$allowedKey->value, true];
        }

        return collect($class::getInstances())
            ->except(array_keys($allowedCases))
            ->mapWithKeys(function (Enum $item) {
                return [$item->key => [$item->value, false]];
            })
            ->merge($allowedCases)
            ->toArray();
    }

    /**
     * Get double set of allowed/restricted cases.
     *
     * @param Enum  $firstClass  First Enum class
     * @param Enum  $secondClass  Second Enum class
     * @param array[] $allowedPairs
     * @return void
     */
    protected function getDoubleSetOfPermissions(
        string $firstClass,
        string $secondClass,
        array $allowedPairs
    ) {
        return collect($firstClass::getValues())
            ->crossJoin($secondClass::getValues())
            ->map(function ($value) use ($allowedPairs) {
                in_array($value, $allowedPairs)
                    ? $value[2] = true
                    : $value[2] = false;

                return $value;
            });
    }
}
