<?php

use App\Http\Controllers\ControlPanel\AgentsController;
use App\Http\Controllers\ControlPanel\CitiesController;
use App\Http\Controllers\ControlPanel\EducationStepsController;
use App\Http\Controllers\ControlPanel\HomeController;
use App\Http\Controllers\ControlPanel\StoriesController;
use App\Http\Controllers\ControlPanel\StoryCategoriesController;
use Illuminate\Support\Facades\Route;

Route::get('/', HomeController::class)->name('home');

Route::resource('agents', AgentsController::class)->except('create', 'store', 'destroy');
Route::resource('cities', CitiesController::class)->except('show');
Route::resource('education/steps', EducationStepsController::class)->names('education.steps');
Route::resource('stories/categories', StoryCategoriesController::class)->names('stories.categories');
Route::resource('stories', StoriesController::class)->except('index', 'show');
