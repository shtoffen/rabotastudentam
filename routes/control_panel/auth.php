<?php

use App\Http\Controllers\ControlPanel\UserLoginController;
use Illuminate\Support\Facades\Route;

Route::get('login', [UserLoginController::class, 'showLoginForm'])
    ->name('login.form');

Route::post('login', [UserLoginController::class, 'login'])
    ->name('login');

Route::post('logout', [UserLoginController::class, 'logout'])
    ->name('logout');
