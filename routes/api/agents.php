<?php

use App\Http\Controllers\API\AgentsController;
use App\Http\Controllers\API\ForgotPasswordController;
use Illuminate\Support\Facades\Route;

Route::post('/', [AgentsController::class, 'store'])->name('store');
Route::post('/reset_password', [ForgotPasswordController::class, 'sendResetLinkEmail'])
    ->name('reset_password');

