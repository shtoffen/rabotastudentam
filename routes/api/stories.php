<?php

use App\Http\Controllers\API\StoriesController;
use App\Http\Controllers\API\StoryCategoriesController;
use Illuminate\Support\Facades\Route;

Route::get('/categories', [StoryCategoriesController::class, 'index'])
    ->name('categories.index');

Route::get('/', [StoriesController::class, 'index'])
    ->name('index');

