<?php

use App\Http\Controllers\API\CitiesController;
use Illuminate\Support\Facades\Route;

Route::get('/', [CitiesController::class, 'index'])->name('index');
Route::get('/{city}', [CitiesController::class, 'show'])->name('show');

