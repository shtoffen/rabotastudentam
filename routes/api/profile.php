<?php

use App\Http\Controllers\API\Profile\FcmTokenController;
use App\Http\Controllers\API\ProfileController;
use Illuminate\Support\Facades\Route;

Route::get('/', [ProfileController::class, 'show'])->name('show');

Route::patch('/', [ProfileController::class, 'update'])
    ->middleware('can:agent-employed')
    ->name('update');

Route::put('/fcm_token', FcmTokenController::class)->name('fcm_token');

