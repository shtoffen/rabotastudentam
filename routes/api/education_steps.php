<?php

use App\Http\Controllers\API\EducationStepsController;
use Illuminate\Support\Facades\Route;

Route::get('/', [EducationStepsController::class, 'index'])->name('index');
Route::get('/{step}', [EducationStepsController::class, 'show'])->name('show');
Route::patch('/{step}', [EducationStepsController::class, 'read'])->name('read');
Route::delete('/{step}', [EducationStepsController::class, 'unread'])->name('unread');
