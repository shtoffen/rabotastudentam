<?php

use App\Http\Controllers\AgentsResetPasswordController;
use Illuminate\Support\Facades\Route;

Route::get('agents/password/reset/success',  [AgentsResetPasswordController::class, 'showPasswordReseted'])
    ->name('agents.password.reset.success');

Route::get('agents/password/reset/{token}',  [AgentsResetPasswordController::class, 'showResetForm'])
    ->name('agents.password.reset');

Route::post('agents/password/reset/{token}', [AgentsResetPasswordController::class, 'reset'])
    ->name('agents.password.update');
