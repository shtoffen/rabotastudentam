<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapControlPanelRoutes();

        $this->mapAgentResetPasswordRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAgentResetPasswordRoutes()
    {
        Route::middleware('web')
            ->group(base_path('routes/agents_reset_password.php'));
    }

    /**
     * Define the "control panel" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapControlPanelRoutes()
    {
        Route::middleware('web')
            ->prefix('cp')
            ->name('cp.')
            ->group(function () {
                Route::prefix('/')
                    ->group(base_path('routes/control_panel/auth.php'));

                Route::middleware('auth')
                    ->group(base_path('routes/control_panel/routes.php'));
            });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api/v1')
             ->middleware('api')
             ->name('api.')
             ->group(function () {
                Route::prefix('agents')
                    ->name('agents.')
                    ->group(base_path('routes/api/agents.php'));

                Route::prefix('cities')
                    ->name('cities.')
                    ->group(base_path('routes/api/cities.php'));

                Route::prefix('profile')
                    ->name('profile.')
                    ->middleware('auth:api')
                    ->group(base_path('routes/api/profile.php'));

                Route::prefix('education/steps')
                    ->name('education.steps.')
                    ->middleware('auth:api', 'can:agent-employed')
                    ->group(base_path('routes/api/education_steps.php'));

                Route::prefix('stories')
                    ->name('stories.')
                    ->middleware('auth:api', 'can:agent-employed')
                    ->group(base_path('routes/api/stories.php'));
             });
    }
}
