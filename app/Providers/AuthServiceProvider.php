<?php

namespace App\Providers;

use App\Enums\AgentStatus;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerGates();

        Passport::routes(fn ($router) => $router->forAccessTokens(), [
            'prefix' => 'api/v1/agents/oauth',
        ]);
    }

    protected function registerGates()
    {
        Gate::define(
            'agent-employed',
            fn ($user) => $user->status->is(AgentStatus::EMPLOYED)
        );
    }
}
