<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Firebase Cloud Messaging Token
 */
class FcmToken extends Model
{
    /**
     * Indicates if all mass assignment is enabled.
     *
     * @var bool
     */
    protected static $unguarded = true;

    // Relations
    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }
    // End of Relations
}
