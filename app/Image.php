<?php

namespace App;

use App\Enums\ImageContentType;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    use CastsEnums;

    public const MIME_TYPES = [
        'image/bmp',
        'image/gif',
        'image/jpeg',
        'image/png',
        'image/svg+xml',
        'image/webp',
    ];

    /**
     * The attributes that should be casted to enum types.
     *
     * @var array
     */
    protected $enumCasts = [
        'content_type' => ImageContentType::class,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'content_type' => 'int',
    ];

    /**
     * Indicates if all mass assignment is enabled.
     *
     * @var bool
     */
    protected static $unguarded = true;

    /**
     * Boot the image instance.
     */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($image) {
            Storage::disk($image->disk)->delete($image->path);
        });
    }

    // Attributes
    public function getUrlAttribute()
    {
        return Storage::disk($this->disk)->url($this->path);
    }
    // End of Attributes

    // Relations
    public function imageable()
    {
        return $this->morphTo();
    }
    // End of Relations
}
