<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use App\Enums\ImageContentType;

class StoryCategory extends Model
{
    public const IMAGES_DISK        = 'public';
    public const IMAGES_PATH        = 'stories/categories';

    /**
     * Indicates if all mass assignment is enabled.
     *
     * @var bool
     */
    protected static $unguarded = true;

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::deleting(function ($category) {
            optional($category->image)->delete();
            $category->stories->each(fn ($story) => $story->delete());
        });
    }

    // Helpers
    public function path(string $route = 'show'): string
    {
        if (!in_array($route, ['edit', 'show'])) {
            throw new \InvalidArgumentException('Invalid route.');
        }

        return route("cp.stories.categories.{$route}", ['category' => $this]);
    }

    public function storeImage(UploadedFile $file = null): void
    {
        if (is_null($file)) {
            return;
        }

        if (!is_null($this->image)) {
            $this->image->delete();
        }

        $imagePath = $file->store(self::IMAGES_PATH, self::IMAGES_DISK);

        $this->image()->create([
            'path'          => $imagePath,
            'disk'          => self::IMAGES_DISK,
            'content_type'  => ImageContentType::STORY_CATEGORY,
        ]);
    }
    // End of Helpers

    // Relations
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable')
            ->where('content_type', ImageContentType::STORY_CATEGORY);
    }

    public function stories()
    {
        return $this->hasMany(Story::class, 'category_id');
    }
    // End of Relations
}
