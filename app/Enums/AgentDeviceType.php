<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static IOS()
 * @method static static ANDROID()
 */
final class AgentDeviceType extends Enum implements LocalizedEnum
{
    /** @var int */
    public const IOS = 0;

    /** @var int */
    public const ANDROID = 1;
}
