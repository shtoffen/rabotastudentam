<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static UNKNOWN()
 * @method static static PASSPORT_PHOTO_SECTION()
 * @method static static PASSPORT_REGISTRATION_SECTION()
 * @method static static STORY_CATEGORY()
 * @method static static STORY()
 */
final class ImageContentType extends Enum
{
    public const UNKNOWN                            = 0;
    public const PASSPORT_PHOTO_SECTION             = 1;
    public const PASSPORT_REGISTRATION_SECTION      = 2;
    public const STORY_CATEGORY                     = 3;
    public const STORY                              = 4;
}
