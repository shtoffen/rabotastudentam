<?php

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static PENDING()
 * @method static static EMPLOYED()
 * @method static static REJECTED()
 */
final class AgentStatus extends Enum implements LocalizedEnum
{
    /** @var int */
    public const PENDING = 0;

    /** @var int */
    public const EMPLOYED = 1;

    /** @var int */
    public const REJECTED = 2;
}
