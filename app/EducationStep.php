<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationStep extends Model
{
    /**
     * Indicates if all mass assignment is enabled.
     *
     * @var bool
     */
    protected static $unguarded = true;

    // Helpers
    public function apiPath()
    {
        return route('api.education.steps.show', ['step' => $this]);
    }

    public function path($route = 'show')
    {
        if (!in_array($route, ['edit', 'show'])) {
            throw new \InvalidArgumentException('Invalid route.');
        }

        return route("cp.education.steps.{$route}", ['step' => $this]);
    }
    // End of Helpers
}
