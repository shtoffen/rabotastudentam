<?php

namespace App;

use App\Enums\ImageContentType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Story extends Model
{
    public const IMAGES_DISK        = 'public';
    public const IMAGES_PATH        = 'stories/items';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'category_id' => 'int',
    ];

    /**
     * Indicates if all mass assignment is enabled.
     *
     * @var bool
     */
    protected static $unguarded = true;

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::deleting(fn ($story) => optional($story->image)->delete());
    }

    // Helpers
    public function path(string $route = 'update'): string
    {
        if (!in_array($route, ['destroy', 'edit', 'update'])) {
            throw new \InvalidArgumentException('Invalid route.');
        }

        return route("cp.stories.{$route}", ['story' => $this]);
    }

    public function storeImage(UploadedFile $file = null): void
    {
        if (is_null($file)) {
            return;
        }

        if (!is_null($this->image)) {
            $this->image->delete();
        }

        $imagePath = $file->store(self::IMAGES_PATH, self::IMAGES_DISK);

        $this->image()->create([
            'path'          => $imagePath,
            'disk'          => self::IMAGES_DISK,
            'content_type'  => ImageContentType::STORY,
        ]);
    }
    // End of Helpers

    // Relations
    public function category()
    {
        return $this->belongsTo(StoryCategory::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable')
            ->where('content_type', ImageContentType::STORY);
    }
    // End of Relations
}
