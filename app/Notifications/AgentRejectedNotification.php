<?php

namespace App\Notifications;

use App\Enums\AgentStatus;
use App\Http\Resources\API\EnumResource;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use NotificationChannels\Fcm\Resources\Notification as FcmNotification;

class AgentRejectedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            FcmChannel::class,
        ];
    }

    /**
     * Get the FCM representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return FcmMessage
     */
    public function toFcm($notifiable): FcmMessage
    {
        $notification = FcmNotification::create()
            ->setTitle('Ваша заявка отклонена.')
            ->setBody('Здравствуйте, к сожалению, ваша заявка на работу отклонена.');

        $data = [
            'category' => 'status_changed',
            'status_id' => (string) AgentStatus::REJECTED()->value,
            'status_name' => AgentStatus::REJECTED()->description,
        ];

        return FcmMessage::create()
            ->setNotification($notification)
            ->setData($data);
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return [
            'push_notification',
        ];
    }
}
