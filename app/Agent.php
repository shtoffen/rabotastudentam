<?php

namespace App;

use App\Enums\AgentDeviceType;
use App\Enums\AgentStatus;
use BenSampo\Enum\Traits\CastsEnums;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use InvalidArgumentException;
use Laravel\Passport\HasApiTokens;

/**
 * @property \App\Enums\AgentStatus|null $status
 * @method \App\City city()
 * @method static \App\City city()
 * @property \App\City $city
 */
class Agent extends Authenticatable
{
    use CastsEnums, HasApiTokens, Notifiable;

    public const PASSPORT_IMAGES_PATH = 'agent_passport_images';
    public const PASSPORT_IMAGES_DISK = 'public';

    /**
     * Indicates if all mass assignment is enabled.
     *
     * @var bool
     */
    protected static $unguarded = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be casted to enum types.
     *
     * @var array
     */
    protected $enumCasts = [
        'device_type' => AgentDeviceType::class,
        'status' => AgentStatus::class,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'device_type' => 'int',
        'status' => 'int',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::deleting(function ($agent) {
            $agent->images->each(fn ($image) => $image->delete());
        });
    }

    /**
     * Specifies the user's FCM token
     *
     * @return string
     */
    public function routeNotificationForFcm()
    {
        return optional($this->fcmToken)->token;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $notification = new ResetPasswordNotification($token);
        $notification->createUrlUsing(function (Agent $notifiable, $token) {
            return url(route('agents.password.reset', [
                'token' => $token,
                'email' => $notifiable->getEmailForPasswordReset(),
            ], false));
        });

        $this->notify($notification);
    }

    // Relations
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function fcmToken()
    {
        return $this->hasOne(FcmToken::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function educationSteps()
    {
        return $this->belongsToMany(
            EducationStep::class,
            'agent_education_step',
            'agent_id',
            'education_step_id'
        )
            ->withPivot('read');
    }
    // End of Relations

    // Helpers
    public function path($route = 'show')
    {
        if (!in_array($route, ['edit', 'show'])) {
            throw new InvalidArgumentException(
                'Only edit and show routes are allowed.'
            );
        }

        return route("cp.agents.{$route}", ['agent' => $this]);
    }
    // End of Helpers
}
