<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;

class City extends Model
{
    /**
     * Indicates if all mass assignment is enabled.
     *
     * @var bool
     */
    protected static $unguarded = true;

    // Helpers
    public function apiPath()
    {
        return route('api.cities.show', ['city' => $this]);
    }

    public function path($route)
    {
        if (!in_array($route, ['edit', 'update', 'destroy'])) {
            throw new InvalidArgumentException(
                'Only edit, update and delete routes are allowed.'
            );
        }

        return route("cp.cities.{$route}", ['city' => $this]);
    }
    // End of Helpers

    // Relationships
    public function agents()
    {
        return $this->hasMany(Agent::class);
    }
    // End of Relationships
}
