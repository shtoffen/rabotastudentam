<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class EqualsTo implements Rule
{
    protected $attribute;
    protected $value;

    /**
     * Create a new rule instance.
     *
     * @param mixed $type
     * @return void
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;

        return (int) $value === $this->value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.equals_to', [
            'attribute' => $this->attribute,
            'value' => $this->value,
        ]);
    }
}
