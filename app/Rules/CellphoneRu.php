<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CellphoneRu implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('/^\+79\d{9}$/', $value) > 0;
    }


    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.cellphone_ru');
    }
}
