<?php

namespace App\Http\Resources\API;

use Illuminate\Http\Resources\Json\JsonResource;

class EducationStepPreviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\EducationStep $this */

        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'read'              => $this->pivot->read,
        ];
    }
}
