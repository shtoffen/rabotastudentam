<?php

namespace App\Http\Resources\API;

use Illuminate\Http\Resources\Json\JsonResource;

class EducationStepResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\EducationStep $this */

        return [
            'id'            => $this->id,
            'description'   => $this->description,
            'name'          => $this->name,
            'read'          => $this->pivot->read,
        ];
    }
}
