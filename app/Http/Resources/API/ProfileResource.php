<?php

namespace App\Http\Resources\API;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'address'           => $this->address,
            'cellphone'         => $this->cellphone,
            'city'              => CityResource::make($this->city),
            'date_of_birth'     => $this->date_of_birth,
            'email'             => $this->email,
            'name'              => $this->name,
            'passport_images'   => ImageResource::collection($this->images),
            'post_code'         => $this->post_code,
            'status'            => EnumResource::make($this->status),
        ];
    }
}
