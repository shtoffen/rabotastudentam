<?php

namespace App\Http\Controllers\API;

use App\Agent;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\Agents\StoreAgent;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class AgentsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\API\Agents\StoreAgent  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAgent $request)
    {
        $agent = Agent::make($request->only([
            'address',
            'cellphone',
            'city_id',
            'email',
            'date_of_birth',
            'name',
            'post_code',
        ]));

        $agent->password = Hash::make($request->password);
        $agent->save();

        foreach ($request->passport_images as $uploadedImage) {
            $path = $uploadedImage['image']->store(
                Agent::PASSPORT_IMAGES_PATH,
                Agent::PASSPORT_IMAGES_DISK
            );

            $agent->images()->create([
                'content_type'  => $uploadedImage['type'],
                'disk'          => Agent::PASSPORT_IMAGES_DISK,
                'path'          => $path,
            ]);
        }

        return response()->json([], Response::HTTP_CREATED);
    }
}
