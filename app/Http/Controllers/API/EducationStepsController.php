<?php

namespace App\Http\Controllers\API;

use App\EducationStep;
use App\Http\Controllers\Controller;
use App\Http\Resources\API\EducationStepPreviewResource;
use App\Http\Resources\API\EducationStepResource;
use Illuminate\Http\Request;

class EducationStepsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /** @var \App\Agent */
        $agent = $request->user();

        $educationSteps = EducationStep::pluck('id');
        $agent->educationSteps()->syncWithoutDetaching($educationSteps);

        return EducationStepPreviewResource::collection($agent->educationSteps);
    }

    /**
     * Display the specified resource.
     *
     * @param \Illuminate\Http\Request  $request
     * @param  mixed  $step
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $step)
    {
        /** @var \App\Agent */
        $agent = $request->user();

        $educationStep = $agent
            ->educationSteps()
            ->where('id', $step)
            ->firstOrFail();

        return EducationStepResource::make($educationStep);
    }

    /**
    * Mark as read the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  mixed  $step
    * @return \Illuminate\Http\Response
    */
    public function read(Request $request, $step)
    {
        /** @var \App\Agent */
        $agent = $request->user();

        $agent->educationSteps()->updateExistingPivot($step, [
            'read' => true,
        ]);

        $educationStep = $agent
            ->educationSteps()
            ->where('id', $step)
            ->firstOrFail();

        return EducationStepResource::make($educationStep);
    }

    /**
    * Mark as unread the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  mixed  $step
    * @return \Illuminate\Http\Response
    */
    public function unread(Request $request, $step)
    {
        /** @var \App\Agent */
        $agent = $request->user();

        $agent->educationSteps()->updateExistingPivot($step, [
            'read' => false,
        ]);

        $educationStep = $agent
            ->educationSteps()
            ->where('id', $step)
            ->firstOrFail();

        return EducationStepResource::make($educationStep);
    }
}
