<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\API\StoryCategoryResource;
use App\StoryCategory;

class StoryCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return StoryCategoryResource::collection(
            StoryCategory::with('image')->get()
        );
    }
}
