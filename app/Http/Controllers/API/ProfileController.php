<?php

namespace App\Http\Controllers\API;

use App\City;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\Profile\UpdateProfile;
use App\Http\Resources\API\ProfileResource;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return ProfileResource::make($request->user('api'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\API\Profile\UpdateProfile  $request
     * @return \Illuminate\Http\Response
     * @throws  ValidationException
     */
    public function update(UpdateProfile $request)
    {
        if ($request->has('city_id') && !City::find($request->city_id)) {
            $message = trans('validation.exists', ['attribute' => 'city']);

            throw ValidationException::withMessages(['city_id' => $message]);
        }

        $agent = $request->user();
        $agent->load('images');

        $agent->update($request->only([
            'address',
            'cellphone',
            'city_id',
            'date_of_birth',
            'name',
            'post_code',
        ]));

        return ProfileResource::make($agent);
    }
}
