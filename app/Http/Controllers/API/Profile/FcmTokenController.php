<?php

namespace App\Http\Controllers\API\Profile;

use App\FcmToken;
use App\Http\Controllers\Controller;
use App\Http\Resources\API\FcmTokenResource;
use Illuminate\Http\Request;

class FcmTokenController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validated = $request->validate([
            'token' => 'required|string|max:255',
        ]);

        $token = FcmToken::updateOrCreate(
            ['agent_id' => $request->user()->id],
            ['token' => $validated['token']]
        );

        return FcmTokenResource::make($token);
    }
}
