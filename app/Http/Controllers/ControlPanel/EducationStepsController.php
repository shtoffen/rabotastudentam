<?php

namespace App\Http\Controllers\ControlPanel;

use App\EducationStep;
use App\Http\Controllers\Controller;
use App\Http\Requests\ControlPanel\EducationSteps\StoreEducationStep;
use App\Http\Requests\ControlPanel\EducationSteps\UpdateEducationStep;

class EducationStepsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $educationSteps = EducationStep::all();

        return view(
            'control_panel.education_steps.index',
            compact('educationSteps')
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('control_panel.education_steps.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\EducationSteps\StoreEducationStep  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEducationStep $request)
    {
        $educationStep = EducationStep::create($request->only([
            'name',
            'description',
        ]));

        return redirect()->to($educationStep->path());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EducationStep  $step
     * @return \Illuminate\Http\Response
     */
    public function show(EducationStep $step)
    {
        return view(
            'control_panel.education_steps.show',
            compact('step')
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EducationStep  $step
     * @return \Illuminate\Http\Response
     */
    public function edit(EducationStep $step)
    {
        return view(
            'control_panel.education_steps.edit',
            compact('step')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\EducationSteps\UpdateEducationStep  $request
     * @param  \App\EducationStep  $step
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEducationStep $request, EducationStep $step)
    {
        $step->update($request->only([
            'name',
            'description',
        ]));

        return redirect()->to($step->path());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EducationStep  $step
     * @return \Illuminate\Http\Response
     */
    public function destroy(EducationStep $step)
    {
        $step->delete();

        return redirect()->to(route('cp.education.steps.index'));
    }
}
