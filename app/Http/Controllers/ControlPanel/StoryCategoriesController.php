<?php

namespace App\Http\Controllers\ControlPanel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ControlPanel\Stories\StoreCategory;
use App\Http\Requests\ControlPanel\Stories\UpdateCategory;
use App\StoryCategory;

class StoryCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('control_panel.stories.categories.index', [
            'categories' => StoryCategory::with('image')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('control_panel.stories.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ControlPanel\Stories\StoreCategory  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategory $request)
    {
        /** @var StoryCategory */
        $category = StoryCategory::create($request->only(['title']));
        $category->storeImage($request->file('image'));

        return redirect()->to($category->path());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StoryCategory  $category
     * @return \Illuminate\Http\Response
     */
    public function show(StoryCategory $category)
    {
        $stories = $category->stories()->with('image')->get();

        return view(
            'control_panel.stories.categories.show',
            compact('category', 'stories')
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StoryCategory  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(StoryCategory $category)
    {
        return view('control_panel.stories.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ControlPanel\Stories\UpdateCategory  $request
     * @param  \App\StoryCategory  $category
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategory $request, StoryCategory $category)
    {
        $category->update($request->only('title'));
        $category->storeImage($request->file('image'));

        return redirect()->to($category->path());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StoryCategory  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(StoryCategory $category)
    {
        $category->delete();

        return redirect()->to(route('cp.stories.categories.index'));
    }
}
