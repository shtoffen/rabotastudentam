<?php

namespace App\Http\Controllers\ControlPanel;

use App\City;
use App\Http\Controllers\Controller;
use App\Http\Requests\ControlPanel\Cities\DeleteCity;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('control_panel.cities.index', [
            'cities' => City::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('control_panel.cities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->validate([
            'name' => 'required|string|max:255|unique:cities,name',
        ]);

        City::create($attributes);

        return redirect()->to(route('cp.cities.index'))->with([
            'message' => "Город {$attributes['name']} успешно добавлен.",
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        return view('control_panel.cities.edit', compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        $attributes = $request->validate([
            'name' => [
                'required',
                'string',
                'max:255',
                Rule::unique('cities')->ignore($city->name, 'name'),
            ],
        ]);

        $city->update($attributes);

        return redirect()->to(route('cp.cities.index'))->with([
            'message' => "Город {$city->name} успешно изменен.",
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Http\Requests\ControlPanel\Cities\DeleteCity  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteCity $request, City $city)
    {
        $city->delete();

        return redirect()->to(route('cp.cities.index'))->with([
            'message' => "Город {$city->name} успешно удален.",
        ]);;
    }
}
