<?php

namespace App\Http\Controllers\ControlPanel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ControlPanel\Stories\StoreStory;
use App\Http\Requests\ControlPanel\Stories\UpdateStory;
use App\Story;
use App\StoryCategory;
use Illuminate\Http\Request;

class StoriesController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $category = StoryCategory::find($request->input('category_id'));

        return view('control_panel.stories.create', [
            'category' => $category,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ControlPanel\Stories\StoreStory  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStory $request)
    {
        /** @var \App\Story */
        $story = Story::create($request->only([
            'category_id',
            'description',
            'title',
        ]));

        $story->storeImage($request->file('image'));

        return redirect($story->category->path());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function edit(Story $story)
    {
        return view('control_panel.stories.edit', [
            'story' => $story,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ControlPanel\Stories\UpdateStory  $request
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStory $request, Story $story)
    {
        $story->update($request->only([
            'description',
            'title',
        ]));

        $story->storeImage($request->file('image'));

        return redirect($story->category->path());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function destroy(Story $story)
    {
        $category = $story->category;
        $story->delete();

        return redirect($category->path());
    }
}
