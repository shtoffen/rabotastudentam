<?php

namespace App\Http\Controllers\ControlPanel;

use App\Agent;
use App\City;
use App\Enums\AgentStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\ControlPanel\Agents\IndexAgents;
use App\Http\Requests\ControlPanel\Agents\UpdateAgent;
use App\Notifications\AgentEmployedNotification;
use App\Notifications\AgentRejectedNotification;

class AgentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Http\Requests\ControlPanel\Agents\IndexAgents  $request
     * @return \Illuminate\Http\Response
     */
    public function index(IndexAgents $request)
    {
        $status = $request->input('status');

        $agents = Agent::with('city')
            ->when(filled($status), function($query) use ($status) {
                return $query->whereStatus($status);
            })
            ->paginate(20);

        return view('control_panel.agents.index', compact('agents', 'status'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {
        return view('control_panel.agents.show', compact('agent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function edit(Agent $agent)
    {
        $cities = City::orderBy('name')->get();

        return view('control_panel.agents.edit', compact('agent', 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ControlPanel\Agents\UpdateAgent  $request
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAgent $request, Agent $agent)
    {
        $agent->update($request->only([
            'address',
            'cellphone',
            'city_id',
            'comment',
            'name',
            'post_code',
            'status',
        ]));

        if ($agent->wasChanged('status')) {
            if ($agent->status->is(AgentStatus::EMPLOYED)) {
                $agent->notify(new AgentEmployedNotification());
            }

            if ($agent->status->is(AgentStatus::REJECTED)) {
                $agent->notify(new AgentRejectedNotification());
            }
        }

        return redirect()->to($agent->path())->with([
            'message' => 'Агент успешно изменен.',
        ]);
    }
}
