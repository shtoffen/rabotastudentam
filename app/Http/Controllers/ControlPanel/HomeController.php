<?php

namespace App\Http\Controllers\ControlPanel;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return view('control_panel.home');
    }
}
