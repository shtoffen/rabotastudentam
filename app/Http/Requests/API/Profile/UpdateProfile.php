<?php

namespace App\Http\Requests\API\Profile;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CellphoneRu;

class UpdateProfile extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address'       => 'string|max:255',
            'cellphone'     => [
                'bail',
                'string',
                new CellphoneRu,
            ],
            'city_id'       => 'numeric',
            'date_of_birth' => 'date_format:Y-m-d',
            'name'          => 'string|max:255',
            'post_code'     => 'digits:6',
        ];
    }
}
