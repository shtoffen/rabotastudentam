<?php

namespace App\Http\Requests\API\Agents;

use App\Enums\AgentDeviceType;
use App\Enums\ImageContentType;
use App\Rules\CellphoneRu;
use App\Rules\EqualsTo;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class StoreAgent extends FormRequest
{
    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'passport_images.0.type' => 'passport_images.0.type',
            'passport_images.1.type' => 'passport_images.1.type',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address'                   => 'required|string|max:255',
            'cellphone'                 => [
                'bail',
                'required',
                'string',
                new CellphoneRu,
            ],
            'city_id'                   => 'bail|required|exists:cities,id',
            'email'                     => 'bail|required|string|max:255|email|unique:agents,email',
            'date_of_birth'             => 'required|date_format:Y-m-d',
            'name'                      => 'required|string|max:255',
            'password'                  => 'required|string|min:6|max:255',
            'passport_images'           => 'required|array|size:2',
            'passport_images.*.image'   => 'required|image|max:10000',
            'passport_images.0.type'    => [
                'required',
                'integer',
                new EqualsTo(ImageContentType::PASSPORT_PHOTO_SECTION),
            ],
            'passport_images.1.type'    => [
                'required',
                'integer',
                new EqualsTo(ImageContentType::PASSPORT_REGISTRATION_SECTION),
            ],
            'post_code'                 => 'required|digits:6',
        ];
    }
}
