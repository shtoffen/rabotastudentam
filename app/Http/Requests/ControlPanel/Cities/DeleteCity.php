<?php

namespace App\Http\Requests\ControlPanel\Cities;

use Illuminate\Foundation\Http\FormRequest;

class DeleteCity extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
    * Configure the validator instance.
    *
    * @param  \Illuminate\Validation\Validator  $validator
    * @return void
    */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->city->agents()->count() > 0) {
                $validator->errors()->add(
                    'city',
                    "Город {$this->city->name} не может быть удален, т.к. имеет клиентов."
                );
            }
        });
    }
}
