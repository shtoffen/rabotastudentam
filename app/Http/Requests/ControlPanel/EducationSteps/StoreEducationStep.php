<?php

namespace App\Http\Requests\ControlPanel\EducationSteps;

use Illuminate\Foundation\Http\FormRequest;

class StoreEducationStep extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|string|max:255',
            'description'       => 'required|string|max:20000',
        ];
    }
}
