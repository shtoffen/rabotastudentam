<?php

namespace App\Http\Requests\ControlPanel\Agents;

use App\Enums\AgentStatus;
use App\Rules\CellphoneRu;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAgent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->has('status')
            && !$this->agent->status->is(AgentStatus::PENDING)
        ) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address'       => 'string|max:255',
            'cellphone'     => ['bail', 'string', new CellphoneRu],
            'city_id'       => 'integer|exists:cities,id',
            'comment'       => 'nullable|string|max:255',
            'name'          => 'string|max:255',
            'post_code'     => 'digits:6',
            'status'        => [
                'integer',
                new EnumValue(AgentStatus::class, false)
            ],
        ];
    }
}
