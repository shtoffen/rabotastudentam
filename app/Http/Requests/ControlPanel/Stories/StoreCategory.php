<?php

namespace App\Http\Requests\ControlPanel\Stories;

use Illuminate\Foundation\Http\FormRequest;

class StoreCategory extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required|string|max:255',
            'image'         => 'required|image|max:10000',
        ];
    }
}
