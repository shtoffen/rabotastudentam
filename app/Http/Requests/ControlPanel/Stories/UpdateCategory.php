<?php

namespace App\Http\Requests\ControlPanel\Stories;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCategory extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required|string|max:255',
            'image'         => 'image|max:10000',
        ];
    }
}
