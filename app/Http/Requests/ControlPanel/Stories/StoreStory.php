<?php

namespace App\Http\Requests\ControlPanel\Stories;

use Illuminate\Foundation\Http\FormRequest;

class StoreStory extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'   => 'required|numeric|exists:story_categories,id',
            'description'   => 'required|string|max:2500',
            'image'         => 'required|image|max:10000',
            'title'         => 'required|string|max:255',
        ];
    }
}
