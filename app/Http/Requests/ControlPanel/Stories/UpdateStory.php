<?php

namespace App\Http\Requests\ControlPanel\Stories;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStory extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'   => 'required|string|max:2500',
            'image'         => 'image|max:10000',
            'title'         => 'required|string|max:255',
        ];
    }
}
