<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentEducationStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_education_step', function (Blueprint $table) {
            $table->unsignedBigInteger('agent_id');
            $table->unsignedInteger('education_step_id');
            $table->boolean('read')->default(false);

            $table->foreign('agent_id')
                ->references('id')
                ->on('agents')
                ->cascadeOnDelete();

            $table->foreign('education_step_id')
                ->references('id')
                ->on('education_steps')
                ->cascadeOnDelete();

            $table->index(['agent_id', 'education_step_id']); // composite index
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_education_step');
    }
}
